trigger BathroomTrigger on Bathroom__c (before insert,before update, after update,before delete) {
    

    boolean runTriggers=true; 
    if(!Test.IsRunningTest() && !trigger.isDelete){   
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(Bathroom__c bath: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && bath.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
        }       
    }   
    if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledBathroomTrigger){
             runTriggers=false;
         }
    }   
    if(runTriggers){       
        
        if(Trigger.isBefore && Trigger.isUpdate){
            HouseJsonOptimizer.houseJsonObjectInitializer(Constants.OBJECT_BATHROOM);
        }
        if(Trigger.isAfter && Trigger.isUpdate){
            BathroomTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        } 

        if(Trigger.isBefore && Trigger.isInsert){
            BathroomTriggerHandler.beforeInsert(Trigger.new);
        }

        if(Trigger.isAfter && Trigger.isUpdate && HouseJsonOptimizer.BATHROOM_WEB_ENTITY_HOUSE_FLAG){     
             
            BathroomTriggerHandler.sendWebEntityHouseJson(Trigger.newMap);        

        }
    }

         if(Trigger.isBefore && Trigger.isDelete)
             {
                CannotDelete.onlyAdmin(Trigger.old);
             }
      
}