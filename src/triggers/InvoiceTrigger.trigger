trigger InvoiceTrigger on Invoice__c (after insert, before update, after update, after delete, after undelete) {

	if(Trigger.isAfter){

		if(Trigger.isInsert){
			InvoiceTriggerHandler.updateHouseEconomics(Trigger.new);
		}

		if(Trigger.isUpdate){
			InvoiceTriggerHandler.updateHouseEconomics(Trigger.new);
		}

		if(Trigger.isDelete){
			InvoiceTriggerHandler.updateHouseEconomics(Trigger.old);
		}		

		if(Trigger.isUndelete){
			InvoiceTriggerHandler.updateHouseEconomics(Trigger.new);
		}		
	}
}