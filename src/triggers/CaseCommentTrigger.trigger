trigger CaseCommentTrigger on Case_Comment__c (after insert, after Update) {

	if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
		CaseCommentTriggerHandler.updateNoOfCommentsOnCase(Trigger.new);
	}

}