/*
* Description: update opportunity records based on few conditions.
*/

trigger QuoteTrigger on Quote (Before Insert, After Insert,After Update,Before Update) {
    
   If(Trigger.IsAfter && Trigger.IsInsert){
        QuoteTriggerHandler.AfterInsert(Trigger.New);
    }
    
    If(Trigger.IsAfter && Trigger.IsUpdate){
        QuoteTriggerHandler.AfterUpdate(Trigger.NewMap,Trigger.OldMap);
        QuoteTriggerHandler.createQuoteLineItems(Trigger.NewMap,Trigger.OldMap);    
    }
    
    If(Trigger.IsBefore && Trigger.IsUpdate){
        QuoteTriggerHandler.BeforeUpdate(Trigger.NewMap, Trigger.OldMap);
        QuoteTriggerHandler.updatePricebookOnQuote(Trigger.NewMap, Trigger.OldMap);
    }
}