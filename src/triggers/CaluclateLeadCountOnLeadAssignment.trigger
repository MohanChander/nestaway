/*
* Description: calculate lead count on lead assignment.
*/


trigger CaluclateLeadCountOnLeadAssignment on Lead_Distribution__c (after insert,after update,before update,after delete){
   // Boolean runTrigger = EnableTrigger__c.getInstance('CaluclateLeadCountOnLeadAssignment').IsEnable__c;
    //if( runTrigger){
        if(Trigger.isUpdate && Trigger.isafter){
            LeadDistributionTriggerHandler.afterUpdateHandler(Trigger.new, Trigger.NewMap, Trigger.OldMap);
        }
        if(trigger.isInsert){
            LeadDistributionTriggerHandler.afterInsertHandler(Trigger.new, Trigger.NewMap);
        }
        if(Trigger.isDelete){
            LeadDistributionTriggerHandler.afterdeleteHandler(Trigger.Old);
        }
       
    //} // run trigger
}