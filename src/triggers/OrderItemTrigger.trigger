trigger OrderItemTrigger on OrderItem (before insert, after update) {

	if(Trigger.isAfter && Trigger.isUpdate){
		OrderItemTriggerHandler.ChangePOStageToVerified(Trigger.newMap, Trigger.oldMap);
	}
}