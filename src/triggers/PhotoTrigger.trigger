trigger PhotoTrigger on Photo__c (before insert, before update, after insert, after update) {

   boolean runTriggers=true; 
   if(!Test.IsRunningTest()){   
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(Photo__c photo: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && photo.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
        }       
    } 
   if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledPhotoTrigger){
             runTriggers=false;
         }
    }   
    if(runTriggers){   

       if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
            HouseJsonOptimizer.houseJsonObjectInitializer(Constants.OBJECT_PHOTO);
        }

        if(trigger.isAfter && trigger.isInsert){
            PhotoTriggerHandler.afterInsert(trigger.newMap) ;
        }
        
        if(trigger.isBefore && trigger.isUpdate){
            PhotoTriggerHandler.beforeUpdate(trigger.newMap,trigger.oldMap);
        }
        

        if(trigger.isAfter && trigger.isUpdate){
            PhotoTriggerHandler.updateDocuments(trigger.newMap,trigger.oldMap);
        }

        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate) && HouseJsonOptimizer.PHOTO_WEB_ENTITY_HOUSE_FLAG){
            PhotoTriggerHandler.sendWebEntityHouseJson(Trigger.new);
        }
    }   
}