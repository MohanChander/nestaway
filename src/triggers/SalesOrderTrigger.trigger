/*
* Description: update opportunity records based on few conditions.
*/

trigger SalesOrderTrigger on Order (after Update) {
	if(Trigger.IsAfter && Trigger.IsUpdate){
        SalesOrderTriggerHandler.AfterUpdate(Trigger.NewMap , Trigger.OldMap);
    }

    if(Trigger.isAfter && Trigger.isUpdate){
    	SalesOrderTriggerHandler.generatePurchaseOrders(Trigger.NewMap , Trigger.OldMap);
    }
    
    
}