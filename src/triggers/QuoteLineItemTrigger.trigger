/***********************************************************
* Created By:   Sanjeev Shukla(KVP Business Solutions)
* Created Date: 16/March/2017
************************************************************/
trigger QuoteLineItemTrigger on QuoteLineItem (before insert, before update, after insert, after update) {
    
    /**************************************
    * Map the Saervice Tax and Vat Tax value from Tax object based on state and product.
    */
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        QuoteLineItemTriggerHandler.updateServiceAndVatTax(trigger.new);
    }

    /**************************************
    * Validate the product family's value of related product with contract's furnishing package's value of related oppportunity.
    */
    
    if (trigger.isBefore && trigger.isInsert ) {
        QuoteLineItemTriggerHandler.validate(trigger.new);
    }
    
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate) ){
        System.debug('************update entry***************');
        QuoteLineItemTriggerHandler.updateQuoteType(trigger.new, trigger.isInsert);
        System.debug('************update exit***************');
    }

    /*
    * map service tax, vat, SwachchaBharat tax, Krish Kalyan Tax from QuoteLineItem to Quote
    */
    
}