trigger AssetTrigger on Asset (before insert, after insert) {
	
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            AssetTriggerHandler.afterInsert(Trigger.NewMap);
        }
    }
}