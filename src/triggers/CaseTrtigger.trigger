trigger CaseTrtigger on Case (after update, after Insert, before update, before insert,before delete) {
    System.debug('***StopRecursion.CASE_SWITCH '+StopRecursion.CASE_SWITCH);
    if(StopRecursion.CASE_SWITCH){
        if(Trigger.isBefore && Trigger.isInsert){
            System.debug('*****Trigger isBefore && Trigger.isInsert ');
            CaseServiceRequestTriggerHandler.beforeInsert(Trigger.new);
          
            CaseMIMOFunctionality.beforeInsertMIMO(Trigger.new) ;


            /*Added By Baibahv*/
            CaseOffboardingTriggerHandler.onBeforeInsertCaseOffboarding(Trigger.new);
            CaseOffboardingTriggerHandler.onBeforeUpdateandInsert(trigger.new);
        }
        
        if(Trigger.isAfter && Trigger.isInsert){
          
            CaseMIMOFunctionality.afterInsertMIMO(Trigger.newMap);

            /*
              Added By Baibahv
            */ 
            CaseOffboardingTriggerHandler.onAfterInsertCaseOffboarding(Trigger.new,Trigger.oldmap);
                        CaseTriggerhandler.afterInsert(Trigger.newMap,Trigger.oldmap);

        }

        if(Trigger.isBefore && Trigger.isUpdate){
            CaseServiceRequestTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
            CaseTriggerhandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
             CaseMIMOFunctionality.beforeUpdate(Trigger.newMap, Trigger.oldMap);
             CaseOffboardingTriggerHandler.onBeforeUpdateandInsert(trigger.new);
             CaseOffboardingTriggerHandler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
        }


         if(trigger.isAfter && trigger.isUpdate)
         {
            System.debug('Case Trigger Fired - Is After Update');
            CaseTriggerhandler.afterUpdate(trigger.newMap,trigger.OldMap);
            CaseTriggerhandler.createAssetsAfterVerification(trigger.newMap, trigger.oldMap);
            CaseServiceRequestTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);            
            CaseMIMOFunctionality.afterUpdateMIMO(trigger.newMap,trigger.OldMap) ;
            CaseMIMOTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
            CaseOffboardingTriggerHandler.onAfterUpdateCaseOffboarding(Trigger.new, Trigger.oldMap);  
          
         }
         if(Trigger.isBefore && Trigger.isDelete)
             {
                CannotDelete.onlyAdmin(Trigger.old);
             }

    }
}