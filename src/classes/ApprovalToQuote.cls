/*
* Description: updates the contract or qoute status and comment field whenever email hits the salesforce.
*/
global class ApprovalToQuote implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        // Captures the sender's email address
        String emailAddress = envelope.fromAddress;
         
          // Retrieves the sender's first and last names
        String fName = email.fromname.substring(0,email.fromname.indexOf(' '));
        String lName = email.fromname.substring(email.fromname.indexOf(' '));
        String autoNum;
        Boolean isQuote = FALSE;
        Boolean isContract = FALSE;
        system.debug('firstname:'+fName); 
        system.debug('lastname:'+lName);
        String[] emailBody = email.plainTextBody.split('\n', 0);
        String [] status= emailBody[0].split('\\s');
        string[] subject = email.subject.split('\\s');
        
        for(String sub: subject ){
            if(sub.containsIgnoreCase('Quote')){
              isQuote = TRUE;  
            }
            if(sub.containsIgnoreCase('Contract')){
              isContract = TRUE;  
            }
            if(sub.length()== 8 && sub.substring(0).isNumeric()){
                autoNum = sub;
                system.debug('===autoNum==='+autoNum );
            }   
        } 
       
        if(autoNum != null && isQuote ){
            Quote quo = [SELECT Id, Approval_Status__c,Comments__c,QuoteNumber From Quote where QuoteNumber=:autoNum ];
           
            if(quo.Id != null){
                for(String str :status){
                    if(str.containsIgnoreCase('YES') || str.containsIgnoreCase('APPROVED')|| str.containsIgnoreCase('APPROVE')){
                        quo.Approval_Status__c = 'Approved by Owner';
                        
                    }
                    else if(str.containsIgnoreCase('REJECT') || str.containsIgnoreCase('REJECTED')|| str.containsIgnoreCase('NO')){
                        quo.Approval_Status__c = 'Rejected by Owner';
                        
                    }
                    
                }
                
                for(integer i =0; i<= emailBody.size()-1; i++){
                    if(quo.Approval_Status__c != 'Approved by Owner' || quo.Approval_Status__c != 'Approved by Owner'){
                        if(quo.Comments__c == null){
                        quo.Comments__c =  emailBody[i] + '\n' ;
                        }
                        else{
                            quo.Comments__c +=  emailBody[i]+'\n';
                        }
                    } 
                    else{
                        if((i+1)<emailBody.size()){
                            if(quo.Comments__c == null){
                                quo.Comments__c =  emailBody[i+1] + '\n' ;
                            }
                            else{
                                quo.Comments__c +=  emailBody[i+1]+'\n';
                            }
                        }
                    }
                    
                       
                }
                try{
                    update quo; 
                }
                catch(Exception ex){
                    system.debug('====Exception===='+ex);
                }
            }
            }
           else if(autoNum != null && isContract){
           system.debug('===Inside contract==='+autoNum );
           Contract contract = [SELECT Id, Approval_Status__c,Comments__c,ContractNumber From Contract where ContractNumber=:autoNum ];   
               if(contract.Id != null){
                    system.debug('===Inside contract.Id==='+contract.Id);
                    for(String str :status){
                        if((str.containsIgnoreCase('YES') || str.containsIgnoreCase('APPROVED')|| str.containsIgnoreCase('APPROVE')) && (contract.Approval_Status__c == 'Approved by ZM' || contract.Approval_Status__c == 'Sample Contract Manually Approved by ZM' )) {
                            contract.Approval_Status__c = 'Sample Contract Approved by Owner';
                            Contract.status = 'Final Contract';
                        }
                        else if(str.containsIgnoreCase('REJECT') || str.containsIgnoreCase('REJECTED')|| str.containsIgnoreCase('NO') && (contract.Approval_Status__c == 'Approved by ZM' || contract.Approval_Status__c == 'Sample Contract Manually Approved by ZM' )){
                            contract.Approval_Status__c = 'Sample Contract Rejected by Owner';
                        }                       
                    }                    
                    for(integer i =1; i<= emailBody.size()-1; i++){
                        
                            if(contract.Comments__c == null){
                                contract.Comments__c =  emailBody[i] + '\n' ;
                            }
                            else{
                                contract.Comments__c +=  emailBody[i]+'\n';
                            }                           
                    }
                    system.debug('===contract==='+contract);
                    try{
                        update contract; 
                    }
                    catch(Exception ex){
                        system.debug('====Exception===='+ex);
                    }
                }
            }
           
        return result;
        
    }  
        
        
}