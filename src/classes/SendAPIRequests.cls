Public class SendAPIRequests{
    
    
    public class RPDetailsWrapeerClass{
        
        public Decimal amount{get;set;}
        public String house_sf_id{get;set;}
        public String work_order_id{get;set;}
        public String reason{get;set;}
    }
    public class RPDetailsJsonRespWrapeerClass{        
        public string success;
        public string info;
        public dataWrapper data;
         
    }
    public class dataWrapper{
        
        public boolean sd_exists{get;set;}      
    }
    
     @future(callout=true)
     public static void sendRPDetailsToWebAppFuture(Id workOrderId){    
       
          sendRPDetailsToWebApp(workOrderId);
     }
    
     public static void sendRPDetailsToWebApp(Id workOrderId){   
     
        String respBody;
        try{
                List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
                Workorder wo = [select id,API_Success__c,API_Info__c,Last_API_Sync_Time__c,house__c,Upfront_amount__c from workorder where id=:workOrderId];    
                RPDetailsWrapeerClass rp = new RPDetailsWrapeerClass();
                rp.amount=wo.Upfront_amount__c;
                rp.house_sf_id=wo.house__c;
                rp.work_order_id=wo.id;
                rp.reason='upfront';                
                string strEndPoint= nestURL[0].WO_Send_RP_Details_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
                HttpResponse res;
                string jsonBody; 
                jsonBody=JSON.serialize(rp);
                res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
                respBody= res.getBody();
                system.debug('***RespBody'+respBody);               
                RPDetailsJsonRespWrapeerClass dataJson= new RPDetailsJsonRespWrapeerClass();
                dataJson = (RPDetailsJsonRespWrapeerClass)JSON.deserialize(respBody,RPDetailsJsonRespWrapeerClass.Class);              
                if(dataJson!=null){
                    wo.API_Success__c = dataJson.success;
                    wo.API_Info__c   = dataJson.info;      
                    wo.Last_API_Sync_Time__c = System.now();                
                    wo.RP_Sent_to_webapp__c=true;
                    if(dataJson.data.sd_exists!=null && dataJson.data.sd_exists==true){
                        wo.Description='Sd is already exists for this house.';
                        wo.Status='Canceled';
                    }
                }                
               if(wo.id!=null){                    
                    StopRecursion.DisabledWorkOrderTrigger= true;                        
                    Update wo;         
               }
            }
            catch(exception e){
                
                UtilityClass.insertErrorLog('Webservices','API Failed for RP send to Webapp :'+workOrderId+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+' **RespBody'+respBody); 
            }               
               
     
       
     }
    
    public static void accountSync(Id accountIds){             
                    
                String respBody;
                try{
                    List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
                    account acc = new account();
                    acc.id=accountIds;
                    string strAccountJson;
                    strAccountJson        = GetObjectJson('Account', accountIds);           
                    strAccountJson        = strAccountJson;
                    string strEndPoint= nestURL[0].Account_Sync_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
                    HttpResponse res;
                    system.debug('***jsonBody'+strAccountJson); 
                    res=RestUtilities.httpRequest('POST',strEndPoint,strAccountJson,null);
                    respBody = res.getBody();                    
                    Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(respBody);
                    system.debug('***RespBody'+respBody); 
                    acc.API_Success__c = String.ValueOf(m.get('success'));
                    acc.API_Info__c   = String.ValueOf(m.get('info'));      
                    acc.Last_API_Sync_Time__c = System.now();              
                   if(acc.id!=null){                    
                        StopRecursion.DisabledAccountTrigger  = true;                        
                        Update acc;         
                   }
                }
                catch(exception e){
                    
                    UtilityClass.insertErrorLog('Webservices','API Failed for account sync:'+accountIds+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+'**RespBody'+respBody); 
                }               
               
            
        
        
    }
    
    
    public static void bankSync(Id bankIds){
        String respBody;
         try{
                    List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
                    Bank_Detail__c bank = new Bank_Detail__c();
                    bank.id=bankIds;
                    string strAccountJson;
                    strAccountJson        = GetObjectJson('Bank_Detail__c', bankIds);           
                    strAccountJson        = strAccountJson;
                    string strEndPoint= nestURL[0].Bank_Sync_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
                    HttpResponse res;
                    system.debug('***jsonBody'+strAccountJson); 
                    res=RestUtilities.httpRequest('POST',strEndPoint,strAccountJson,null);
                    respBody = res.getBody();                    
                    Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(respBody);
                    system.debug('***RespBody'+respBody); 
                    bank.API_Success__c = String.ValueOf(m.get('success'));
                    bank.API_Info__c   = String.ValueOf(m.get('info'));      
                    bank.Last_API_Sync_Time__c = System.now();              
                   if(bank.id!=null){                    
                        StopRecursion.DisabledBankTrigger   = true;                        
                        Update bank;         
                   }
                }
                catch(exception e){
                    
                    UtilityClass.insertErrorLog('Webservices','API Failed for bank account sync :'+bankIds+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+'**RespBody'+respBody); 
                }            
        
        
    }
    
    
    Public Static String GetObjectJson(String SobjectName , Id recId){
        String SobjectApiName = SobjectName;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String strFields = '';
         
        for(Schema.SObjectField fieldName : fieldMap.Values() ){
            if(strFields == null || strFields == '')
            {
            strFields = String.ValueOf(fieldName);
            }else{
            strFields = strFields + ' , ' + String.ValueOf(fieldName);
            }
        }
        
        JSONGenerator gen_opp = JSON.createGenerator(true);
        gen_opp.writeStartObject();
        if(recId!=null){
            String query ='';
            
            query = 'SELECT ' + strFields + ' FROM ' + SobjectApiName + ' WHERE ID=:recId Limit 1';
           
            
            Sobject objOpp = Database.query(query);        
           
            if(objOpp!=null){
                for(Schema.SObjectField eachField : fieldMap.Values()){
                
                    String fieldName = String.ValueOf(eachField);
                    Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                    
                    
                    if(fielddataType==Schema.DisplayType.Boolean){
                        gen_opp.writeBooleanField(fieldName,Boolean.valueOf(objOpp.get(fieldName)));
                    }else if(fielddataType==Schema.DisplayType.Date){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeDateField(fieldName,Date.ValueOf(objOpp.get(fieldName)));
                        }else{
                               gen_opp.writeNullField(fieldName);
                            }
                    }else if(fielddataType==Schema.DisplayType.DateTime){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeDateTimeField(fieldName,DateTime.ValueOf(objOpp.get(fieldName)));
                        }else{
                               gen_opp.writeNullField(fieldName);
                            }
                    }else if(fielddataType==Schema.DisplayType.Double){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Double.ValueOf(objOpp.get(fieldName)));
                        }else{
                               gen_opp.writeNullField(fieldName);
                            }
                    }else if(fielddataType==Schema.DisplayType.Percent){
                            if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Double.ValueOf(objOpp.get(fieldName)));
                        }else{
                               gen_opp.writeNullField(fieldName);
                            }
                    }else if(fielddataType==Schema.DisplayType.Currency){
                            if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Double.ValueOf(objOpp.get(fieldName)));
                        }else{
                               gen_opp.writeNullField(fieldName);
                            }
                    }else if(fielddataType==Schema.DisplayType.Integer){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Integer.ValueOf(objOpp.get(fieldName)));
                        }else{
                               gen_opp.writeNullField(fieldName);
                            }
                    }else if(fielddataType==Schema.DisplayType.Email  || fielddataType==Schema.DisplayType.URL || fielddataType==Schema.DisplayType.ID || fielddataType==Schema.DisplayType.MultiPicklist ||  fielddataType==Schema.DisplayType.Picklist || fielddataType==Schema.DisplayType.String || fielddataType==Schema.DisplayType.TextArea || fielddataType==Schema.DisplayType.REFERENCE){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeStringField(fieldName,String.ValueOf(objOpp.get(fieldName)));
                        }else{
                               gen_opp.writeNullField(fieldName);
                            }
                    }                                
                }
            }            
            gen_opp.writeEndObject();
            
            //System.debug('==gen_opp=='+gen_opp.getAsString());
        }
        return gen_opp.getAsString();
    }
 
    
    
}