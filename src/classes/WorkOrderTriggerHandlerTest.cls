@IsTest
public class WorkOrderTriggerHandlerTest {
    public Static id recordTypeOffboardingTanent = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_TENANT_MOVE_OUT).getRecordTypeId();
    public Static id recordTypeOffboardingUnfurnished = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_HOUSE_UNFURNISHED).getRecordTypeId();
    public Static id recordTypeOffboardingDisconnected = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_HOUSE_DISCONNECTED).getRecordTypeId();
    public Static id recordTypeOffboardingFullandFinal = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_FULL_FINAL_AMT).getRecordTypeId();
    public Static id recordTypeOffboardingFinalInspection = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_FINAL_INSPECTION).getRecordTypeId();
    public Static id recordTypeOffboardingHouseMaintance= Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_HOUSE_MAINTENANCE).getRecordTypeId();
    public Static id unfurnishedOffboardingChecklist = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_OFFBOARDING_UNFURNISHED_CHEKCLIST).getRecordTypeId();
    public Static id recordTypeOuotationformVender = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_QUOTATION_VENDER).getRecordTypeId();
    
    public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    
    Public Static TestMethod Void doTest(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Test.startTest();
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.house__c=hos.id;
        insert b;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.RecordTypeId=caseOccupiedFurnishedRecordtype;
        c.Active_Tenants__c=0;
        c.HouseForOffboarding__c=hos.id;
        c.Status='Open';
        c.Booking_Type__c = Constants.HOUSE_BOOKING_TYPE_SHARED_HOUSE;
        insert c;
        
        workOrder wrko=new workOrder();
        wrko.Status='Open';
        wrko.House__c=hos.id;
        wrko.RecordTypeId=recordTypeOffboardingTanent;
        wrko.CaseId=c.id;
        insert wrko;
          List<workorder> wdList= new List<workorder>();   
        WdList.add(wrko);
        Map<id,workorder> newmap= new Map<Id,workorder>();
        newmap.put(wrko.id,wrko);
        List<String> listofemail= new List<String>();
        listofemail.add('deepak@gmail.com');
   
      WorkOrderTriggerHandler.afterInsert(newmap);
        WorkOrderTriggerHandler.beforeUpdate(newmap,newmap);
        WorkOrderTriggerHandler.afterUpdate(newmap,newmap);
        WorkOrderTriggerHandler.onAfterUpdateOffboardingWordkorder(WdList,newmap);
        WorkOrderTriggerHandler.updateCaseInWebApp(c.id,'Closed','Closed','Closed');
        //WorkOrderTriggerHandler.SendEmailToTenants(listofemail);
      
        
    }
}