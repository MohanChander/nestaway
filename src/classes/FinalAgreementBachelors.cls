/*
* Description: It's extention class of FinalAgreementBachelors page to generate PDF File.
*/

public class FinalAgreementBachelors {
    public Order orderRec{get;set;}
    public id PageOppId{get;set;}
    public Contract contractTerm{get;set;}
    public House_Inspection_Checklist__c hicChecklist{get;set;}
    public List<integer> roomCount{get;set;}
    //Constructor
    public FinalAgreementBachelors(ApexPages.StandardController stdController) {
        //this.pageOrder = (Order)stdController.getRecord();
        //system.debug('pageOrder' + pageOrder);
        id contractId = ApexPages.currentPage().getParameters().get('contractId');
        system.debug('id = ' + contractId );
        
        contractTerm = new contract();
        contractTerm = [Select id,Opportunity__c,Deposit_Payment_Date__c,Furniture_Buyback_duration__c,Furniture_Buyback_Option_Available__c,Furnishing_Plan__c,Furnishing_Type__c,Security_Deposit_Payment_Mode__c,MG_Start_Date__c,MG_Amount__c,MG_Valid_Until__c,MG_End_Date__c,MG_Payout_Frequency__c,Tenancy_Type__c,Base_House_Rent__c,Maximum_number_of_tenants_allowed__c,Furnishing_Commission_Percentage__c,Tenancy_Approval_time_period__c,Who_pays_Deposit__c,Restrict_SD_to_maximum_SD__c,Maximum_SD_Amount__c,SD_Upfront_Amount__c,MG_Applicable__c,Is_MG_Applicable__c from contract where Id=: contractId limit 1];
        Id hicRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('House Inspection Checklist').getRecordTypeId();
        PageOppId = contractTerm.Opportunity__c;
        
        //orderRec = [Select id,POA_Name__c,POA_Relation_with_Guardian__c,POA_Date__c,POA_Witness_Name__c,POA_Father_Husband_Name__c,POA_Age__c,POA_Permanent_Address__c,Do_you_want_to_add_a_POA__c,Rental_Plan__c,Service_Agreement_In_Effect_Date__c,Primary_Phone__c,Secondary_Phone__c,Fixed_Monthly_Rent__c,Monthly_Rental_Commission__c,opportunityId,Name__c,Age__c,Relation_with_Guardian__c,Father_Husband_Name__c,Street__c,City__c,State__c,Country__c,Pin_Code__c from Order where ContractId =: contractId ];
        orderRec = [Select id,POA_Name__c,POA_Relation_with_Guardian__c,POA_Date__c,POA_Witness_Name__c,POA_Father_Husband_Name__c,POA_Age__c,POA_Permanent_Address__c,Do_you_want_to_add_a_POA__c,Rental_Plan__c,Service_Agreement_In_Effect_Date__c,Primary_Phone__c,Secondary_Phone__c,Fixed_Monthly_Rent__c,Monthly_Rental_Commission__c,opportunityId,Name__c,Age__c,Relation_with_Guardian__c,Father_Husband_Name__c,Street__c,City__c,State__c,Country__c,Pin_Code__c from Order where OpportunityId =: PageOppId ];
        hicChecklist = [Select id,Type_of_parking_available__c,Number_of_bike_parking__c,Number_of_car_parking__c,House_layout__c from House_Inspection_Checklist__c where Opportunity__c =: PageOppId and RecordTypeId =: hicRecordTypeId limit 1];
        roomCount = new List<integer>();
        if(hicChecklist.House_layout__c != null){
            string strCount = hicChecklist.House_layout__c.substring(0,hicChecklist.House_layout__c.indexOf('B'));
            integer intRoomCount = integer.valueOf(strCount.trim());
            for(integer i=1;i<= intRoomCount;i++ ){
                roomCount.add(i);    
            }
            system.debug('--roomCount---'+roomCount);
        }
    }
}