@iStest
public class BedTriggerHelperTest {
    
    public Static TestMethod void houseTest(){
               id recordTypeOffboardingTanent = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_TENANT_MOVE_OUT).getRecordTypeId();

            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
          User newUser = Test_library.createStandardUser(1);
        insert newuser;
           Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
       
        
         Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            insert opp;
         contract c= new Contract();
        c.accountid=accobj.id;
        c.Opportunity__c=opp.id;
        c.Furnishing_Type__c = Constants.CONTRACT_FURNISHING_CONDITION_FURNISHED;
        insert c;
        
          zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        Room_Terms__c rc= new Room_Terms__c();
        insert rc;
        City__c ci = new City__c();
        ci.name='bangalore';
        insert ci;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Contract__c=c.id;
        hos.City__c='bangalore';
        hos.House_Owner__c=accObj.id;
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c =	'House Live';
       	hos.Opportunity__c=opp.id;
        hos.Initiate_Offboarding__c='no';
        hos.OwnerId=newuser.id;
        insert hos;
         Test.StartTest();
         
        workOrder wrko=new workOrder();
           wrko.Status='Open';
        wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
          insert wrko;
        
        bed__c bedc= new bed__c();
        bedc.House__c=hos.id;
        bedc.Room_Terms__c=rc.id;
        insert bedc;
        Bathroom__c bac= new Bathroom__c ();
        bac.House__c=hos.Id;
        insert bac;
      
        List<bed__c> bedList= new   List<bed__c>();
       bedList.add(bedc);
        
            BedTriggerHelper.BedTriggerTanentOffboardingUpdate(bedList);
       
            Test.stopTest();
    }

}