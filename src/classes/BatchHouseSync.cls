global class BatchHouseSync implements Database.Batchable<sObject>,  Database.AllowsCallouts {
    
    public final string query;
    
    public BatchHouseSync(string query){
        
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<house__c> houseList) {
      
        try{
                
                for(House__c house: houseList){
                    House2Json.createJson(house.Id);
                }
        
        }
        catch(exception e){
            System.debug('***exception '+e.getLineNumber()+'  '+e.getMessage()+' \n '+e);  
            UtilityClass.insertGenericErrorLog(e, 'BatchHouseSync'); 
        }

         
    }   
    
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchHouseSync executed');
    }
}