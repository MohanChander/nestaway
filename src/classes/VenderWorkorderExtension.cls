public class VenderWorkorderExtension 
{

    public Case cas;
    public Datetime startTime {get;set;}
    public Datetime endTime {get;set;}
    public String username {get;set;}
    public String vendername {get;set;}
    public String assignTo {get;set;}
    public List<SelectOption> userOptions {get;set;}
    public List<SelectOption> venderOptions {get;set;}
    public boolean assintovender {get;set;}
    public boolean assintoaggre {get;set;}
    public String billTo {get;set;}
    public boolean recoverable {get;set;}
    public String addErrorMessage;
    public Integer wrkCount {get;set;}
    public id casid;

    public VenderWorkorderExtension(ApexPages.StandardController stdController) 
    {
        try
        {
        casid = stdController.getRecord().id;
        billTo='';
        recoverable=false;
        cas = new case();
        List<SelectOption> assignOptions=new List<SelectOption>();
        assignOptions.add(new SelectOption('Assign To Technician','Assign To Technician'));
        assignOptions.add(new SelectOption('Assign To Aggregator','Assign To Aggregator'));
        Account acc=new Account();
        cas=[Select id,Zone_Code__c,Problem__r.Skills__c,Description,Preferred_Visit_Time__c,Recoverable_From__c,House1__c,Recoverable__c,House1__r.House_Owner__c,Account.name,Service_Visit_Time__c,Casenumber,Accountid,subject from case where id=:casid];
        if(cas.House1__c!=null)
        {
            
            if(cas.House1__r.House_Owner__c!=null)
            {

                        acc=[select id,name from Account where id=:cas.House1__r.House_Owner__c];
                   
                        if(cas.Preferred_Visit_Time__c!=null)
                        {
                        startTime=cas.Preferred_Visit_Time__c;
                        endTime=cas.Preferred_Visit_Time__c.addHours(1);
                        }
                         If(cas.Recoverable__c=='Yes'||cas.Recoverable__c=='No')
                         {
                           recoverable=true; 
                         }
                         else 
                         {
                             addErrorMessage='Recoverable field is null';
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
                         }

                        if(cas.Recoverable__c=='Yes')
                        {
                            if(cas.Recoverable_From__c==Constants.CASE_RECOVERABLE_OWNER)
                            {
                               billTo= acc.name;
                            }
                            else if(cas.Recoverable_From__c==Constants.CASE_RECOVERABLE_TENANT_NESTAWAY || cas.Recoverable_From__c==Constants.CASE_RECOVERABLE_OWNER_NESTAWAY)
                            {
                               billTo= 'Nestaway';
                            }
                            else if(cas.Recoverable_From__c==Constants.CASE_RECOVERABLE_TENANT || cas.Recoverable_From__c==Constants.CASE_RECOVERABLE_TENANT_OWNER)
                            {
                               billTo= cas.Account.name;
                            }
                        }
                }
                else
                {
                    addErrorMessage='There is no Owner on House';
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));    
                }
             }
        else if(cas.House1__c==null)
        {
          addErrorMessage='There is no house on case'; 
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));    
        }
        System.debug(assignOptions);
        assintovender=false;
        assintoaggre=false;
        Options();
      //  Vender();
        VenderWrkCount();
        } Catch(Exception e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage+' '+'Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString()));  
            }
    }

/**********************************************************************************************************************************************************************/
    public List<SelectOption> getAssignOptions()
    {
         List<SelectOption> assignOptions=new List<SelectOption>();
        assignOptions.add(new SelectOption('Assign To Technician','Assign To Technician'));
        assignOptions.add(new SelectOption('Assign To Aggregator','Assign To Aggregator'));
        return assignOptions;
    }
    public void Assign()
    {
     if(assignTo=='Assign To Aggregator') 
     {
        assintovender=true;
        assintoaggre=false;
     } 
     else if(assignTo=='Assign To Technician') 
     {
        assintovender=false;
        assintoaggre=true;
     } 

    }

    public void Options()
     { 
            
        System.debug('*******'+startTime);
        userOptions = new List<SelectOption>();
        List<Zone_and_OM_Mapping__c> znMapList= [select User__c from Zone_and_OM_Mapping__c where Zone_Code__c=:cas.Zone_Code__c and isActive__c=true and Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_TECHNICIAN];
        Set<id> usrIDSet = new Set<id>();
        for(Zone_and_OM_Mapping__c zm:znMapList)
        {
            usrIDSet.add(zm.User__c);  
        } 
        List<Holiday__c> holiList = [select User__c,End_Date_Time__c,Start_Date_Time__c from Holiday__c where User__c=:usrIDSet and ((( Start_Date_Time__c < :startTime)  and (End_Date_Time__c > :startTime ))or ((Start_Date_Time__c < :endTime) and (End_Date_Time__c > :endTime)))];
        Map<id,List<Holiday__c>> holiMapUsrId = new Map<id,List<Holiday__c>>();
        List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:usrIDSet and status!=:Constants.CASE_STATUS_RESOLVED and ((( StartDate <= :startTime)  and (EndDate > :startTime ))or ((StartDate < :endTime) and (EndDate >= :endTime)))];
        Map<id,List<Workorder>> wrkMapUsrId = new Map<id,List<Workorder>>();
        for(id us:usrIDSet)
        {
           List<Holiday__c> holi=new List<Holiday__c>();
           List<Workorder> wrk=new List<Workorder>();
           if(holiList!=null)
           {           
               for(Holiday__c ho:holiList)
               {
                 if(ho.User__c==us)
                   holi.add(ho);
               }   
               holiMapUsrId.put(us,holi);
           }
             if(wrkList!=null)
           {           
               for(Workorder wo:wrkList)
               {
                 if(wo.OwnerId==us)
                   wrk.add(wo);
               }   
               wrkMapUsrId.put(us,wrk);
           }
        }

        Set<id> userIdToPass = new set<Id>();
        for(id ui:usrIDSet)
        {
            if((holiMapUsrId.get(ui)==null || holiMapUsrId.get(ui).size()==0) && (wrkMapUsrId.get(ui)==null || wrkMapUsrId.get(ui).size()==0))
            {
               userIdToPass.add(ui); 
            }
        }
        userOptions.add(new SelectOption('-None-','-None-'));
        List<User> usrList=[select id,username,name,Vendor_Account_name__c from User where id=:userIdToPass and ((Vendor_Capability__c Includes(:cas.Problem__r.Skills__c)) OR (Vendor_Capability__c Includes('Generalist'))) and ((Account.Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_INHOUSE) or (Account.Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_3RD_PARTY))];
        System.debug('******USERLIST****'+usrList);
        if(!usrList.isEmpty())
        {
            for(User us:usrList)
            {
                userOptions.add(new SelectOption(us.name,us.name+' - ('+us.Vendor_Account_name__c+')'));
            }
        }
        Vender();
    } 

/***************************************************************************************************************************************/

    public void Vender() 
    {
        venderOptions = new List<SelectOption>();
        List<Zone_and_OM_Mapping__c> znMapList= [select User__c from Zone_and_OM_Mapping__c where Zone_Code__c=:cas.Zone_Code__c and isActive__c=true and Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_VENDOR];
        Set<id> usrIDSet = new Set<id>();
        for(Zone_and_OM_Mapping__c zm:znMapList)
        {
            usrIDSet.add(zm.User__c);  
        } 
        List<User> usrList=[select id,username,name from User where id=:usrIDSet and (Vendor_Capability__c Includes(:cas.Problem__r.Skills__c) OR Vendor_Capability__c Includes('Generalist')) and (Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_3RD_PARTY_AGGREGUTERS)];
        System.debug('*******Vender USer***'+usrList);
        venderOptions.add(new SelectOption('-None-','-None-'));
        if(!usrList.isEmpty())
        {
            for(User us:usrList)
            {
                venderOptions.add(new SelectOption(us.name,us.name));
            }
         }   
    }

/****************************************************************************************************************************************************/
     
     public void VenderWrkCount()
     {  if(vendername!=null)
        {  System.debug('*****Count*******');
                    User us = [select id,name from user where name=:vendername];
                    List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:us.id and ((( StartDate <= :startTime)  and (EndDate > :startTime ))or ((StartDate < :endTime) and (EndDate >= :endTime)))];
                    
                    if(wrkList!=null || !wrkList.isEmpty())
                    {
                      wrkCount=wrkList.size();
                    }
                    else
                    {   
                       wrkCount=0;
                    }
        }
        else
        wrkCount=0;

        System.debug(vendername+'**********count*****'+wrkCount);
     }

/*************************************************************************************************************************************/

    public pagereference InsertWorkorder()
    { 
        try
        {     
                if(username==null && vendername==null)
                {
                  addErrorMessage='Whom to Assign Vender or Technician is Empty.';
                }
                if(startTime==null || endTime==null)
                {
                   addErrorMessage='StartTime and EndTime Cannot be bkank.'; 
                }
                 if(startTime > endTime)
                {
                   addErrorMessage='StartTime should be smaller then EndTime.'; 
                }
                id recordTypeVendor= Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
                User assignTO =new User();
                workorder wrk=new workorder();
                wrk.StartDate=startTime;
                wrk.EndDate=endTime;
                wrk.Tenant__c=cas.Accountid;
                wrk.Caseid=cas.id;
                wrk.RecordTypeId=recordTypeVendor;
                wrk.subject=cas.subject;
                wrk.Bill_To__c=billTo;
                wrk.Description=cas.Description;
                wrk.House__c=cas.House1__c;
                if(assintovender==false)
                {
                    assignTO=[Select id from user where name=:username];
                }
                else
                {
                    assignTO=[Select id from user where name=:vendername];
                }
                 System.debug('******Bony1******'+assignTO);
                 wrk.ownerid=assignTO.id;
                 insert wrk;
                 cas.Service_Visit_Time__c=startTime;
                 update cas;
                 PageReference parentPage;
                if(cas!=null){
                    parentPage = new PageReference('/' + cas.Id);
                    parentPage.setRedirect(true);
                    return parentPage;
                } 
        }
        catch (Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
        }
        return null;
    }
     public pagereference cancel(){
       
        PageReference parentPage;
        if(cas!=null){
            parentPage = new PageReference('/' + cas.Id);
            parentPage.setRedirect(true);
            return parentPage;
        }
        return null;
    }
}