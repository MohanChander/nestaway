/*************************************************************************************************************************
Added By  Baibhav
Purpose: To Stop record from Deleting except Admin
Created on: 07-09-2017
*************************************************************************************************************************/
public with sharing class CannotDelete 
{
	public static void onlyAdmin(List<Sobject> sobjList)
	{
		Profile pro=[Select id, Name from Profile where Name='System Administrator'];
		
	  for(Sobject ob:sobjList)
	  {
	  	If(UserInfo.getProfileId()!=pro.id)
	  	  {
	        ob.addError('****** Only Admin can Delete This Record ********');
	  	  }
	  }
	}
}