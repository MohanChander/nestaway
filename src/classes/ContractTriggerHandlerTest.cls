@isTest
public Class ContractTriggerHandlerTest{
    
    public Static TestMethod void contractTest(){
        Test.StartTest();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        insert quoObj;
        
        Id recordTypeId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        //houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'Draft';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.Opportunity__c = opp.Id;
        //houseIC1.House_Layout__c = '1 R';
        houseIC1.Status__c = 'Draft';           
        houseList.add(houseIC1);
        
        insert houseList;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.SD_Upfront_Amount__c = 10;
        cont.status = 'Draft';
        insert cont;
        
        
        cont.Status = 'Final Contract';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        update cont;
        
        quoObj.Status = 'To be Revised';
        update quoObj;
        
        cont.status = 'To be Revised';
        cont.SD_Upfront_Amount__c = 11;
        update cont;
        
        cont.status = 'Cancelled';
        update cont;
        
        Test.StopTest();
        
    }
      public Static TestMethod void contractTest1(){
        Test.StartTest();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        insert quoObj;
        
        Id recordTypeId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        //houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'Draft';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.Opportunity__c = opp.Id;
        //houseIC1.House_Layout__c = '1 R';
        houseIC1.Status__c = 'Draft';           
        houseList.add(houseIC1);
        
        insert houseList;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.SD_Upfront_Amount__c = 10;
        cont.status = 'Draft';
        insert cont;
         Map<id,contract> oldMap= new Map<id,contract>();
        oldMap.put(cont.id,cont);
        
        cont.Status = 'Final Contract';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        cont.Approval_Status__c = 'Awaiting HRM Approval';	 
        update cont;
        Map<id,contract> newMap= new Map<id,contract>();
        newMap.put(cont.id,cont);  
          ContractTriggerHandler.shareAccountAndContract(newMap,oldMap);
          
       /* quoObj.Status = 'To be Revised';
        update quoObj;
        
        cont.status = 'To be Revised';
        cont.SD_Upfront_Amount__c = 11;
        update cont;
        
        cont.status = 'Cancelled';
        update cont;
        
        Test.StopTest(); */
        
    }

    public Static TestMethod void contractTest2()
    {

        Test.StartTest();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;

        user us=Test_library.createStandardUser(1);
        us.Firstname='qwerty';
        us.lastname = 'kur';
        us.CommunityNickname='krqwerty';
        us.Email='qwerty@gamil.com';
        us.Username='qwerty@gamil.com';
        insert us;
       
       user us1 = Test_library.createStandardUser(1);
        us1.Firstname ='abc';
       us1.lastname = 'kur';
        us1.CommunityNickname='krabc';
        us1.Email='abc@gamil.com';
        us1.City_Manager__c=us.id;
        us1.Username='abc123qwe@gamil.com';
        insert us1;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.ownerid=us1.id;
        insert opp;
        
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        insert quoObj;
        
        Id recordTypeId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        //houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'Draft';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.Opportunity__c = opp.Id;
        //houseIC1.House_Layout__c = '1 R';
        houseIC1.Status__c = 'Draft';           
        houseList.add(houseIC1);
        insert houseList;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.SD_Upfront_Amount__c = 10;
        cont.status = 'Draft';
        cont.Opportunity__c=opp.id;
        insert cont;
         Map<id,contract> oldMap= new Map<id,contract>();
        oldMap.put(cont.id,cont);
        
        cont.Status = 'Final Contract';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        cont.Approval_Status__c = 'Awaiting HRM Approval';   
        update cont;
        Map<id,contract> newMap= new Map<id,contract>();
        newMap.put(cont.id,cont);  
          ContractTriggerHandler.shareAccountAndContract(newMap,oldMap);
          
       /* quoObj.Status = 'To be Revised';
        update quoObj;
        
        cont.status = 'To be Revised';
        cont.SD_Upfront_Amount__c = 11;
        update cont;
        
        cont.status = 'Cancelled';
        update cont;
        
        Test.StopTest(); */
        
    }
        public Static TestMethod void contractTest3()
   {

        Test.StartTest();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;

        user us=Test_library.createStandardUser(1);
        us.Firstname='qwerty12';
        us.lastname = 'kur';
        us.CommunityNickname='krqwerty';
        us.Email='qwerty12@gamil.com';
        us.Username='qwerty@gamil.com';
        insert us;
       
       user us1 = Test_library.createStandardUser(1);
        us1.Firstname ='abc';
       us1.lastname = 'kur';
        us1.CommunityNickname='krabc';
        us1.Email='abc12@gamil.com';
        us1.City_HRM__c=us.id;
        us1.Username='abc12312qwe@gamil.com';
        insert us1;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.ownerid=us1.id;
        insert opp;
        
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        insert quoObj;
        
        Id recordTypeId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        //houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'Draft';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.Opportunity__c = opp.Id;
        //houseIC1.House_Layout__c = '1 R';
        houseIC1.Status__c = 'Draft';           
        houseList.add(houseIC1);
        insert houseList;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.SD_Upfront_Amount__c = 10;
        cont.status = 'Draft';
        cont.Opportunity__c=opp.id;
        insert cont;
         Map<id,contract> oldMap= new Map<id,contract>();
        oldMap.put(cont.id,cont);
        
        cont.Status = 'Final Contract';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        cont.Approval_Status__c = 'Awaiting HRM Approval';   
        update cont;
        Map<id,contract> newMap= new Map<id,contract>();
        newMap.put(cont.id,cont);  
          ContractTriggerHandler.shareAccountAndContract(newMap,oldMap);
          
       /* quoObj.Status = 'To be Revised';
        update quoObj;
        
        cont.status = 'To be Revised';
        cont.SD_Upfront_Amount__c = 11;
        update cont;
        
        cont.status = 'Cancelled';
        update cont;
        
        Test.StopTest(); */
        
    }
    public Static TestMethod void contractTest4()
       {

        Test.StartTest();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;

        user us=Test_library.createStandardUser(1);
        us.Firstname='qwerty123';
        us.lastname = 'kur';
        us.CommunityNickname='krqwerty';
        us.Email='qwerty123@gamil.com';
        us.Username='qwerty123@gamil.com';
        insert us;
       
       user us1 = Test_library.createStandardUser(1);
        us1.Firstname ='abc';
       us1.lastname = 'kur';
        us1.CommunityNickname='krabc';
        us1.Email='abc123@gamil.com';
        us1.Document_Reviewer__c=us.id;
        us1.Username='abc123123qwe@gamil.com';
        insert us1;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.ownerid=us1.id;
        insert opp;
        
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        insert quoObj;
        
        Id recordTypeId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        //houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'Draft';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.Opportunity__c = opp.Id;
        //houseIC1.House_Layout__c = '1 R';
        houseIC1.Status__c = 'Draft';           
        houseList.add(houseIC1);
        insert houseList;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.SD_Upfront_Amount__c = 10;
        cont.status = 'Draft';
        cont.Opportunity__c=opp.id;
        insert cont;
         Map<id,contract> oldMap= new Map<id,contract>();
        oldMap.put(cont.id,cont);
        
        cont.Status = 'Final Contract';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        cont.Approval_Status__c = 'Awaiting HRM Approval';   
        update cont;
        Map<id,contract> newMap= new Map<id,contract>();
        newMap.put(cont.id,cont);  
          ContractTriggerHandler.shareAccountAndContract(newMap,oldMap);
          
       /* quoObj.Status = 'To be Revised';
        update quoObj;
        
        cont.status = 'To be Revised';
        cont.SD_Upfront_Amount__c = 11;
        update cont;
        
        cont.status = 'Cancelled';
        update cont;
        
        Test.StopTest(); */
        
    }
        public Static TestMethod void contractTest5()
       {

        Test.StartTest();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;

        user us=Test_library.createStandardUser(1);
        us.Firstname='qwerty123';
        us.lastname = 'kur';
        us.CommunityNickname='krqwerty';
        us.Email='qwerty1234@gamil.com';
        us.Username='qwerty1234@gamil.com';
        insert us;
       
       user us1 = Test_library.createStandardUser(1);
        us1.Firstname ='abc';
       us1.lastname = 'kur';
        us1.CommunityNickname='krabc';
        us1.Email='abc1234@gamil.com';
        us1.Managerid=us.id;
        us1.Username='abc1231234qwe@gamil.com';
        insert us1;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.ownerid=us1.id;
        insert opp;
        
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        insert quoObj;
        
        Id recordTypeId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        //houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'Draft';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.Opportunity__c = opp.Id;
        //houseIC1.House_Layout__c = '1 R';
        houseIC1.Status__c = 'Draft';           
        houseList.add(houseIC1);
        insert houseList;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.SD_Upfront_Amount__c = 10;
        cont.status = 'Draft';
        cont.Opportunity__c=opp.id;
        insert cont;
         Map<id,contract> oldMap= new Map<id,contract>();
        oldMap.put(cont.id,cont);
        
        cont.Status = 'Final Contract';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        cont.Approval_Status__c = 'Awaiting HRM Approval';   
        update cont;
        Map<id,contract> newMap= new Map<id,contract>();
        newMap.put(cont.id,cont);  
          ContractTriggerHandler.shareAccountAndContract(newMap,oldMap);
          
       /* quoObj.Status = 'To be Revised';
        update quoObj;
        
        cont.status = 'To be Revised';
        cont.SD_Upfront_Amount__c = 11;
        update cont;
        
        cont.status = 'Cancelled';
        update cont;
        
        Test.StopTest(); */
        
    }
     public Static TestMethod void contractTest6()
       {

        Test.StartTest();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;

        user us=Test_library.createStandardUser(1);
        us.Firstname='qwerty123';
        us.lastname = 'kur';
        us.CommunityNickname='krqwerty';
        us.Email='qwerty12345@gamil.com';
        us.Username='qwerty12345@gamil.com';
        insert us;
       
       user us1 = Test_library.createStandardUser(1);
        us1.Firstname ='abc';
       us1.lastname = 'kur';
        us1.CommunityNickname='krabc';
        us1.Email='abc12345@gamil.com';
        us1.Item_Manager__c=us.id;
        us1.Username='abc12312345qwe@gamil.com';
        insert us1;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.ownerid=us1.id;
        insert opp;
        
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        insert quoObj;
        
        Id recordTypeId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        //houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'Draft';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.Opportunity__c = opp.Id;
        //houseIC1.House_Layout__c = '1 R';
        houseIC1.Status__c = 'Draft';           
        houseList.add(houseIC1);
        insert houseList;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.SD_Upfront_Amount__c = 10;
        cont.status = 'Draft';
        cont.Opportunity__c=opp.id;
        insert cont;
         Map<id,contract> oldMap= new Map<id,contract>();
        oldMap.put(cont.id,cont);
        
        cont.Status = 'Final Contract';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        cont.Approval_Status__c = 'Awaiting HRM Approval';   
        update cont;
        Map<id,contract> newMap= new Map<id,contract>();
        newMap.put(cont.id,cont);  
          ContractTriggerHandler.shareAccountAndContract(newMap,oldMap);
          
       /* quoObj.Status = 'To be Revised';
        update quoObj;
        
        cont.status = 'To be Revised';
        cont.SD_Upfront_Amount__c = 11;
        update cont;
        
        cont.status = 'Cancelled';
        update cont;
        
        Test.StopTest(); */
        
    }
}