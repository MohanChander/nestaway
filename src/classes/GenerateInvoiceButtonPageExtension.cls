global class GenerateInvoiceButtonPageExtension {

	global Case c {get; set;}
    global boolean stop {get; set;}

	global GenerateInvoiceButtonPageExtension(ApexPages.StandardController stdController){
		this.c = (Case)stdController.getRecord();
		Id genTaskRT = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_VERIFICATION_TASK).getRecordTypeId();
		list<task> tskList=[Select id,status from task where whatId=:this.c.id and RecordTypeId=:genTaskRT 
							and status!=:Constants.TASK_STATUS_VERIFED];

		Case caseObj = [select Id, Material_Cost__c, Labour_Cost__c, Tenant_Material_Cost__c, Owner_Material_Cost__c, 
						NestAway_Material_Cost__c, Tenant_Labor_Cost__c, Owner_Labor_Cost__c, NestAway_Labor_Cost__c,
						Recoverable_From__c
						from Case where Id =: c.Id];

		this.stop=true;

		//Check for Split
		if(caseObj.Recoverable_From__c == 'Tenant'){
			if(caseObj.NestAway_Labor_Cost__c > 0 || caseObj.NestAway_Material_Cost__c > 0){
				this.stop=false;
				String addErrorMessage='You cannot Split the Share with NestAway when Recoverable From is Tenant';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 				
			}

			if(caseObj.Owner_Labor_Cost__c > 0 || caseObj.Owner_Material_Cost__c > 0){
				this.stop=false;
				String addErrorMessage='You cannot Split the Share with Owner when Recoverable From is Tenant';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 				
			}			
		}

		if(caseObj.Recoverable_From__c == 'Owner'){
			if(caseObj.NestAway_Labor_Cost__c > 0 || caseObj.NestAway_Material_Cost__c > 0){
				this.stop=false;
				String addErrorMessage='You cannot Split the Share with NestAway when Recoverable From is Owner';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 				
			}

			if(caseObj.Tenant_Labor_Cost__c > 0 || caseObj.Tenant_Material_Cost__c > 0){
				this.stop=false;
				String addErrorMessage='You cannot Split the Share with Owner when Recoverable From is Owner';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 				
			}			
		}	

		if(caseObj.Recoverable_From__c == 'Tenant & Owner'){
			if(caseObj.NestAway_Labor_Cost__c > 0 || caseObj.NestAway_Material_Cost__c > 0){
				this.stop=false;
				String addErrorMessage='You cannot Split the Share with NestAway when Recoverable From is Tenant & Owner';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 				
			}		
		}

		if(caseObj.Recoverable_From__c == 'Tenant & NestAway'){
			if(caseObj.Owner_Labor_Cost__c > 0 || caseObj.Owner_Material_Cost__c > 0){
				this.stop=false;
				String addErrorMessage='You cannot Split the Share with Owner when Recoverable From is Tenant & NestAway';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 				
			}		
		}

		if(caseObj.Recoverable_From__c == 'Owner & NestAway'){
			if(caseObj.Tenant_Labor_Cost__c > 0 || caseObj.Tenant_Material_Cost__c > 0){
				this.stop=false;
				String addErrorMessage='You cannot Split the Share with Tenant when Recoverable From is Owner & NestAway';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 				
			}		
		}								

		//Check if the Material Cost is split accurately
		if(caseObj.Material_Cost__c != caseObj.Owner_Material_Cost__c + caseObj.Tenant_Material_Cost__c + caseObj.NestAway_Material_Cost__c){
			this.stop=false;
			String addErrorMessage='Please check the Material Cost split. It does not match the Material Cost ' + caseObj.Owner_Material_Cost__c;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 
		}

		//Check if the Labour Cost is split accurately
		if(caseObj.Labour_Cost__c != caseObj.Owner_Labor_Cost__c + caseObj.Tenant_Labor_Cost__c + caseObj.NestAway_Labor_Cost__c){
			this.stop=false;
			String addErrorMessage='Please check the Labour Cost split. It does not match the Labour Cost ' + caseObj.Labour_Cost__c;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 
		}	

		if(tskList!=null && tskList.size()!=0)
		{   
			this.stop=false;
			String addErrorMessage='Please ensure that Verification Task is Verified before generating the Invoice';
			 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));   
		}
	}

    @RemoteAction
    global static Boolean sendInvoiceDetailsToWebApp(Id caseId,boolean Stop){

    	System.debug('**********************sendInvoiceDetailsToWebApp');

	    try{
	    	if(stop)
                {
		    		Case c = [select Id, CaseNumber, Free_Visit__c, Tenant_Labor_Cost__c, Service_visit_cost__c,
		    				  Express_Service_Cost__c, Tenant_Material_Cost__c, Owner_Labor_Cost__c, Owner_Material_Cost__c,
		    				  NestAway_Labor_Cost__c, NestAway_Material_Cost__c from Case where Id =: caseId];

		    		List<String> imageURLList = new List<String>();
					HttpRequest req = new HttpRequest();
			        
			        List<NestAway_End_Point__c> nestURL = NestAway_End_Point__c.getall().values();       
			        String authToken=nestURL[0].Webapp_Auth__c;
			        String EndPoint = nestURL[0].Invoice_Generation_API__c +'&auth='+authToken;
			        
			        System.debug('***EndPoint  '+EndPoint);

			        List<WorkOrder> woList = [select Id, (SELECT Id, Name FROM Attachments) 
			        						  from WorkOrder where CaseId =: c.Id and Status =: Constants.WORKORDER_STATUS_RESOLVED
			        						  limit 1];
			        for(WorkOrder wo: woList){
			        	for(Attachment att: wo.Attachments){
			        		String imgURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=' + att.Id;
			        		imageURLList.add(imgURL);
			        	}
			        }
			        
			        string jsonBody='';
			        WBMIMOJSONWrapperClasses.ServiceRequestInvoiceJSON invoiceObject = new WBMIMOJSONWrapperClasses.ServiceRequestInvoiceJSON();
			        invoiceObject.ticket_number = c.CaseNumber;
			        invoiceObject.free_visit = c.Free_Visit__c;
			        invoiceObject.tenant_labour_cost = c.Tenant_Labor_Cost__c;
			        invoiceObject.service_visit_cost = c.Service_visit_cost__c;
			        invoiceObject.express_service_cost = c.Express_Service_Cost__c;
			        invoiceObject.tenant_material_cost = c.Tenant_Material_Cost__c;
			        invoiceObject.owner_labour_cost = c.Owner_Labor_Cost__c;
			        invoiceObject.owner_material_cost = c.Owner_Material_Cost__c;
			        invoiceObject.nestaway_labour_cost = c.NestAway_Labor_Cost__c;
			        invoiceObject.nestaway_material_cost = c.NestAway_Material_Cost__c;	     

			        WBMIMOJSONWrapperClasses.AttachmentsObject attObj = new WBMIMOJSONWrapperClasses.AttachmentsObject();
			        attObj.material_related = imageURLList;

			        invoiceObject.attachments = attObj;

			        jsonBody = JSON.serialize(invoiceObject);
			        System.debug('***jsonBody  '+jsonBody);
			        
			        HttpResponse res;
			        res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
			        
			        String respBody = res.getBody();
			        system.debug('****respBody'+ respBody);
			            
			        if(res.getStatusCode() == 200) {

			        	//insert Case if response is 200
			        	Case updatedCase = new Case();
			        	updatedCase.Id = caseId; 
			        	updatedCase.Invoice_Generated__c = true;
			        	update updatedCase;
			        	return true;
			        } else{
			        	return true;
			        }
			    }
			    else
			    {
			      return true;	
			    }
	    }
	    catch (Exception e){
	    	System.debug('*******************There is an exception');
	        UtilityClass.insertGenericErrorLog(e, 'Invoice Generation API'); 
	        return false;   
	    }    	    	
    }    	
}