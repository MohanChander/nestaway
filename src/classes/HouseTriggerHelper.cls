/*  Created By   : Mohan - WarpDrive Tech Works
Purpose      : Helper Methods for HouseTriggerHandler */

public class HouseTriggerHelper {
    
    
    /*Initiate Offboarding API SYNC to NA */
    @future(CallOut=True)
    public static void initiateOffBoarding(Id house_Sf_Id) {
        try {
            
            System.debug('****initiateOffBoarding' +house_Sf_Id);
            
            HttpRequest req = new HttpRequest();
            
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
            String authToken=nestURL[0].Webapp_Auth__c;
            String EndPoint = nestURL[0].Initiate_Offboarding_API__c +'&auth='+authToken;
            
            System.debug('***EndPoint  '+EndPoint);
            
            string jsonBody='';
            WBMIMOJSONWrapperClasses.SendHouseSfID initateofboardingreq = new WBMIMOJSONWrapperClasses.SendHouseSfID();
            initateofboardingreq.sf_id = String.valueOf(house_Sf_Id);
            
            jsonBody = JSON.serialize(initateofboardingreq);
            System.debug('***jsonBody  '+jsonBody);
            
            HttpResponse res;
            res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
            
            String respBody = res.getBody();
            system.debug('****respBody'+ respBody);
            house__c h = new house__c();
            h.id = house_Sf_Id;
            
            if(res.getStatusCode() == 200) {
                h.Initiate_API_Message__c = (respBody);
                h.Initiate_API_Success__c  = String.valueOf(res.getStatusCode())+' - True';
            }
            else{
                h.Initiate_API_Message__c = (respBody);
                h.Initiate_API_Success__c  = String.valueOf(res.getStatusCode())+' - False';
            }    
            
            System.debug('**** house to be updated '+ h);
            StopRecursion.HouseSwitch = false;
            update h; 
            StopRecursion.HouseSwitch = true;
            System.debug('**** house after be updated '+h);            
        }
        catch (Exception e){
            
            house__c h = new house__c();
            h.id = house_Sf_Id;
            h.Initiate_API_Message__c = String.valueOf(e);
            h.Initiate_API_Success__c  = ' False';
            System.debug('**** house to be updated '+h);
            StopRecursion.HouseSwitch = false;
            update h; 
            StopRecursion.HouseSwitch = true;
            System.debug('**** house after be updated '+h);
            System.debug('******Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e);    
        }
        
    }
    
    /*house offborded on Sf ->  API callout to SYNC to NA */
    @future(CallOut=True)
    public static void houseOffBoarded(Id house_Sf_Id,String offboarding_reason, String offboarded_by ) {
        try {
            
            System.debug('****houseOffBoarded' +' --- '+ house_Sf_Id+offboarding_reason+' --- '+offboarded_by);
            
            HttpRequest req = new HttpRequest();
            
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
            String authToken=nestURL[0].Webapp_Auth__c;
            String EndPoint = nestURL[0].House_Offboarderd_API__c +'&auth='+authToken;
            
            System.debug('***EndPoint  '+EndPoint);
            
            string jsonBody='';
            WBMIMOJSONWrapperClasses.HouseJsonWhenOffboarded house_offboardedApireq = new WBMIMOJSONWrapperClasses.HouseJsonWhenOffboarded();
            house_offboardedApireq.sf_id = String.valueOf(house_Sf_Id);
            house_offboardedApireq.offboarding_reason = String.valueOf(offboarding_reason);
            house_offboardedApireq.offboarded_by = String.valueOf(offboarded_by);
            house_offboardedApireq.offboarded_datetime = String.valueOf(System.now());
            
            jsonBody = JSON.serialize(house_offboardedApireq);
            System.debug('***jsonBody  '+jsonBody);
            
            HttpResponse res;
            res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
            
            String respBody = res.getBody();
            system.debug('****respBody'+ respBody);
            house__c h = new house__c();
            h.id = house_Sf_Id;
            
            if(res.getStatusCode() == 200) {
                h.Offboarded_API_Message__c = (respBody);
                h.Offboarded_API_Success__c   = String.valueOf(res.getStatusCode())+' - True';
            }
            else{
                h.Offboarded_API_Message__c  = (respBody);
                h.Offboarded_API_Success__c   = String.valueOf(res.getStatusCode())+' - False';
            }    
            
            System.debug('**** house to be updated '+ h);
            StopRecursion.HouseSwitch = false;
            update h; 
            StopRecursion.HouseSwitch = true;
            System.debug('**** house after be updated '+h);            
        }
        catch (Exception e){
            
            house__c h = new house__c();
            h.id = house_Sf_Id;
            h.Offboarded_API_Message__c  = String.valueOf(e);
            h.Offboarded_API_Success__c  = ' False';
            System.debug('**** house to be updated '+h);
            StopRecursion.HouseSwitch = false;
            update h; 
            StopRecursion.HouseSwitch = true;
            System.debug('**** house after be updated '+h);
            System.debug('******Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e);    
        }
        
    }
    
    /*  Created By : Mohan - WarpDrive Tech Works
purpose    : Update the Room Status and Bed Status associated with House to Available when House is made live */
    public static void updateRoomAndBedStatus(Set<Id> houseIdSet){
        
        try{
            List<House__c> houseList = HouseSelector.getHousesWithRoomsAndBeds(houseIdSet);
            List<Room_Terms__c> roomList = new List<Room_Terms__c>();
            List<Bed__c> bedList = new List<Bed__c>();
            
            if(!houseList.isEmpty()){
                for(House__c house: houseList){
                    //update Room Status related to the House
                    for(Room_Terms__c room: house.Room_Terms__r){
                        Integer i=0;
                        System.debug('************'+room);
                        room.Status__c = Constants.ROOM_TERM_STATUS_AVAILABLE;
                        room.Listing__c = 'On';
                        roomList.add(room);
                    }
                    
                    //update Bed Status related to the House
                    for(Bed__c bed: house.Beds__r){
                        bed.Status__c = Constants.BED_STATUS_AVAILABLE;
                        bedList.add(bed);
                    }
                }
            }   
            
            if(!roomList.isEmpty()) 
                update roomList;
            
            if(!bedList.isEmpty())
                BedTriggerHandler.checkrec = false;
            update bedList;     
            
        } Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);              
        }
    } 
    
/*********************************************
    Created By : Mohan
    Purpose    : Update the Room, Room Inspection, Bathroom Names when House Id is populated on House
                 Invoked from HouseJson and beforeUpdate
*********************************************/                 
    public static void updateRoomAndBedAndRIC(List<House__c> newHouseList){
        try{
            System.debug('*********************updateRoomAndBedAndRIC');

            System.debug('***************newHouseList: ' + newHouseList + '\n Size of newHouseList: ' + newHouseList.size());

            Set<Id> houseIdSet = new Set<Id>();
            for(House__c house: newHouseList){
                houseIdSet.add(house.Id);
            }

            System.debug('********************houseIdSet'+houseIdSet);

            List<Room_Terms__c> roomTermList = new List<Room_Terms__c>();
            List<Bathroom__c> bathroomList = new List<Bathroom__c>();
            List<Room_Inspection__c> roomInspectionList = new List<Room_Inspection__c>();

            List<Room_Terms__c> rtList = [select Id, Name, House__c from Room_Terms__c where House__c =: houseIdSet];
            List<Room_Inspection__c> ricList = [select Id, Name, House__c, House_Inspection_Checklist__r.House__c
                                                from Room_Inspection__c where 
                                                House_Inspection_Checklist__r.House__c =: houseIdSet];
            List<Bathroom__c> bicList = [select Id, Name, House__c from Bathroom__c where House__c =: houseIdSet];

            System.debug('***************rtList: ' + rtList + '\n Size of rtList: ' + rtList.size());  
            System.debug('***************ricList: ' + ricList + '\n Size of ricList: ' + ricList.size());  
            System.debug('***************bicList: ' + bicList + '\n Size of bicList: ' + bicList.size());             

            Map<Id, List<Room_Terms__c>> rtMap = new Map<Id, List<Room_Terms__c>>();
            Map<Id, List<Room_Inspection__c>> ricMap = new Map<Id, List<Room_Inspection__c>>();
            Map<Id, List<Bathroom__c>> bicMap = new Map<Id, List<Bathroom__c>>();

            for(Room_Terms__c rt: rtList){
                if(rtMap.containsKey(rt.House__c)){
                    rtMap.get(rt.House__c).add(rt);
                } else {
                    rtMap.put(rt.House__c, new List<Room_Terms__c>{rt});
                }
            }

            for(Room_Inspection__c ric: ricList){
                if(ricMap.containsKey(ric.House_Inspection_Checklist__r.House__c)){
                    ricMap.get(ric.House_Inspection_Checklist__r.House__c).add(ric);
                } else {
                    ricMap.put(ric.House_Inspection_Checklist__r.House__c, new List<Room_Inspection__c>{ric});
                }
            }    
            
            for(Bathroom__c bic: bicList){
                if(bicMap.containsKey(bic.House__c)){
                    bicMap.get(bic.House__c).add(bic);
                } else {
                    bicMap.put(bic.House__c, new List<Bathroom__c>{bic});
                }
            }    

            System.debug('***************rtMap: ' + rtMap + '\n Size of rtMap: ' + rtMap.size());    
            System.debug('***************ricMap: ' + ricMap + '\n Size of ricMap: ' + ricMap.size());
            System.debug('***************bicMap: ' + bicMap + '\n Size of bicMap: ' + rtMap.size());                        

            for(House__c house: newHouseList){
                if(house.HouseId__c != null){
                    if(rtMap.containsKey(house.Id)){
                        for(Room_Terms__c rt: rtMap.get(house.Id)){
                            rt.Name = rt.Name + ' - ' + house.HouseId__c;
                            roomTermList.add(rt);
                        }
                    }

                    if(ricMap.containsKey(house.Id)){
                        for(Room_Inspection__c ric: ricMap.get(house.Id)){
                            ric.Name = ric.Name + ' - ' + house.HouseId__c;
                            roomInspectionList.add(ric);
                        }
                    }

                    if(bicMap.containsKey(house.Id)){
                        for(Bathroom__c bic: bicMap.get(house.Id)){
                            bic.Name = bic.Name + ' - ' + house.HouseId__c;
                            bathroomList.add(bic);
                        }
                    }
                }
            }

            if(!roomTermList.isEmpty()){
                update roomTermList;
            }

            if(!bathroomList.isEmpty()){
                update bathroomList;
            }

            if(!roomInspectionList.isEmpty()){
                update roomInspectionList;
            } 
            
            for(House__c house: newHouseList){
                house.Updated_Child_Name_with_House_Id__c = true;
            }            
            
        } Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);              
        }
    } 


    // Method to validate House Live - Onboarding Workflow should be closed to Make House Live
    public static void checkOnboardingworkflow(List<House__c> houseList, Map<Id, House__c> newMap){
        
        System.debug('*************checkOnboardingworkflow');
        
        try{
            Set<Id> setOfHouse= new Set<Id>();
            List<House__c> houseList01 = new List<House__c>();    //Houses with Onboarding workflow info
            
            for(House__c  hc: houseList){
                setOfHouse.add(hc.Id);
            }
            
            houseList01 = HouseSelector.getHousesWithCaseDetails(setOfHouse);
            
            for(House__c house: houseList01){
                if(house.cases__r.size() == 0)
                    newMap.get(house.Id).addError('Please check the House does not have Onboarding Workflow associated');
                else{
                    for(Case c: house.cases__r){
                        if(c.Status != Constants.CASE_STATUS_CLOSED){
                            newMap.get(house.Id).addError('Please ensure that Onboarding Workflow is Closed to Make the House Live');
                        }
                    }
                }
            }
            
        } Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);              
        }
    } // end of
    
    /* Created By : Mohan - WarpDrive Tech Works
Purpose    : Validate Room Rent and Bed Rent for the House when Rent Verified Checkbox is checked */
    public static void verifyRentonHouse(Map<Id, House__c> houseMap, Map<Id, House__c> newMap){
        
        try{
            List<Room_Terms__c> roomList = RoomTermsSelector.getRoomTermWithRentDetails(houseMap.keySet());
            List<Bed__c> bedList = BedSelector.getBedsWithRent(houseMap.keySet());
            
            
            for(Room_Terms__c room: roomList){
                System.debug('**************roomList for Loop');
                if(room.Actual_Room_Rent__c == null || room.Actual_Room_Rent__c <= 0)
                    newMap.get(room.House__c).addError('Please verify Room Rent for ' + room.Name);
            }
            
            for(Bed__c bed: bedList){
                System.debug('**************bedList for Loop');
                if(bed.Actual_Rent__c == null || bed.Actual_Rent__c <= 0)
                    newMap.get(bed.House__c).addError('Actual Rent for the Bed ' + bed.Name + ' Cannot be empty');
            }
        } Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);                
        }
    }
    
    /*  Created By : Mohan
Purpose    : Populate Owner Email for Template on Houses */
    public static void populateOwnerEmailOnHouse(List<House__c> houseList){
        try{
            Set<Id> accountIdSet = new Set<Id>();
            
            for(House__c house: houseList){
                if(house.House_Owner__c != null)
                    accountIdSet.add(house.House_Owner__c);
            }
            
            Map<Id, Account> accountMap = new Map<Id, Account>(AccountSelector.getAccountDetailsFromSet(accountIdSet));
            
            for(House__c house: houseList){
                if(house.House_Owner__c != null && accountMap.containsKey(house.House_Owner__c))
                    house.Owner_Email_for_Template__c = accountMap.get(house.House_Owner__c).PersonEmail;
            }
               
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);                
        }
    }    
    
    /*   
Created by Baibhav - WarpDrive Tech Works

Purpsoe: To create Offboarding Case (onAfterUpdate of house)

*/    
    public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
    public static void houseOffboardingCaseCreation(List<House__c> houseList)
    {
        try
        {
            List<Case> caseListToCreate = new List<Case>(); 
            set<id> houseid = new Set<id>();
            Set<id>  ownerIdSet = new Set<Id>(); 
            Set<id>  conidSet =new Set<Id>();
            for(House__c hus:houseList)
            {
                houseid.add(hus.id);
                ownerIdSet.add(hus.House_Owner__c);
                conidSet.add(hus.Contract__c);
            }
            Map<id,Account> accountmap = new Map<id,Account>([Select id,name from account where id=:ownerIdSet]); 
            Map<Id, Contact> contactMap = new Map<Id, Contact>();
            
            List<Zone__c> zonelist = ZoneAndUserMappingSelector.getZoneRecordsForRecordTypeId(OffboardingZoneRTId);
            Map<String,Zone__c> zoneMapTocode = new  Map<String,Zone__c>();
            for(Zone__c zo:zonelist)
            {
                zoneMapTocode.put(zo.Zone_code__c,zo); 
            }         
            
            for(Contact con: ContactSelector.getContactsFromAccountIdSet(ownerIdSet)){
                contactMap.put(con.AccountId, con);
            }
            
            List<Case> caselist = CaseSelector.getCasesFromHouseIdSet(houseid);
            
            list<Contract> contList = ContractSelector.getContractsWithHouseStatus(conidSet);
            Map<id,Contract> contractMaphouseid =  new  Map<id,Contract>();
            Map<id,Contract> contractMapid =  new  Map<id,Contract>();
            
            for(Contract cont:contList)
            {
                contractMapid.put(cont.id,cont); 
            }
            for(House__c hus:houseList)
            {
                contractMaphouseid.put(hus.id,contractMapid.get(hus.Contract__c));
            }
            
            // Cont No of Tanents
            
            list<Bed__c> bedList = BedSelector.getBedsWithRent(houseid);
            Map<id,List<Bed__c>>  bedMaphouseid =  new  Map<id,List<Bed__c>> ();
            Map<id,Integer> bedcount = new Map<id,Integer>(); 
            if(bedList!=null)
            {
                for(Bed__c b:bedList)
                {
                    Integer n=0;
                    List<Bed__c> balist = new List<Bed__c>();
                    
                    if(bedMaphouseid.containsKey(b.house__c))
                    { 
                        balist=bedMaphouseid.get(b.house__c);
                    }
                    if(bedcount.containsKey(b.house__c))
                    {
                        n=bedcount.get(b.house__c);
                    }
                    balist.add(b);
                    if(b.Status__c == Constants.BED_STATUS_SOLD_OUT && b.Status__c !=null)
                    {
                        n=n+1;
                        bedcount.put(b.house__c,n);
                    }
                    bedMaphouseid.put(b.house__c,balist); 
                }
            }
            // End
            
            // Creatation Of CASE
            for(House__c hus:houseList)
            {
                Boolean occ=false;
                Boolean fur=false;
                
                for(Case cae:caselist)
                {
                    if(cae.HouseForOffboarding__c == hus.id && cae.Status == 'Open')
                        hus.addError('House Already contain Offboarding Case with Open Status');
                }
                
                if(bedMaphouseid.get(hus.id) != null)
                    for(Bed__c be:bedMaphouseid.get(hus.id))
                {
                    if(be.Status__c == Constants.BED_STATUS_SOLD_OUT)
                        occ=true;
                }
                
                Contract con = contractMaphouseid.get(hus.id);
                
                // Selcting the record type of Case
                
                if(con==null)
                {
                    hus.adderror(hus.name+' House Do not have contract, Cannot Initiate Offboarding');
                }
                
                if(con!=null && (con.Furnishing_Plan__c == Constants.FURNISHING_PLAN_NESTWAY_RENTAL ||
                                 con.Furnishing_Plan__c == Constants.FURNISHING_PLAN_NESTWAY))            
                    fur=true;
                
                SYStem.debug(con);
                Case cas = new Case();
                if(occ==true && fur==true)
                    cas.RecordTypeId = caseOccupiedFurnishedRecordtype;
                
                if(occ == true && fur == false)
                    cas.RecordTypeId = caseOccupiedUnfurnishedRecordtype;
                
                if(occ == false && fur == false)
                    cas.RecordTypeId = caseUnoccupiedUnfurnishedRecordtype;
                
                if(occ == false && fur == true)
                    cas.RecordTypeId = caseUnoccupiedFurnishedRecordtype;
                // end
                
                // System.debug(con.Furnishing_Type__c+'assd');
                if(con!=null)
                {
                    if(con.Furnishing_Type__c == 'fully-furnished'){
                        cas.Furnishing_Type__c = Constants.FURNISHING_CONDITION_FURNISHED;
                    } else if(con.Furnishing_Type__c == 'semifurnished'){
                        cas.Furnishing_Type__c = Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
                    } else {
                        cas.Furnishing_Type__c = con.Furnishing_Type__c;
                    }
                    cas.House_Id_For_Template__c= hus.HouseId__c;
                    cas.Furnishing_Plan__c = con.Furnishing_Plan__c;
                    cas.Contract__c = con.id;
                    cas.Booking_Type__c = con.Booking_Type__c;
                }
                
                System.debug('********BAIBAHV***********************'+bedcount.get(hus.id));
                cas.Active_Tenants__c = bedcount.get(hus.id);
                cas.Type = Constants.CASE_TYPE_OFFBOARDING;
                
                if(hus.Name != null && hus.houseid__c != null)
                    cas.subject = 'Offboarding Case - House: ' + hus.Name + ' ID:' + hus.houseid__c;
                else
                    cas.subject = 'Offboarding Case';
                
                
                if(hus.House_Owner__c != null){
                    cas.AccountId = hus.House_Owner__c;
                    cas.ContactId = contactMap.containsKey(hus.House_Owner__c) ? contactMap.get(hus.House_Owner__c).Id : null;
                }
                
                cas.Offboarding_Reason__c = hus.Offboarding_Reason__c;
                cas.HouseForOffboarding__c = hus.id; 
                cas.ZAM_Email_for_Template__c = hus.ZAM_Email__c;
                cas.ZAM_Name__c = hus.ZAM_Name__c;  
                cas.Move_Out_Date_of_Last_Tenant__c = System.today() + 30;
                
                if(hus.HRM__c != null){
                    cas.HRM_Email__c = hus.HRM_Email__c;
                    cas.HRM_Name__c = hus.HRM_Name__c;                 
                }  
                
                cas.Stage__c=Constants.CASE_STAGE_OWNER_RETENTION_CALL;
                cas.status = Constants.CASE_STATUS_OPEN;
                caseListToCreate.add(cas);
            }
            if(caseListToCreate!=null)
                insert caseListToCreate;
        }
        catch(Exception e)
        {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);                    
        }
    }
    
    /*  Created By : Mohan
Purpose    : Send call out to Offboarded API Web App*/
    public static void sendCalloutToOffboarded(Set<Id> houseIdSet, Map<Id, House__c> houseMap){
        
        Map<Id, Case> caseMap = new Map<Id, Case>();
        List<Case> caseList = CaseSelector.getOffboardedCase(houseIdSet);
        
        for(Case c: caselist){
            caseMap.put(c.HouseForOffboarding__c, c);
        }
        
        for(Id houseId: houseIdSet){
            if(!caseMap.containsKey(houseId)){
                houseMap.get(houseId).addError('You can Offboard the House only through Offboarding Case');
            } else {
                String OffboardedReason = caseMap.get(houseId).Offboarding_Reason__c;
                houseOffBoarded(houseId, OffboardedReason, UserInfo.getUserId());
            }
        }
    } 
    
    /**************************************************
Created By : Mohan
Purpose    : Cannot Change Initiate Offboarding to No when the Offboarding Case is not Closed
***************************************************/
    public static void validateInitateOffboarding(List<House__c> houseList){
        
        Set<Id> houseIdSet = new Set<Id>();
        Map<Id, Case> houseCaseMap = new Map<Id, Case>();
        
        for(House__c house: houseList){
            houseIdSet.add(house.Id);
        }
        
        List<Case> caseList = CaseSelector.getOffboardedCase(houseIdSet);
        
        for(Case c: caseList){
            houseCaseMap.put(c.HouseForOffboarding__c, c);
        }
        
        for(House__c house: houseList){
            if(houseCaseMap.containsKey(house.Id)){
                house.addError('You can only change the Initiate Offboarding to No via the Offboarding Case');
            }
        }
        
    } 
    
    /**************************************************
Created By : Baibahv
Purpose    : To cahnge room listing off
***************************************************/
    /*public static void doRoomListingOff(Set<Id> houseSet,Map<id,House__c> newMap){ 
try {
List<Room_Terms__c> roomList = RoomTermsSelector.getRoomTermWithRentDetails(houseSet);
for(Room_Terms__c room : roomList)
{
room.Listing__c='Off';
room.Stop_Room_Listing_Till__c= newMap.get(room.House__c).Stop_House_Listing_Till__c;
}
Update roomList;
}  catch(Exception e){
System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
UtilityClass.insertGenericErrorLog(e); 
}
}*/
    
    /*  *****************************************************************************************************************
Added by Baibhav
Created on 05-09-2017
Purpose:  To create Create SD payment work Order
*******************************************************************************************************************/
    public static id sdpaymentRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();
    
    public static void createSDPaymentWorkorder(List<House__c> houList)
    {   
        try
        { 
           
            Set<id> contractIds=new Set<id>(); 
            Map<Id,house__c> houseIdmapwithContract = new Map<Id,house__c>();
            for(House__c hou:houList)
            {
                if(hou.Contract__c!=null){
                    contractIds.add(hou.Contract__c);
                    houseIdmapwithContract.put(hou.Contract__c,hou);
                } 
                
            }
            
            List<contract> conList =ContractSelector.getContractsWithHouseStatus(contractIds);
            Set<Id> accId = new Set<Id>();
            for(Contract con:conList)
            {
                accId.add(con.AccountId);
            } 
            List<Bank_Detail__c> bnkList =  BankDetailSelector.getBankDetailByAccount(accId);
            Map<Id,Bank_Detail__c> bnkMapAccId =new Map<Id,Bank_Detail__c>();
            for(Bank_Detail__c bk:bnkList)
            {
                bnkMapAccId.put(bk.Related_Account__c,bk);
            }
            
            List<Workorder> wrklistToInsert = new List<Workorder>();
            for(Contract con:conList)
            {
                
                if(con.Who_pays_Deposit__c==Constants.CONTRATC_WHO_PAY_DEPOSIT_NESTAWAY && con.SD_Upfront_Amount__c!=null && con.SD_Upfront_Amount__c>0)
                {
                    
                    Workorder wrk = new Workorder();
                    
                    // if(con.Opportunity__c!=null)
                    //  {
                    //      wrk.Opportunity__c=con.Opportunity__c;
                    //  }
                    
                    if(houseIdmapwithContract.containsKey(con.id))
                    {
                        
                        wrk.House__c=houseIdmapwithContract.get(con.id).id;
                    }
                    
                    if(con.AccountId!=null)
                        wrk.AccountId=con.AccountId;
                    
                    if(bnkMapAccId.containsKey(con.AccountId))
                    {
                        if(bnkMapAccId.get(con.AccountId).id!=null)   
                            wrk.Bank_Detail__c=bnkMapAccId.get(con.AccountId).id;
                        
                        if(bnkMapAccId.get(con.AccountId).Account_Number__c!=null)
                            wrk.Account_Number__c=bnkMapAccId.get(con.AccountId).Account_Number__c;
                        
                        if(bnkMapAccId.get(con.AccountId).Bank_Name__c!=null)
                            wrk.Bank_Name__c=bnkMapAccId.get(con.AccountId).Bank_Name__c;
                        
                        if(bnkMapAccId.get(con.AccountId).IFSC_Code__c!=null)                 
                            wrk.IFSC_Code__c=bnkMapAccId.get(con.AccountId).IFSC_Code__c;
                        
                        if(bnkMapAccId.get(con.AccountId).Branch_Name__c!=null)                 
                            wrk.Branch_Name__c=bnkMapAccId.get(con.AccountId).Branch_Name__c;
                    }
                    if(con.Name__c!=null)
                        wrk.Agreement_Name__c=con.Name__c;
                    
                    if(con.Security_Deposit_Payment_Mode__c!=null)
                        wrk.Payment_mode__c=con.Security_Deposit_Payment_Mode__c;
                    
                    wrk.RecordTypeId=sdpaymentRecordType;
                    wrk.subject='Upfront SD';
                    wrk.Upfront_amount__c=con.SD_Upfront_Amount__c;
                    wrk.Security_Deposit_Payment_Details__c=con.Security_Deposit_Payment_Details__c; 
                    if(con.Opportunity__c!=null && con.Opportunity__r.OwnerId!=null && string.ValueOf(con.Opportunity__r.OwnerId).startsWith('005')){   
                      wrk.ownerId=con.Opportunity__r.OwnerId;
                    }
                  
                    wrklistToInsert.add(wrk);
                }
            }
            if(wrklistToInsert.size()>0)
            {
                System.debug('****bony5****'+ wrklistToInsert);
                insert wrklistToInsert ;
            }
        } Catch(Exception e)
        {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
         /***********************************************************************************************************************************************
    Added by Baibhav
    purpose: to update Available from field
    ***********************************************************************************************************************************************/
    
     public static void houseUpdateLive(List<House__c> houseList)
        {
            try
            {
                Set<Id> houIdSet = new Set<Id>();
                for(House__c hou:houseList)
                {
                    houIdSet.add(hou.Id);
                }
                List<Room_Terms__c> rtList=RoomTermsSelector.getRoomTermWithRentDetails(houIdSet);
                List<Bed__c> bedList=BedSelector.getBedsWithRent(houIdSet);
                Map<id, List<Room_Terms__c>> roomWithHousIDMap = new Map<id, List<Room_Terms__c>>();
                Map<id, List<Bed__c>> bathWithHousIDMap = new Map<id, List<Bed__c>>(); 
                for(Room_Terms__c rm:rtList)
                {
                     List<Room_Terms__c> rtSt = new List<Room_Terms__c>();
                     if(roomWithHousIDMap.containsKey(rm.house__c))
                     {
                       rtSt= roomWithHousIDMap.get(rm.house__c); 
                     }
                     rtSt.add(rm);
                     roomWithHousIDMap.put(rm.house__c,rtSt);
                }
                 for(Bed__c bt:bedList)
                  {
                     List<Bed__c> batSt = new List<Bed__c>();
                     if(bathWithHousIDMap.containsKey(bt.house__c))
                     {
                       batSt= bathWithHousIDMap.get(bt.house__c); 
                     }
                     batSt.add(bt);
                     bathWithHousIDMap.put(bt.house__c,batSt);
                }
                 List<Room_Terms__c> updaterotList= new List<Room_Terms__c>();
                List<Bed__c> updatebathList= new List<Bed__c>();
                 for(House__c hou:houseList)
                {
                    
                        hou.Available_from__c=System.Today();
                
                     List<Room_Terms__c> rlist=roomWithHousIDMap.get(hou.id);
                     if(rlist!=null)
                         {
                            for(Room_Terms__c r:rlist)
                                          {
                                             r.Available_from__c=System.Today();
                                             updaterotList.add(r);
                                         }
                         }
                     
                       List<Bed__c> blist=bathWithHousIDMap.get(hou.id);
                      if(blist!=null) 
                         {
                            for(Bed__c b:blist)
                                          {
                                             b.Available_from__c=System.Today();
                                             updatebathList.add(b);
                                          }
                        }
                }
                if(!updaterotList.isEmpty())
                  Update updaterotList;  

                if(!updatebathList.isEmpty())
                  Update updatebathList;         
           } Catch(Exception e)
             {
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
             }
        }
    // Added by baibhav
    // Purpose : to update Address from house
    public static void Oppupadte(list<House__c> holist)
    {   System.debug('*****QWER2****');
        set<id> oppidSet =new Set<id>();
       
        for(House__c ho:holist)
        {
          oppidSet.add(ho.Opportunity__c);
        } 
         Map<id,Opportunity> oppMap =OpportunitySelector.getOpportunitiesOppidSet(oppidSet);
          System.debug('*****QWER2****'+oppMap);
         list<Opportunity> updateoppList= new list<Opportunity>();
         for(House__c ho:holist)
         {
            Opportunity opp=oppMap.get(ho.Opportunity__c);
            opp.Street__c=ho.Street__c;
            opp.State__c=ho.State__c;
            opp.Pin_Code__c=ho.Pincode__c;
            opp.City__c=ho.City__c;
            opp.Country__c=ho.Country__c;
            opp.Door_Apartment_Number__c=ho.Door_Number__c;
            updateoppList.add(opp);
         }
         if(!updateoppList.isEmpty())
         { System.debug('*****QWER3****'+updateoppList);
            update updateoppList;
         }
     
    }  
}