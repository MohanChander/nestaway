/*  Created By : Mohan
    Purpose    : User Object related queries   */

public class UserSelector {

    //get user details
    public static List<User> getUserDetails(Set<Id> userIdSet){
        return [select Id, ManagerId,AccountID,email, Manager.Email from User where Id =: userIdSet];
    }

     public static Map<id,User> getUserDetailsMap(Set<Id> userIdSet){
        return new Map<id,User>([select Id, ManagerId,AccountID,email, Vendor_Type__c, Manager.Email from User where Id =: userIdSet]);
    }

    public static List<User> getAllUserDetails(){
        return [select Id, Manager.Email, ManagerId from User where isActive = true];
    }
}