/*  Created By : Mohan
	Purpose    : All the Queries related to WorkOrder Object are here */

public class WorkOrderSelector {

	//get WOLI from WorkOrders
	public static List<WorkOrder> getWorkOrdersWithWOLI(Set<Id> woIdSet){
		return [select Id, Status, (select Id, Status from WorkOrderLineItems) from WorkOrder
				where Id =: woIdSet];
	}

	public static List<WorkOrder> getTanentOffBoardingWorkOrders(Set<Id> houseIdSet){
		Id tanentoffboardingRecodTypeId = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_TENANT_MOVE_OUT).getRecordTypeId();
		return [select Id, Status, House__c from WorkOrder
				where house__c =:houseIdSet and RecordTypeId =:tanentoffboardingRecodTypeId and 
				Case.Status =: Constants.CASE_STATUS_OPEN];
	}

	public static List<WorkOrder> getWoWithChecklists(Set<Id> woIdSet){

		String hicString = UtilityServiceClass.getQueryString('House_Inspection_Checklist__c');
		String ricString = UtilityServiceClass.getQueryString('Room_Inspection__c');
		String bicString = UtilityServiceClass.getQueryString('Bathroom__c');
		String queryString = 'Select Id, CaseId, (' + hicString + ' from Checklist__r), ('  +
						      ricString + ' from Room_Inspection__r), (' +
						      bicString + ' from Bathrooms__r) from WorkOrder where Id =: woIdSet';
		List<WorkOrder> woList = (List<WorkOrder>)Database.Query(queryString);
		return woList;
	}
	public static List<WorkOrder> getPaymentSDWorkOrders(Set<Id> houseIdSet){
		Id sdpaymentRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();
		return [select Id, Status, House__c from WorkOrder
				where house__c =:houseIdSet and RecordTypeId =:sdpaymentRecordType ];
	}
	public static List<WorkOrder> getKeysWorkOrders(Set<Id> oppIdSet)
	{
		Id keysWrkRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_KEYS).getRecordTypeId();
		return [Select id, Opportunity__c, Status from WorkOrder where RecordTypeId=:keysWrkRecordType and Status!='Recieved'];
	}
    public static List<WorkOrder> getvenderWorkOrders(Set<Id> caseidSet)
	{
		Id wrkRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
		return [Select id,CaseId, Opportunity__c, ownerID, Status from WorkOrder where CaseId=:caseidSet and RecordTypeId=:wrkRecordType and Status=:Constants.WORKORDER_STATUS_RESOLVED];
	}
}