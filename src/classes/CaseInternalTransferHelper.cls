public class CaseInternalTransferHelper {
    public static void afterInsertCaseOfMoveInType(Map<Id,Case> newMap){
        Id moveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK).getRecordTypeId();
        Id MoveIn_CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        List<WorkOrder> woList = new List<WorkOrder>();
        for(CAse c:newMap.values()){
            if(c.recordtypeId == MoveIn_CaseRecordTypeId && c.ParentId !=null && c.SD_Status__c=='Pending' ){
                WorkOrder wo = new WorkOrder();
                wo.CaseId = c.Id;
                wo.RecordTypeId = moveInWorkOrderRTId;
                wo.caseid=c.parentid;
                wo.Subject = 'SD Recovery Task';
                wo.Type_of_WO__c =Constants.WORKORDER_TYPE_SD_RECOVERY;
                woList.add(wo);
            }
        }
        system.debug('woList'+woList);
        insert woList;
    }
     public static void afterupdateInternal(Map<Id,Case> newMap,Map<Id,Case> oldMap){
        Id moveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK).getRecordTypeId();
        Id MoveIn_CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
           Id InternalTransfer_CRTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_INTERNAL_TRANSFER).getRecordTypeId();
        List<WorkOrder> woList = new List<WorkOrder>();
         Set<id> SetofCase = new Set<Id>();
         for(case each:newMap.values()){
             if(each.recordtypeId == InternalTransfer_CRTypeId){
                 if(each.SD_Status__c != null && (each.SD_Status__c != OldMap.get(each.id).SD_Status__c) && each.SD_Status__c == Constants.CASE_STATUS_IN_PAID ){
                    SetofCase.add(each.Id); 
                 }
             }
         }
        List<Case> caseList= [Select id,(Select id,status from workorders WHERE Type_of_WO__c =:Constants.WORKORDER_TYPE_SD_RECOVERY) from case where id in:SetofCase];
         for(Case each:caseList){
             {
                 for(Workorder wo:each.workorders){
                     wo.status=Constants.WorkORDER_COMPLETE_STATUS_MOVEOUT_RECORDTYPE;
                     woList.add(wo);
                 }
             }
         }
         update woList;
    }
}