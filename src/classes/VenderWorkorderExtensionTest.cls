@isTest
public  class VenderWorkorderExtensionTest 
{
	public static TestMethod void  TestVenderWorkorderExtension()
	{
		/* PageReference pageRef = Page.success;
        Test.setCurrentPage(pageRef);*/

       // String nextPage = controller.save().getUrl();
      //  System.assertEquals('/apex/failure?error=noParam', nextPage);
        
        Problem__c pb=new Problem__c();
        pb.Name='Electrical ➢ Fan ➢ Not Provided';
        pb.Skills__c='Electrician';
        pb.Priority__c='Medium';
        insert pb;

        Account acc=new Account();
        acc.firstname='vendor1';
        acc.lastname='test';
        acc.Vendor_Type__c='Inhouse';
        insert acc;

        Contact co=new Contact();
        co.firstname='v1';
        co.lastname='test';
        co.Accountid=acc.id;
        co.Type_of_Contact__c='vendor';
        insert co;
       
        Contact co1=new Contact();
        co1.firstname='v1';
        co1.lastname='test2';
        co1.Accountid=acc.id;
        co1.Type_of_Contact__c='Technician';
        insert co1;

        Contact co2=new Contact();
        co2.firstname='v1';
        co2.lastname='test3';
        co2.Accountid=acc.id;
        co2.Type_of_Contact__c='Technician';
        insert co2;

        user us=new User();
        us.Alias='Vt1';
        us.Email='Vt.@gmail.com';
        us.Username='Vt.@gmail.com';
        us.IsActive=true;
        us.Contactid=co.id;
       // us.Accountid=acc.id;
        us.Vendor_Capability__c='Electrician';
        insert us;

        user us1=new User();
        us1.Alias='Vt2';
        us1.Email='Vt2.@gmail.com';
        us1.Username='Vt2.@gmail.com';
        us1.IsActive=true;
        us1.Contactid=co1.id;
       // us1.Accountid=acc.id;
        us1.Vendor_Capability__c='Electrician';
        insert us1;

        zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='HSR';
        insert zc;

        Zone_and_OM_Mapping__c zm=new Zone_and_OM_Mapping__c();
        zm.Zone__c=zc.id;
        zm.isActive__c=true;
        zm.User__c=us.id;
        insert zm;
         
        Zone_and_OM_Mapping__c zm1=new Zone_and_OM_Mapping__c();
        zm1.Zone__c=zc.id;
        zm1.isActive__c=true;
        zm1.User__c=us1.id;
        insert zm1;

        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.Onboarding_Zone__c=zc.id;
        insert houseObj;
         
	}
	
		
}