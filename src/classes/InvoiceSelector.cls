/*  Created By   : Mohan - WarpDrive Tech Works
	Created Date : 22/05/2017
	Purpose      : All the queries related to the Invoice Object are here */

public class InvoiceSelector {

	//Aggregate Query to get Summation of Onboarding, OPM and Offboarding costs
	public static List<AggregateResult> getSummatedAmountOfInvoices(Set<Id> houseIdSet){

		return  [select House__c, Category__c, sum(Amount__c)total from Invoice__c 
                 where House__c =: houseIdSet group by House__c, Category__c];
	}
}