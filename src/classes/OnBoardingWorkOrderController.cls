public class OnBoardingWorkOrderController {
    Public List<WorkOrder> workoderList{get;set;}
    public String selectedoption{get;set;}
    Public List<Room_Inspection__c > RoomInspectionList{get;set;}
    public String NumberOfCommonBathrooms{get; set;} 
    Public String workorderid;
    public String NumberOfAttachedBathrooms{get; set;}
    public String  StringHouseInspectionID;
    public String RoomInspectionID;  
    public boolean showPage{get;set;}
    public boolean pageReadOnly{get;set;}
    public House_Inspection_Checklist__c hic{get;set;}
    
    
    public String StringHouseID;
    public OnBoardingWorkOrderController (ApexPages.StandardController sController) {
        StringHouseID=null;
        StringHouseInspectionID=null;
        showPage=true;
        pageReadOnly=false;
        hic = new   House_Inspection_Checklist__c();    
        workoderList=[select House__c,Case.status,StatusCategory  from WorkOrder WHERE id =: ApexPages.currentPage().getParameters().get('id')];
        if(workoderList.size()>0){
            
            WorkOrder wo=workoderList.get(0);
            if(wo.Case.status!='Key Handling' || wo.StatusCategory=='Completed'){
                pageReadOnly=true;
            }
            
            workorderid=wo.id;
            if(wo.House__c!=null){
                StringHouseID=wo.House__c;
                List<House_Inspection_Checklist__c> HouseInspectionList=[Select id,Number_of_common_bathrooms__c,Number_of_attached_bathrooms__c from House_Inspection_Checklist__c where house__c=:StringHouseID and Type_Of_HIC__c =  'House Inspection Checklist' ];
                if(HouseInspectionList.size()>0){
                    hic=HouseInspectionList.get(0);
                    StringHouseInspectionID=hic.id;             
                    RoomInspectionList=[select id,Name,Has_an_attached_bathroom__c   from Room_Inspection__c where House_Inspection_Checklist__c=:StringHouseInspectionID];
                }
                else{
                   showPage=false;
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Checklist is not present in house.'));
                 }
              }
             else{
               showPage=false;
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'House detail is not present on workorder.'));
             }          
        }
        else{
            showPage=false;
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid work order id.'));
        }
        
        
       
        
    }
    
    
    
    public void save(){
       
        try{
            Integer   CountOfAttachedBAthroomInRoom=0;
            boolean hasBlankOption=false;
            for(Room_Inspection__c each:RoomInspectionList){
                if(each.Has_an_attached_bathroom__c=='Yes'){
                    CountOfAttachedBAthroomInRoom++;
                }
                if(each.Has_an_attached_bathroom__c==null || each.Has_an_attached_bathroom__c=='' || each.Has_an_attached_bathroom__c=='--None--'){
                    
                    hasBlankOption=true;
                }
            }       
            if(hic.Number_of_common_bathrooms__c==null){
                
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,LABEL.WOPAGE_COMMBATH_ERROR_MSG));
            }
            else if(hic.Number_of_attached_bathrooms__c==null){
                
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,LABEL.WOPAGE_ATTACHBATH_ERROR_MSG));
            }
            else if(hasBlankOption){
                
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,LABEL.WOPAGE_ATTACHBATH_ROOMERROR_MSG));
            }
            else if(hic.Number_of_attached_bathrooms__c!=CountOfAttachedBAthroomInRoom){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,LABEL.WOPAGE_ATTACHBATH_NOT_EQUAL_ERROR));
            }
            else{               
                    
                StopRecursion.DisabledBathRoomValidationOnWOPage=true;  
                update hic;
                
                if(RoomInspectionList.size()>0){                    
                    update RoomInspectionList;
                }
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,Label.WOPAGE_COMMBATH_SUCCESS_MSG));   
              
                    
             }    
          }        
        
        catch(Exception e){
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'An internal error has occurred. please contact your system administrator.'));
            System.debug('***Exception -> '+e+e.getLineNumber()+e.getMessage());
        }
    }
    
}