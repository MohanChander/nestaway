@IStest
public class WorkOrderTriggerHelperTest {
    Public Static TestMethod Void doTest(){
          
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK_READ_ONLY).getRecordTypeId();
        Id MoveOutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK_READ_ONLY).getRecordTypeId();
        Id mimohicTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
        Id mimoricRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
        Id mimobicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
        
        
        map<id,workOrder> newmap= new  map<id,workOrder>();
        map<id,workOrder> oldmap= new  map<id,workOrder>();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*
        Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;
        */
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        Case c=  new Case();
        c.Status=Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveIn_Slot_End_Time__c=System.today();
        c.MoveIn_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOveOUt';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        workorder wd = new workorder();
        wd.caseid=c.id;
        wd.RecordTypeId = MoveInWorkOrderRTId;
        wd.Status=Constants.CASE_STATUS_MOVED_IN_ALL_OK ;
        insert wd;
        oldmap.put(wd.id,wd);
        wd.Status=Constants.CASE_STATUS_MOVED_IN_WITH_ISSUE;
        update wd;
        newmap.put(wd.id,wd);
        House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Work_Order__c=wd.id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;
      
        Room_Inspection__c rc= new Room_Inspection__c();
        rc.House__c=hos.id;
        rc.RecordTypeId=mimoricRTId; 
        rc.Work_Order__c=wd.id;
        insert rc;
         Test.startTest();
        List<workorder> wdlist= new   List<workorder>();
        WorkOrderTriggerHelper.syncMimoChecklists(wdlist);
        WorkOrderTriggerHelper.createChecklistsForMoveInOrMoveOut(newmap);
        WorkOrderTriggerHelper.cloneRoomInspection(rc,wd,false);
        test.stopTest();
    }
       Public Static TestMethod Void doTes1t(){
        
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK_READ_ONLY).getRecordTypeId();
        Id MoveOutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK_READ_ONLY).getRecordTypeId();
        Id mimohicTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
        Id mimoricRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
        Id mimobicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
        
        
        map<id,workOrder> newmap= new  map<id,workOrder>();
        map<id,workOrder> oldmap= new  map<id,workOrder>();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*
        Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;
        */
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
             b.House__c=hos.id;
        insert b;
        Case c=  new Case();
        c.Status=Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveIn_Slot_End_Time__c=System.today();
        c.MoveIn_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOveOUt';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        workorder wd = new workorder();
        wd.caseid=c.id;
        wd.RecordTypeId = MoveInWorkOrderRTId;
        wd.Status=Constants.CASE_STATUS_MOVED_IN_ALL_OK ;
        insert wd;
        oldmap.put(wd.id,wd);
        wd.Status=Constants.CASE_STATUS_MOVED_IN_WITH_ISSUE;
        update wd;
        newmap.put(wd.id,wd);
        House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Work_Order__c=wd.id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;
      
        Room_Inspection__c rc= new Room_Inspection__c();
        rc.House__c=hos.id;
        rc.RecordTypeId=mimoricRTId; 
        rc.Work_Order__c=wd.id;
        insert rc;
              Test.startTest();
        List<workorder> wdlist= new   List<workorder>();
        WorkOrderTriggerHelper.needToCheckChecklistStatusMOveIn(newmap,oldmap);
        WorkOrderTriggerHelper.needToCheckChecklistStatusMoveOut(newmap,oldmap);
       
        test.stopTest();
    }
   
    Public Static TestMethod Void doTest3(){
         
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK_READ_ONLY).getRecordTypeId();
        Id MoveOutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK_READ_ONLY).getRecordTypeId();
        Id mimohicTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
        Id mimoricRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
        Id mimobicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
        
        
        map<id,workOrder> newmap= new  map<id,workOrder>();
        map<id,workOrder> oldmap= new  map<id,workOrder>();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
          b.House__c=hos.id;
        insert b;
        Case c=  new Case();
        c.Status=Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveIn_Slot_End_Time__c=System.today();
        c.MoveIn_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOveOUt';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        workorder wd = new workorder();
        wd.caseid=c.id;
        wd.House__c=hos.id;
        wd.RecordTypeId = MoveInWorkOrderRTId;
        wd.Status=Constants.CASE_STATUS_MOVED_IN_ALL_OK ;
        insert wd;
        oldmap.put(wd.id,wd);
        wd.Status=Constants.CASE_STATUS_MOVED_IN_WITH_ISSUE;
        update wd;
        newmap.put(wd.id,wd);
        Set<Id> wdid= new Set<id>();
        wdid.add(wd.id);
        Set<id> houseset= new Set<id>();
        houseset.add(hos.id);
        House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Work_Order__c=wd.id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;
        Room_Inspection__c rc1= new Room_Inspection__c();
        rc1.House__c=hos.id;
        rc1.RecordTypeId=mimoricRTId; 
        rc1.Work_Order__c=wd.id;
        rc1.Type_of_RIC__c= Constants.ROOM_INSPECTION_TYPE_OF_RIC_MOVE_IN_CHECK;
        insert rc1;
        Room_Inspection__c rc= new Room_Inspection__c();
        rc.House__c=hos.id;
        rc.RecordTypeId=mimoricRTId; 
        rc.Work_Order__c=wd.id;
        rc.CheckFor__c=rc1.id;
        rc.Type_of_RIC__c= Constants.ROOM_INSPECTION_TYPE_OF_RIC_MOVE_IN_CHECK;
        insert rc;
    Test.startTest();
        List<workorder> wdlist= new   List<workorder>();
        wdlist.add(wd);
        WorkOrderTriggerHelper.syncMimoChecklists(wdlist);
        test.stopTest();
    }
    
    

    
}