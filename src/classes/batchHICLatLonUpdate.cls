global class batchHICLatLonUpdate implements Database.Batchable<sObject>,  Database.AllowsCallouts {
    
    public final string query;
    
    public batchHICLatLonUpdate(string query){
        
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
       /* String query = 'Select id,Onboarding_Zone_code__c,House_Lat_Long_details__Latitude__s,'+
                        'House_Lat_Long_details__Longitude__s , Acquisition_Zone_code__c, House__c'+
                        'FROM House_Inspection_Checklist__c'+
                        'where (Acquisition_Zone_code__c = NULL OR Onboarding_Zone_code__c = NULL)'+
                    'AND (House_Lat_Long_details__Longitude__s != NULL AND House_Lat_Long_details__Latitude__s != NULL )';
        */
       // String query = 'Select id,Acquisition_Zone_Code__c,Onboarding_Zone_Code__c,House_Lattitude__c,House_Longitude__c,city__c,HRM__c,ZAM__c from house__c';
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<house__c> houseList) {
      
        StopRecursion.HouseSwitch = false;       
        list <house__c> houseListToUpdate = new list <house__c> ();
        list <Zing_API_URL__c> zingapiurl = [SELECT id, URL__c  FROM Zing_API_URL__C];
        List<WBMIMOJSONWrapperClasses.wrapperZone > wrpList = new List< WBMIMOJSONWrapperClasses.wrapperZone >(); 
        try { 
        Date dateToday = Date.today();
            
        String sMonth = String.valueof(dateToday.month());
        String sDay = String.valueof(dateToday.day());
        if(sMonth.length()==1){
            sMonth = '0' + sMonth;
        }
        if(sDay.length()==1){
            sDay = '0' + sDay;
        }
        String sToday = String.valueof(dateToday.year()) +'-'+sMonth +'-'+ sDay ;
        for(house__c hic : houseList) {
               
                String ApiEndpoint='';
                String APIEndpoint_acquisition = '';             
                string lat = String.valueOf(hic.House_Lattitude__c) ;       // '12.9201963' ;  
                string lon = String.valueOf(hic.House_Longitude__c) ;      //'77.6481260' ;
                
                String timestamp = sToday;//'2017-05-23';
                if(zingapiurl.size() > 0) {
                    ApiEndpoint = zingapiurl[0].URL__c ;
                    APIEndpoint_acquisition = zingapiurl[0].URL__c ;
                }
                if( ApiEndpoint!= null && ApiEndpoint!= '' && lat!= null) {
                    ApiEndpoint+='tag=HouseOnboarding&long='+lon+'&lat='+lat+'&timestamp='+timestamp;
                    //ApiEndpoint = 'https://staging.nestaway.xyz/admin/zinc/lat_long?tag=OwnerAcquisition&long=77.6481260&lat=12.9201963&timestamp=2017-04-21';
                    
                    System.debug('***ApiEndpoint'+ApiEndpoint);
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    req.setEndpoint(ApiEndpoint);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setCompressed(true); // otherwise we hit a limit of 32000
                    
                    try {
                        System.debug('try');
                        res = http.send(req);
                        System.debug('****'+res.getStatusCode());
                        if(res != null){
                            String str = res.getBody();
                            if(res.getStatusCode()==200){
                                System.debug('***RES '+res);
                                System.debug('***List  str'+str);
                                
                                wrpList = (List<WBMIMOJSONWrapperClasses.wrapperZone>)JSON.deserialize(str,List<WBMIMOJSONWrapperClasses.wrapperZone>.class);
                                System.debug('***wrapper list= '+wrpList);
                                if(wrpList.size() > 0 ){
                                    System.debug('***Area details '+wrpList[0].area_id+'\n'+wrpList[0].area_name);
                                    
                                    if(wrpList[0].area_id != null) {

                                        hic.Onboarding_Zone_Code__c=wrpList[0].area_id;
                                        /*  
                                            hic.Onboarding_Zone_code__c = wrpList[0].area_id ;
                                            //hic_to_Update.add(hic);
                                            if(house.id != null ) {
                                                house.Onboarding_Zone_Code__c = wrpList[0].area_id ;
                                                //houseListToUpdate.add(house);
                                                System.debug('\n***'+houseListToUpdate);
                                            }
                                            
                                            System.debug('****'+hic_to_Update);
                                            */
                                    }
                                }
                                else{
                                    System.debug('***Error' +res.toString());
                                }
                                
                            }
                            
                        }
                    } 
                    catch(System.CalloutException e) {
                        System.debug('***EXCEPTION: '+ e);
                        UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 107');
                    }
                }

                if( APIEndpoint_acquisition != null && APIEndpoint_acquisition != '' && lat!= null ) {
                    APIEndpoint_acquisition+='tag=OwnerAcquisition&long='+lon+'&lat='+lat+'&timestamp='+timestamp;
                    System.debug('***ApiEndpoint'+ApiEndpoint);
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    req.setEndpoint(APIEndpoint_acquisition);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setCompressed(true); // otherwise we hit a limit of 32000

                    try {
                        System.debug('try1');
                        res = http.send(req);
                        System.debug('****'+res.getStatusCode());
                        if(res != null){
                            String str = res.getBody();
                            if(res.getStatusCode()==200){
                                System.debug('***RES '+res);
                                System.debug('***List  str'+str);
                                
                                wrpList = (List<WBMIMOJSONWrapperClasses.wrapperZone>)JSON.deserialize(str,List<WBMIMOJSONWrapperClasses.wrapperZone>.class);
                                System.debug('***wrapper list= '+wrpList);
                                if(wrpList.size() > 0 ) {
                                    System.debug('***Area details '+wrpList[0].area_id+'\n'+wrpList[0].area_name);

                                    if(wrpList[0].area_id != null) {
                                        hic.Acquisition_Zone_code__c   = wrpList[0].area_id ;
                                   /*     //hic_to_Update.add(hic);
                                        if(house.id != null ) {
                                            house.Acquisition_Zone_Code__c = wrpList[0].area_id ;
                                            //houseListToUpdate.add(house);
                                            System.debug('\n***'+houseListToUpdate);
                                        }
                                        
                                        System.debug('****'+hic_to_Update);
                                     */ 
                                    }
                                }
                                else{
                                    System.debug('***Error' +res.toString());
                                }
                            }
                        }
                    } 
                    catch(System.CalloutException e) {
                        System.debug('***EXCEPTION: '+ e);
                        UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 151');
                    }
                }
              // hic_to_Update.add(hic);

              if(hic.Acquisition_Zone_Code__c!=null){
                                   
                        Id acquisitionRecTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RT_ACQUISITION).getRecordTypeId();
                        Map<string,Zone__c> zoneCodeToZoneMap = new Map<string,Zone__c>();
                        Map<String,City__c> cityNameToCityMap = new Map<String,City__c>();
                        Set<String> zoneCodes = new Set<String>();
                        zoneCodes.add(hic.Acquisition_Zone_Code__c);
                        List<City__c> allCityLst=[select id,Name,HRM__c,ZAM__c from City__c];

                        for(City__c city: allCityLst){      

                            cityNameToCityMap.put(city.Name.toUpperCase(),city);        
                        }

                        List<Zone__c> allZones=[select id,Zone_code__c,HRM__c,ZAM__c from Zone__c where Zone_code__c IN:zoneCodes and recordtypeId=:acquisitionRecTypeId];
                        for(Zone__c zone:allZones){

                           zoneCodeToZoneMap.put(zone.Zone_code__c,zone);
                        }


                        
                    

                           City__c city;
                           if(hic.city__c!=null && cityNameToCityMap.containsKey(hic.city__c.toUpperCase())){
                               
                                city=cityNameToCityMap.get(hic.city__c.toUpperCase());
                            } 
                          
                          
                           if(hic.Acquisition_Zone_Code__c!=null && zoneCodeToZoneMap.containsKey(hic.Acquisition_Zone_Code__c)){
                                
                                 
                                 Zone__c zone=zoneCodeToZoneMap.get(hic.Acquisition_Zone_Code__c);
                                  
                                  
                                  if(zone.HRM__c!=null){
                                       hic.HRM__c=zone.HRM__c;
                                  }
                                  else if(city!=null && city.HRM__c!=null){
                                       hic.HRM__c=city.HRM__c;
                                  }
                                  
                                  if(zone.ZAM__c!=null){
                                       hic.ZAM__c=zone.ZAM__c;
                                  }
                                  else if(city!=null && city.ZAM__c!=null){
                                       hic.ZAM__c=city.ZAM__c;
                                  }
                                  
                               
                           }
                           else if(city!=null){      
                               
                                   if(city.HRM__c!=null){
                                       
                                         hic.HRM__c=city.HRM__c;
                                   }
                                   if(city.ZAM__c!=null){
                                       
                                        hic.ZAM__c=city.ZAM__c;
                                   }
                           }          
                        
                  
              }
              
              
              
              
                houseListToUpdate.add(hic);

            } 
        
            if(houseListToUpdate.size() > 0) {
                
                 StopRecursion.HouseSwitch = false;
                     update houseListToUpdate;
                

            }
        }
        catch(exception e){
            System.debug('***exception '+e.getLineNumber()+'  '+e.getMessage()+' \n '+e);  
            UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 151'); 
        }

         
    }   
    
    global void finish(Database.BatchableContext BC) {
        System.debug('***One batch Executed -- Ameed');
    }
}