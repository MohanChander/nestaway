/*******************************************************************************************************************************
Description: It's controller class of 'PurcahseOrderPDF' page. 
This will display order and orderline items data in PDF format.
DevelopedBy: WarpDrive Tech Works
********************************************************************************************************************************/
public with sharing class OrderPurchaseOrderPDFExtension {
    public boolean BisError {get;set;}
    public String SpurchaseOrderNo {get;set;}
    public String SpoType{get;set;}
    public string sCategory{get;set;}
    public String dOrderDate {get;set;}
    public String dArrivalDate {get;set;}
    public String sSuppierName {get;set;}
    public String sSupplierBillingAdd {get;set;}
    public String SvendormobileNumber {get;set;}
    public String sVendorEmail {get;set;}
    public String sReferenceNumber {get;set;}
    public String sOwnerBillingAdd {get;set;}
    public String shouseBillingAdd {get;set;}
    public String sSupplierTin {get;set;}
    public String sNestawayTin {get;set;}
    public String sModeOfpayment {get;set;}
    public String sTermsofPayment {get;set;}
    public list<WrapperOrderItem> wrapperListOli {get;set;}
    public list<OrderItem> oi{get;set;}
    public decimal dSubtototal1 {get;set;}
    public decimal dfnaltotal {get;set;}
    
    public String BiilingAddressForPDF {get;set;}
    
    public OrderPurchaseOrderPDFExtension(ApexPages.StandardController stdController) {
        BiilingAddressForPDF = '';
        BisError = false;SpurchaseOrderNo='-';SpoType='-';sCategory='-';dOrderDate='';
        dSubtototal1 = 0;dfnaltotal = 0;
        sSuppierName ='-';
        Id orderId = ApexPages.currentPage().getParameters().get('id');
        System.debug('***id = ' + orderId );
        order o = OrderService.getOrderRecords(orderId);
        
        //System.debug('***House address ' +o.house__r.City__c+o.house__r.City__c+o.house__r.Pincode__c+o.house__r.country__c);
        setOrderMasterData(o);
        setOrderLineItemsData(o.id);
    }
    public void setOrderMasterData(order o){
        try{
            if(o.OrderNumber !=null)
                SpurchaseOrderNo = o.OrderNumber;
            if(o.Type != null){
                SpoType = o.Type; 
            }
            if(o.Category__c != null ){
                sCategory = o.Category__c;
            }
            if(o.EffectiveDate != null){
                Integer day = o.EffectiveDate.day();
                Integer month = o.EffectiveDate.month();
                Integer year = o.EffectiveDate.year();
                String dateToShow =  String.valueOf(day)+'/'+String.valueOf(month)+'/'+String.valueOf(year);
                dOrderDate = dateToShow;
                //dOrderDate = o.EffectiveDate;
            }
            dArrivalDate = '';
            if(o.Expected_Arrival_Date__c != null){
                Integer day = o.Expected_Arrival_Date__c.day();
                Integer month = o.Expected_Arrival_Date__c.month();
                Integer year = o.Expected_Arrival_Date__c.year();
                String dateToShow =  String.valueOf(day)+'/'+String.valueOf(month)+'/'+String.valueOf(year);
                dArrivalDate = dateToShow;
                //dArrivalDate = o.Expected_Arrival_Date__c;
            }
            if(o.Vendor__r.name != null ){
                sSuppierName = o.Vendor__r.name;
            }
            sSupplierBillingAdd = '';
            if(o.Vendor__r.BillingStreet != null){
                sSupplierBillingAdd = ''+o.Vendor__r.BillingStreet ;
            }
            if(o.Vendor__r.BillingCity != null ){
                sSupplierBillingAdd = sSupplierBillingAdd+' '+o.Vendor__r.BillingCity;
            }
            if(o.Vendor__r.BillingState != null){
                sSupplierBillingAdd = sSupplierBillingAdd+' '+o.Vendor__r.BillingState;
            }
            if(o.Vendor__r.BillingPostalCode != null){
                sSupplierBillingAdd = sSupplierBillingAdd+' - '+o.Vendor__r.BillingPostalCode;
            }
            if(o.Vendor__r.BillingCountry != null){
                sSupplierBillingAdd = sSupplierBillingAdd+ ' '+o.Vendor__r.BillingCountry;
            }
            SvendormobileNumber = '-';
            if(o.Vendor__r.phone != null){
                SvendormobileNumber = o.Vendor__r.Phone;
            }
            svendorEmail ='-';
            if(o.Vendor__r.Contact_Email__c != null){
                svendorEmail = o.Vendor__r.Contact_Email__c;
            }
            sReferenceNumber ='-';
            if(o.Vendor__r.Reference_Number__c != null){
                sReferenceNumber = o.Vendor__r.Reference_Number__c;
            }
            
            if(o.Show_Nestaway_Billing_Address__c != null && o.Show_Nestaway_Billing_Address__c == true){
            
                list<account> accNstaway = new list <account>();
                accNstaway = [Select id,name,BillingStreet,BillingCity,BillingPostalCode,BillingCountry from account where recordtype.name = : 'Company Account'];
                
                BiilingAddressForPDF = '';
                
                if(accNstaway.size() > 0) { 
                
                    if(accNstaway[0].BillingStreet != null){
                        BiilingAddressForPDF = ''+o.account.BillingStreet ;
                    }
                    if(accNstaway[0].BillingCity != null ){
                        BiilingAddressForPDF = BiilingAddressForPDF+' '+o.account.BillingCity;
                    }
                    if(accNstaway[0].BillingState != null){
                        BiilingAddressForPDF = BiilingAddressForPDF+' '+o.account.BillingState;
                    }
                    if(accNstaway[0].BillingPostalCode != null){
                        BiilingAddressForPDF = BiilingAddressForPDF+' - '+o.account.BillingPostalCode;
                    }
                    if(accNstaway[0].BillingCountry != null){
                        BiilingAddressForPDF = BiilingAddressForPDF+ ' '+o.account.BillingCountry;
                    }
                
                }
                
                if(BiilingAddressForPDF == '' || BiilingAddressForPDF == ' ' || BiilingAddressForPDF == null){
                    BiilingAddressForPDF = '1546 & 1547, Beside Biryani Zone, 19th Main Road, Sector 1, HSR layout, C, Karnataka 560102';
                }
                
            }
            else{
            
                if(o.account.BillingStreet != null){
                    BiilingAddressForPDF = ''+o.account.BillingStreet ;
                }
                if(o.account.BillingCity != null ){
                    BiilingAddressForPDF = BiilingAddressForPDF+' '+o.account.BillingCity;
                }
                if(o.account.BillingState != null){
                    BiilingAddressForPDF = BiilingAddressForPDF+' '+o.account.BillingState;
                }
                if(o.account.BillingPostalCode != null){
                    BiilingAddressForPDF = BiilingAddressForPDF+' - '+o.account.BillingPostalCode;
                }
                if(o.account.BillingCountry != null){
                    BiilingAddressForPDF = BiilingAddressForPDF+ ' '+o.account.BillingCountry;
                }
                
            }
            
            /*set owner address details*/
            sOwnerBillingAdd = '';
            if(o.account.BillingStreet != null){
                sOwnerBillingAdd = ''+o.account.BillingStreet ;
            }
            if(o.account.BillingCity != null ){
                sOwnerBillingAdd = sOwnerBillingAdd+' '+o.account.BillingCity;
            }
            if(o.account.BillingState != null){
                sOwnerBillingAdd = sOwnerBillingAdd+' '+o.account.BillingState;
            }
            if(o.account.BillingPostalCode != null){
                sOwnerBillingAdd = sOwnerBillingAdd+' - '+o.account.BillingPostalCode;
            }
            if(o.account.BillingCountry != null){
                sOwnerBillingAdd = sOwnerBillingAdd+ ' '+o.account.BillingCountry;
            }
            //set house address details
            shouseBillingAdd =' ';
            
            if(o.House_PO__c != null  &&  o.House_PO__r.Street__c != null ){
                System.debug('****'+o.house__r.Street__c);
                shouseBillingAdd =''+ o.House_PO__r.Street__c ;
            }
            if(o.House_PO__c != null &&  o.House_PO__r.City__c != null ){
                System.debug('***'+o.House_PO__r.City__c);
                shouseBillingAdd = shouseBillingAdd+' '+o.House_PO__r.City__c;
            }
            if(o.House_PO__c != null && o.House_PO__r.State__c != null){
                System.debug('***'+o.House_PO__r.State__c);
                shouseBillingAdd = shouseBillingAdd+' '+o.House_PO__r.State__c;
            }
            if(o.House_PO__c != null && o.House_PO__r.Pincode__c != null){
                System.debug('***'+o.House_PO__r.Pincode__c);
                shouseBillingAdd = shouseBillingAdd+' - '+o.House_PO__r.Pincode__c;
            } 
            if(o.House_PO__c != null && o.House_PO__r.country__c != null){
                System.debug('***'+o.House_PO__r.country__c);
                shouseBillingAdd = shouseBillingAdd+ ' '+o.House_PO__r.country__c;
            }
            System.debug('***shouseBillingAdd  '+shouseBillingAdd);
            
            sSupplierTin = '-';
            if(o.Vendor__r.TIN_No__c != null){
                sSupplierTin = o.Vendor__r.TIN_No__c;
            }
            sModeOfpayment = '-';
            if(o.Vendor__r.Mode_of_Payment__c != null){
                sModeOfpayment = o.Vendor__r.Mode_of_Payment__c;
            }
            sTermsofPayment = '-';
            if(o.Vendor__r.Terms_of_Payment__c != null){
                sTermsofPayment = o.Vendor__r.Terms_of_Payment__c;
            }
        }
        catch(exception e){
            System.debug('***Exception in setOrderMasterData'+e.getLineNumber()+' - '+e.getMessage());
        }
    }
    public void setOrderLineItemsData(id orderId){
        System.debug('***setOrderLineItemsData '+orderId);
        try{
            oi = new list<orderitem>();
            oi=OrderService.getOrderProducts(orderId);
            System.debug('***oi - '+oi);
            wrapperListOli = new list<WrapperOrderItem> ();
            integer sno = 0;
            for(orderitem oli : oi){
                sno+=1;
                WrapperOrderItem wrapperRecord = new WrapperOrderItem(oli,sno);
                wrapperListOli.add(wrapperRecord);
                Decimal vatPer;
                if(oli.Vat__c == null){vatPer = .15;}
                else{vatPer = oli.Vat__c;}
                dSubtototal1 = dSubtototal1 + (oli.Quantity * oli.UnitPrice).setScale(2)+((oli.Quantity * oli.UnitPrice).setScale(2) * (vatPer/100).setScale(2)).setScale(2);
            }
            System.debug('***wrapperListOli - '+wrapperListOli);
        }
        catch(exception e){
            System.debug('***Exception in setOrderLineItemsData'+e.getLineNumber()+' - '+e.getMessage());
        }
    }
    
    public class WrapperOrderItem{
        public orderItem oli_wrapper {get;set;}
        public Integer iSno {get;set;}
        public decimal dTotalPricewithoutTax {get;set;}
        public decimal dTotalPricewithTax {get;set;}
        public decimal dTaxAmount {get;set;}
        public decimal vatPer {get;set;}
        public WrapperOrderItem(orderitem oli,Integer s_no){
            System.debug('***WrapperOrderItem'+oli+'--\n ***s_no-- '+s_no);
            try{
                Decimal vat_percentage;
                if(oli.Vat__c == null){
                    vat_percentage = .15;
                    vatPer =.15;
                }
                else {
                   vat_percentage = oli.Vat__c;
                    vatPer = oli.Vat__c;
                }
                
                iSno = s_no;
                oli_wrapper = oli;
                dTotalPricewithoutTax = (oli.Quantity * oli.UnitPrice).setScale(2);
                dTaxAmount = ((oli.Quantity * oli.UnitPrice).setScale(2) * (vat_percentage/100).setScale(2)).setScale(2);
                dTotalPricewithTax = (dTotalPricewithoutTax.setScale(2)+(oli.Quantity * oli.UnitPrice * vat_percentage/100)).setScale(2);
            }
            catch(exception e){
                System.debug('***Exception in WrapperOrderItem constructor'+e.getLineNumber()+' - '+e.getMessage());
            }
        }
    }
    
 
}