public class BankDetailTriggerHandler 
{
  public static void beforeInsert(List<Bank_Detail__c> newlist)
  { 
      BankDetailTriggerHelper.bankName(newlist);     
  }
  public static void beforeUpdate(Map<id,Bank_Detail__c> newMap,Map<id,Bank_Detail__c> oldMap)
  {
    List<Bank_Detail__c> bkList = new List<Bank_Detail__c>(); 
    for(Bank_Detail__c bk:newMap.values())
    {
      if(bk.Account_Firstname__c!=oldMap.get(bk.id).Account_Firstname__c || bk.Bank_Name__c!=oldMap.get(bk.id).Bank_Name__c || bk.Account_Number__c!=oldMap.get(bk.id).Account_Number__c )
      {
              bkList.add(bk);
      }
    }
      BankDetailTriggerHelper.bankName(bkList);     
  }
  
  // added by chandu to remove the process builder and move the change here
  
  public static void updateAccountForBankDetail(List<Bank_Detail__c> allBankLst){
  
    List<account> updateAccountLst= new List<account>();
    
    for(Bank_Detail__c bk: allBankLst){
    
       if(bk.Related_Account__c!=null){
         account acc = new account();
         acc.id=bk.Related_Account__c;
         acc.isBankDetails__c=true;
         updateAccountLst.add(acc);
        } 
    }
    
    if(updateAccountLst.size()>0){
       update updateAccountLst;
    }
  
  }
  
  
  // added by chandu : to sync bank details with  webapp
    
     public static void syncBank(Map<Id,Bank_Detail__c> newMap){
         
         if(newMap.keySet().size()>0){
             
             syncBanks(newMap.keySet());
         }
     }
    
    @future(callout=true)
     public static void syncBanks(set<Id> bankIds){
         
         for(Id bankid: bankIds){
             SendAPIRequests.bankSync(bankid);
         }
          
     }
  
}