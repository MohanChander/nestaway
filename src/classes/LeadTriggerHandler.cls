/*
* Description: It's trigger handler class of Lead object's trigger, Here we wrote custom lead conversion functionality
*/

Public Class LeadTriggerHandler{
    
 Public Static void beforeInsert(List<Lead> newLst){
     
      Set<String> cityNameSet = UtilityClass.getCitySet(); 
      for(Lead eachLead : newLst){          
                   
         if(eachLead.City != null && !cityNameSet.contains(eachLead.City.toUpperCase())){
             
             eachLead.Status='Disqualified';
             eachLead.Reason_for_closing__c='Non-serviceable City';
             eachLead.NonServiceable_City__c=true;
         }
      }   
  }
  
   
  Public Static void AfterIsert(Map<Id,Lead> newMap){
      NA_TerittoryMappingSite.handleMappingSite(newMap.keyset());
      
      for(Lead eachLead : newMap.values()){
          
           //calling phone number validation from trigger         
           if(eachLead.status=='House Visit Scheduled'){
               eachLead.addError(system.label.Lead_HVS_ERROR_MSG);
           }
           // added by Chandu :- to bypass the API for bulk csv upload.
           else if(eachLead.Bulk_Lead__c){
              
           }
           else{
               
                PhoneValidationCheck.numberValidationFuture(eachLead.id,eachLead.Phone,eachLead.Email,eachLead.Country_Code_Primary__c);
           }
           
         
      }   
  }
  
  Public Static Void BeforeValidation(Map<Id,Lead> oldMap, Map<Id,Lead> newMap){
  
      System.debug('*********BeforeValidation');
    
    Id ownerLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(Constants.LEAD_RECORD_TYPE_OWNER ).getRecordTypeId();
    Id ownerLeadIdWithPhoneOption = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(Constants.LEAD_RECORD_TYPE_OWNER_LEAD_WITH_PHONE_OPTION).getRecordTypeId(); 


     for(Lead eachLead : newMap.values()){

        /*added by Mohan
       if(eachLead.RecordTypeId == ownerLeadIdWithPhoneOption && eachLead.Status == Constants.LEAD_STATUS_HOUSE_VISIT_SCHEDULED && eachLead.Status !=  oldMap.get(eachLead.Id).Status && eachLead.IsValidPrimaryContact__c && eachLead.Phone_Option__c == null)
            eachLead.addError('Please select if you would want to update the Owner Phone Number'); */

       if(eachLead.RecordTypeId == ownerLeadRecordTypeId && eachLead.Status == Constants.LEAD_STATUS_HOUSE_VISIT_SCHEDULED && eachLead.Status !=  oldMap.get(eachLead.Id).Status && !eachLead.IsValidPrimaryContact__c)
            eachLead.addError(eachLead.Phone_Validation_Message__c + ' Please save the Lead in New or Open Stage');

       if(eachLead.RecordTypeId == ownerLeadRecordTypeId && eachLead.Status == Constants.LEAD_STATUS_HOUSE_VISIT_SCHEDULED && eachLead.Status !=  oldMap.get(eachLead.Id).Status && eachLead.IsValidPrimaryContact__c){
             
             if(eachLead.Phone != oldMap.get(eachLead.Id).Phone && eachLead.Email != oldMap.get(eachLead.Id).Email)
                 eachLead.addError('You modified the validated number and Email. Please save this lead in New or Open stage and try again.');
             else if(eachLead.Phone != oldMap.get(eachLead.Id).Phone)
                 eachLead.addError('You modified the validated number. Please save this lead in New or Open stage and try again.');
             else if(eachLead.Email != oldMap.get(eachLead.Id).Email)
                 eachLead.addError('Phone number is not validated due to modification of email address, please save this lead in New or Open stage and try again');
         }         
         
         /*
         if((eachLead.Phone!=Null && eachLead.Phone!=oldMap.get(eachLead.Id).Phone) || (eachLead.Email!=Null && eachLead.Email!=oldMap.get(eachLead.Id).Email) ){
            eachLead.IsValidPrimaryContact__c=false;
            eachLead.isAPISuccess__c=false;
            eachLead.phone_validation_message__C = 'Once saved, please click on Validate Phone Number button';
         }   */
    } 
  }
  Public Static Void AfterUpdate(Map<Id,Lead> oldMap, Map<Id,Lead> newMap){
    //if(StopRecursion.isLeadAfterUpdate){
        set<Id> setLeadId = new set<Id>();
        Id leadOwnerId    = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Owner Lead').getRecordTypeId();
        Id leadUnClfdId   = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Unclassified Lead').getRecordTypeId();
        for(Lead each : newMap.values()){
                          
            if(each.RecordtypeId==leadOwnerId && oldMap.get(each.Id).RecordtypeId==leadUnClfdId){
                setLeadId.add(each.Id);
            }
            if(each.Street!= oldMap.get(each.Id).Street || each.city!= oldMap.get(each.Id).City || each.State!= oldMap.get(each.Id).State || each.PostalCode!= oldMap.get(each.Id).PostalCode){
                setLeadId.add(each.Id);
            }
            
            if(each.status=='House Visit Scheduled' && ((each.Phone!=Null && each.Phone!=oldMap.get(each.Id).Phone) || (each.Email!=Null && each.Email!=oldMap.get(each.Id).Email) ) ){
                  each.addError(system.label.LEAD_HVS_EMAIL_PHONE_CHANGE_ERROR_MSG);
             
            }
           /*  else if(each.status=='House Visit Scheduled' && !each.isAPISuccess__c){
                   each.addError(system.label.LEAD_PHONE_VALIDATION_WAIT_MSG);
            }
            else if(each.status=='House Visit Scheduled' && !each.IsValidPrimaryContact__c){
                   each.addError(system.label.LEAD_INVALID_COMBINATION);
            }
            */
            else if(!each.isAPISuccess__c || ((each.Phone!=Null && each.Phone!=oldMap.get(each.Id).Phone) || (each.Email!=Null && each.Email!=oldMap.get(each.Id).Email))){
                 PhoneValidationCheck.numberValidationFuture(each.id,each.Phone,each.Email,each.Country_Code_Primary__c);
            }
            else{
                
            }
            
            
            
            
        } 
        System.debug('==setLeadId=='+setLeadId);
       /* List<Lead>lstLeadToCheck = new List<Lead>();
        lstLeadToCheck=[SELECT id,RecordType.Name FROM Lead WHERE Id IN : setLeadId];
        System.debug('==lstLeadToCheck=='+lstLeadToCheck); */
        if(setLeadId.size()>0){
            NA_TerittoryMappingSite.handleMappingSite(setLeadId);
        }
        //StopRecursion.isLeadAfterUpdate=FALSE;
    //}
  }
  Public Static void LeadAutoConvert(Map<Id,Lead> oldMap, Map<Id,Lead> newMap){
    Map<Id,Lead> IsConvertedLeadMap = new Map<Id,Lead>(); 
    Set<String> HVSEmailSet = new Set<String>();
    set<String> HVSPhoneSet = new set<String>();

    for(Lead each : newMap.values()){
      if(each.Status!= oldMap.get(each.Id).Status && each.Status == 'House Visit Scheduled' && !each.IsConverted){
        IsConvertedLeadMap.put(each.Id, each);
        HVSEmailSet.add(each.Email);
        if(each.Phone!=null){
            HVSPhoneSet.add(each.Phone);
        }
      }
    }

    if (!IsConvertedLeadMap.isEmpty()) {
      Id OwnerPerAccId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(); 
      Id leadOwnerId   = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Owner Lead').getRecordTypeId(); 
      Id leadTenantId  = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Tenant Lead').getRecordTypeId(); 
      Id OwnerAccId    = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId(); 
       
      List<Database.LeadConvert> lstLeadToConvert = new List<Database.LeadConvert>();
      Map<Id,Opportunity> mapLead_Opp = new map<Id,Opportunity>();
      Map<Id,Account> mapLead_Acc = new map<Id,Account>();
      Map<Id,Contact> mapLead_Cont = new map<Id,Contact>();
      Map<Id,Account> mapExistingAcc = new Map<Id,Account>();
      Map<String,String> HVSLeadMap = new Map<String,String>();
      try {
          if (!HVSEmailSet.isEmpty() || !HVSPhoneSet.isEmpty()) {
              for(Account acc : [SELECT Id,Phone,BillingCity,BillingCountry,BillingGeocodeAccuracy,BillingLatitude,BillingLongitude,BillingPostalCode,BillingState,BillingStreet,Contact_Email__c, PersonEmail FROM Account WHERE (Contact_Email__c IN : HVSEmailSet OR PersonEmail IN : HVSEmailSet) OR Phone IN : HVSPhoneSet]){
                if (acc.PersonEmail != NULL && acc.PersonEmail != '') {
                  HVSLeadMap.put(acc.PersonEmail, acc.Id);
                  mapExistingAcc.put(acc.Id,acc);
                } else if(acc.Contact_Email__c != NULL && acc.Contact_Email__c != ''){
                  HVSLeadMap.put(acc.Contact_Email__c, acc.Id);
                  mapExistingAcc.put(acc.Id,acc);
                }
              }
          }

      }catch (Exception e) {
          
      }
      

      for(Lead each : IsConvertedLeadMap.values()){

        if(each.OwnerId.getSObjectType()==User.SobjectType){
          if(!((String.IsBLANK(each.Company) && each.Customer_Group__c==Null) || each.Customer_Group__c=='Owner - Individual')){
            Contact cont = new contact();
            cont.OwnerId   = each.OwnerId;
            cont.FirstName = each.FirstName;
            cont.LastName  = each.LastName;
            cont.Email     = each.Email;
            cont.Lead__c   = each.Id;
            /****Added bu Poornapriya****/
            cont.Country_Code_Primary__c = each.Country_Code_Primary__c;
            /****************************/
            mapLead_Cont.put(cont.Lead__c,cont);
          }
          
          
          Opportunity opp = new Opportunity();
          opp.OwnerId     = each.OwnerId;
          opp.AccountId   = NULL;
          opp.Name        = each.FirstName +' '+ each.LastName;
          opp.Lead__c     = each.Id;
          opp.CLoseDate   = System.Today().addDays(30);
          opp.StageName   = 'House Inspection';
          opp.City__c     = each.City;
          opp.State__c    = each.State;
          opp.Street__c   = each.Street;
          opp.Pin_Code__c = each.PostalCode;
          opp.Door_Apartment_Number__c = each.Door_Apartment_Number__c;
          opp.Country__c  = each.Country;
          opp.House_Type__c = each.House_Type__c;
          opp.House_Layout__c = each.House_Layout__c;
          opp.Primary_Contact_Number__c = each.phone;
          opp.Alternate_Contact_Number__c = each.MobilePhone;
          opp.House_visit_schedule_date__c = each.House_Visit_Scheduled_Date__c;
           /*** Added By Poornapriya ****/
          opp.Agreement_Type__c  =  each.Agreement_Type__c;        
          opp.Country_Code_Primary__c = each.Country_Code_Primary__c;
          /*****************************/
          if(each.recordtypeId==leadOwnerId){
              opp.Opportunity_Type__c='Owner';
          }else if(each.recordtypeId==leadTenantId){
              opp.Opportunity_Type__c='Tenant';
          }
          opp.Contact_Email__c  = each.email;
          mapLead_Opp.put(opp.Lead__c,opp);

          if (HVSLeadMap.get(each.Email) == NULL) {
            Account acc           = new Account();
            acc.OwnerId           = each.OwnerId;
            acc.phone             = each.phone;
            acc.Contact_Email__c  = each.Email;
            
            /****Added by Poornapriya *******/
            acc.Alternate_Contact_Number__c = each.MobilePhone;
            acc.Country_Code_Primary__c     = each.Country_Code_Primary__c;
            /***********************************/
            
            if( each.RecordtypeId ==leadOwnerId){
                 acc.Owner__c      = TRUE;  
              if((String.IsBLANK(each.Company) && each.Customer_Group__c==Null) || each.Customer_Group__c=='Owner - Individual'){
                  
                acc.RecordTypeId  = OwnerPerAccId;
                acc.FirstName     = each.FirstName;
                acc.LastName      = each.LastName;
                acc.PersonEmail   = each.Email;
              }else{
                acc.RecordTypeId  = OwnerAccId;
                acc.Name          = each.Company;
              }
            }
            acc.Lead__c       = each.Id;
            mapLead_Acc.put(acc.Lead__c,acc);
          }else{
            if (mapLead_Cont.get(each.Id) != NULL) {
                 mapLead_Cont.get(each.Id).AccountId = HVSLeadMap.get(each.Email);
            }
            opp.AccountId   = HVSLeadMap.get(each.Email);
          }
        }else{
          each.addError('You can not convert Unassigned Lead');
        }
      }
      
      if(mapLead_Acc.values().size()>0){
          try{
              system.debug('account map'+mapLead_Acc.values());
              insert mapLead_Acc.values();
              system.debug('account map after insert'+mapLead_Acc.values());
          }catch(exception ex){
              System.debug('==exception=='+ex);
          }
      }
      System.Debug('==Account=='+mapLead_Acc);
      for(Account each : mapLead_Acc.values()){
          system.debug('inside account'+each.id);
          if(each.Id!=null){
              //mapLead_AccId.put(each.Lead__c,each.Id);
              if(!mapLead_Opp.isEmpty() && mapLead_Opp.containsKey(each.Lead__c)){
                  mapLead_Opp.get(each.Lead__c).AccountId=each.Id;
              }
              if(!mapLead_Cont.isEmpty() && mapLead_Cont.containsKey(each.Lead__c)){
                  mapLead_Cont.get(each.Lead__c).AccountId = each.Id;
              }
          }
      }
      if(mapLead_Cont.values().size()>0){
          try{
              insert mapLead_Cont.values();
          }catch(exception ex){
              System.debug('==exception contact=='+ex);
          }
      }
      System.Debug('==Contact=='+mapLead_Cont);
      if(mapLead_Opp.values().size()>0){
          try{
              insert mapLead_Opp.values();
              // create tasks for all opportunities.
              List<Task> taskList = new List<Task>();
              Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Genral Task').getRecordTypeId();
              for(Opportunity o : mapLead_Opp.values()){
                if(o.Agreement_Type__c != 'LOI'){
                    Task t = new Task();
                    t.OwnerId       = o.OwnerId;
                    t.ActivityDate  = o.House_visit_schedule_date__c.date();
                    t.Subject       = 'House Visit';
                    t.Status        = 'Open';
                    t.Priority      = 'Normal';
                    t.WhatId        = o.Id;
                    t.recordTypeId  = taskRecordTypeId;
                    taskList.add(t);
                }
                
              }
              if(!taskList.isEmpty()){
                insert taskList;
              }
          }catch(exception ex){
              System.debug('==exception Opportunity=='+ex);
          }
      }
      System.Debug('==Opportunity=='+mapLead_Opp);
      set<Id> setConvertedAcc = new set<Id>();
      // adding to update phone of account.
       list<Account> lstAccUpdatePhone = new list<Account>();
       // end
      for(Lead each : IsConvertedLeadMap.values()){
        if(each.OwnerId.getSObjectType()==User.SobjectType){
          Database.LeadConvert lc = new Database.LeadConvert();
          lc.setLeadId(each.id);
          if(mapLead_Cont.containsKey(each.Id)){
              lc.setContactId(mapLead_Cont.get(each.Id).Id);
          }
          if(mapLead_Acc.containsKey(each.Id)){
              lc.setAccountId(mapLead_Acc.get(each.Id).Id);
              setConvertedAcc.add(mapLead_Acc.get(each.Id).Id);
          }else{
            lc.setAccountId(HVSLeadMap.get(each.Email));
            if(each.Phone_Option__c=='Update' && mapExistingAcc.containsKey(HVSLeadMap.get(each.Email))){
                
                account accUpdate=mapExistingAcc.get(HVSLeadMap.get(each.Email));
                accUpdate.phone=each.Phone;
                lstAccUpdatePhone.add(accUpdate);
            }
            
          }
          
          lc.setDoNotCreateOpportunity(true);
          lc.setOwnerId(each.ownerId);
          lc.setConvertedStatus('House Visit Scheduled');
          lstLeadToConvert.add(lc);
        }
      }
      if(lstLeadToConvert.size()>0){
         database.convertLead(lstLeadToConvert);
         list<Account> lstAccUpdate = new list<Account>();
          
          for(Id each: setConvertedAcc){
            Account acc = new Account();
            acc.Id                     =each;
            acc.BillingCity            ='';
            acc.BillingCountry         ='';
            acc.BillingGeocodeAccuracy ='';
            acc.BillingLatitude        =NULL;
            acc.BillingLongitude       =NULL;
            acc.BillingPostalCode      ='';
            acc.BillingState           =''; 
            acc.BillingStreet          =''; 
            lstAccUpdate.add(acc);
            
         } system.debug('==lstAccUpdate=='+lstAccUpdate);
         System.debug('==mapExistingAcc=='+mapExistingAcc);
         if(!mapExistingAcc.isEmpty() && mapExistingAcc.values().size()>0){
             try{
                 update mapExistingAcc.values();
             }catch(exception ex){
                 System.debug('==Existing Account Update exception in Lead TriggerHadler=='+ex);
             }
         }
         if(lstAccUpdate.size()>0){
             try{
                 Update lstAccUpdate;
             }catch(exception ex){
                 System.debug('== exception while Update ACcount Address in LEad Handler Class =='+ex);
             }
         }
         
         if(lstAccUpdatePhone.size()>0){
             
             try{
                 update lstAccUpdatePhone;
             }catch(exception ex){
                 System.debug('== exception while Update account phone number'+ex);
             }
            
         }
      }
    }
  }

  public static void mapDuplicateLead(List<Lead> leadList){
    List<Lead> disqualifiedNewLeadList  = new List<Lead>();
    Map<String,String> disqualifiedNewLeadMap = new Map<String,String>();
    Map<String,String> phoneValidateCheck = new Map<String,String>();
    Set<Id> leadSet = new Set<Id>();
    for(Lead lea : leadList){
        leadSet.add(lea.OwnerId);
    }
    Map<Id,User>MapOfManager = new Map<Id,User>([SELECT ManagerId,Manager.Email FROM User WHERE Id in:leadSet]);
    system.debug('MapOfManager--- '+MapOfManager);
    for(Lead ld : leadList){
      if(!MapOfManager.isEmpty() && MapOfManager.containskey(ld.OwnerId) && MapOfManager.get(ld.OwnerId).ManagerId != null){
          system.debug('inside if');
          ld.Manager_Email__c = MapOfManager.get(ld.OwnerId).Manager.Email;
           system.debug('inside if Email'+ld.Manager_Email__c );
      }
      
     
   //   if((ld.status == 'Disqualified' || ld.status == 'New') &&  ld.Related_Account__c == NULL){
       // adding email condition to only query related accounts.
      if((ld.status == 'Disqualified' || ld.status == 'New') &&  ld.Related_Account__c == NULL && ld.Email!=null){
        disqualifiedNewLeadMap.put(ld.Email, NULL);
        phoneValidateCheck.put(ld.Phone, NULL);
        disqualifiedNewLeadList.add(ld);
      }
      
    }
    try {
      if (!disqualifiedNewLeadMap.isEmpty()) {
          for(Account acc : [SELECT Id, Contact_Email__c, PersonEmail FROM Account WHERE (Contact_Email__c IN : disqualifiedNewLeadMap.keyset() OR PersonEmail IN : disqualifiedNewLeadMap.keyset()) Or phone In: phoneValidateCheck.keySet()]){
            if (acc.PersonEmail != NULL && acc.PersonEmail != '') {
              disqualifiedNewLeadMap.put(acc.PersonEmail, acc.Id);
            } else if(acc.Contact_Email__c != NULL && acc.Contact_Email__c != ''){
              disqualifiedNewLeadMap.put(acc.Contact_Email__c, acc.Id);
            }
          }
      }
      for(Lead ld : disqualifiedNewLeadList){
        if (disqualifiedNewLeadMap.get(ld.Email) != NULL) {
            ld.Related_Account__c = disqualifiedNewLeadMap.get(ld.Email);
            ld.IsValidPrimaryContact__c = TRUE;
            ld.phone_validation_message__C = 'Phone number already validated';
        }
      }

    }catch (Exception e) {}
  }

  /*  Created By : Mohan
      Purpose    : Validation for the City Name */
  public static void cityNameValidator(List<Lead> leadList){

    System.debug('***********cityNameValidator');

    Set<String> cityNameSet = UtilityClass.getCitySet();

    for(Lead l: leadList){
      if(l.status=='House Visit Scheduled' && l.City != null && !cityNameSet.contains(l.City.toUpperCase()) && l.Country == 'India'){
        l.addError(Label.City_Name_Validation_Error_Message);
      }
    }
  }  
}