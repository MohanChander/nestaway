public  class ChangeAssigneeToWorlkorderExtension {

	public id wrkid;
	public WorkOrder wrk;
	public DateTime StartTime {get;set;}
	public DateTime EndTime {get;set;}
	public List<SelectOption> userOption {get;set;}
	public id ownerId;
	public Boolean vendor {get;set;}
	public Boolean techni {get;set;}
	public List<User> vendorUsrList;
	public String techniName {get;set;}

	public ChangeAssigneeToWorlkorderExtension(ApexPages.StandardController stdController) 
	{
	     wrkid = stdController.getRecord().id;
	     wrk=[Select id,ownerId,StartDate,EndDate,Work_Start_Time__c from workorder where id=:wrkid];
	     StartTime = wrk.StartDate;
	     EndTime = wrk.EndDate;
	     ownerId=wrk.ownerId;
	     Profile admin=[Select id from profile where name='System Administrator'];
	     User currUsr=[Select id,Type_of_Contact__c from user where id=:UserInfo.getUserId()];
	     if((currUsr.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_VENDOR || UserInfo.getProfileID()==admin.id) && wrk.Work_Start_Time__c==null)
	     {
	     	vendor=true;
	     	techni=true;
	     }
	     else if(currUsr.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_TECHNICIAN && wrk.Work_Start_Time__c==null)
	     {
	     	vendor=false;
	     	techni=true;
	     }
	     else if(wrk.Work_Start_Time__c==null)
	     {  System.debug('***bony***');
	     	vendor=false;
	     	techni=false;
	     	String addErrorMessage='You are neither Vendor nor Technician';
	     	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
	     }
	     else if(wrk.Work_Start_Time__c!=null)
	     {  System.debug('***bony***');
	     	vendor=false;
	     	techni=false;
	     	String addErrorMessage='Once the Work is Started you cannot reassign or change time slot';
	     	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
	     }

	   Option();
	}
	/**************************************************************************************************************************************/        
	public void Option()
	{   
		userOption=new List<SelectOption>();
		User owner =[Select id,name,AccountID,Vendor_Type__c,contactId from User where id=:ownerId];
		System.debug('******AccountID****'+owner.AccountId);
		if(owner.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_INHOUSE && owner.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY)
		{
				if(owner.AccountId!=null)
				{
					Map<id,User> useMap = new Map<id,User>([Select id,name from user where AccountID=:owner.AccountID and Contact.Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_TECHNICIAN]);
					List<Holiday__c> holiList = [select User__c,End_Date_Time__c,Start_Date_Time__c from Holiday__c where User__c=:useMap.keySet() and ((( Start_Date_Time__c < :StartTime)  and (End_Date_Time__c > :StartTime ))or ((Start_Date_Time__c < :EndTime) and (End_Date_Time__c > :EndTime)))];
			        Map<id,List<Holiday__c>> holiMapUsrId = new Map<id,List<Holiday__c>>();
			        List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:useMap.keySet() and ((( StartDate <= :StartTime)  and (EndDate > :StartTime ))or ((StartDate < :EndTime) and (EndDate >= :EndTime)))];
			        Map<id,List<Workorder>> wrkMapUsrId = new Map<id,List<Workorder>>();
			        for(id us:useMap.keySet())
			        {
			           List<Holiday__c> holi=new List<Holiday__c>();
			           List<Workorder> wrk=new List<Workorder>();
			           if(holiList!=null)
			           {           
			               for(Holiday__c ho:holiList)
			               {
			                 if(ho.User__c==us)
			                   holi.add(ho);
			               }   
			               holiMapUsrId.put(us,holi);
			           }
			             if(wrkList!=null)
			           {           
			               for(Workorder wo:wrkList)
			               {
			                 if(wo.OwnerId==us)
			                   wrk.add(wo);
			               }   
			               wrkMapUsrId.put(us,wrk);
			           }
	                }
	                Set<id> userIdToPass = new set<Id>();
			        for(id ui:useMap.keySet())
			        {
			            if((holiMapUsrId.get(ui)==null || holiMapUsrId.get(ui).size()==0) && (wrkMapUsrId.get(ui)==null || wrkMapUsrId.get(ui).size()==0))
			            {
			               userIdToPass.add(ui); 
			            }
			        }
			        vendorUsrList=[Select id,name from user where id=:userIdToPass];
			}
		}
		else
		{
			if(owner.AccountId!=null)
			{
				vendorUsrList=[Select id,name from user where AccountID=:owner.AccountID and Contact.Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_TECHNICIAN];
			}

		}

		userOption.add(new SelectOption('-None-','-None-'));
		if(!vendorUsrList.isEmpty())
		{
			for(User us:vendorUsrList)
			{
				userOption.add(new SelectOption(us.name,us.name));
			}
		}
	}
	/******************************************************************************************************************************************/        
	public pagereference UpdateWrk()
	{    
	     if(vendor==true && techni==true && techniName!='-None-')
	     {
	     	User ur=[Select id from user where name=:techniName];
	     	wrk.ownerid=ur.id;
	     }

	     wrk.StartDate=StartTime;
	     wrk.EndDate=EndTime;
	     Update wrk;

	     PageReference parentPage;
	     if(wrk!=null){
	        parentPage = new PageReference('/' + wrk.Id);
	        parentPage.setRedirect(true);
	        return parentPage;
	    }
	    return null;
	}
	/**************************************************************************************************************************************************************/
	public pagereference Cancel()
	{

	     PageReference parentPage;
	     if(wrk!=null){
	        parentPage = new PageReference('/' + wrk.Id);
	        parentPage.setRedirect(true);
	        return parentPage;
	    }
	    return null;

	}

}