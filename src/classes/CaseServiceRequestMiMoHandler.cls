/*  Created By   : Deepak - WarpDrive Tech Works
Created Date : 18/05/2017
*/

public class CaseServiceRequestMiMoHandler {
    
    /*  Created By : Deepak
Purpose    : When a Service Request case is Resolved update mimo hic bic ric
*/
    
    public static void UpdateMiMoRicBicHic(List<Case> caseList){
        try{
            Set<Id> SetOfAgreement = new Set<Id>();
            Set<Id> setOfLivingRoom = new Set<Id>(); 
            Set<Id> setOfdDiningRoom = new Set<Id>();
            Set<Id> setOfTenantRoomcleaned = new Set<Id>();
            Set<Id> setOfTenantBathroomCleaned = new Set<Id>();
            Set<Id> setOfDth = new Set<Id>();
            Set<Id> setOfInternet = new Set<Id>(); 
            Set<Id> setOfTubelight = new Set<Id>(); 
            Set<Id> setOfCeilingFan  = new Set<Id>();
            Set<Id> setOfWaterHeater  = new Set<Id>();
            Set<Id> setOftv  = new Set<Id>(); 
            Set<Id> setOfFridge  = new Set<Id>(); 
            Set<Id> setOfWashingMachine = new Set<Id>();
            Set<Id> setOfSofa = new Set<Id>();
            Set<Id> setOfCenterTable = new Set<Id>();
            Set<Id> setOfWindowCurtainRoom = new Set<Id>();
            Set<Id> setOfDiningTable = new Set<Id>();
            Set<Id> setOfDiningChairs = new Set<Id>();
            Set<Id> setOfGasPipes = new Set<Id>();
            Set<Id> setOfStove = new Set<Id>();
            Set<Id> setOfCrockerySet = new Set<Id>();
            Set<Id> setOfPillow = new Set<Id>();
            Set<Id> setOfBedSheet = new Set<Id>();
            Set<Id> setOfWindowCurtainHouse = new Set<Id>();
            Set<Id> setOfCot = new Set<Id>();
            Set<Id> setOfmattress = new Set<Id>();
            Set<Id> setOfac = new Set<Id>();
            Set<Id> setOfchair = new Set<Id>();
            Set<Id> setOfSideStable = new Set<Id>();
            Set<Id> setOfMirror = new Set<Id>();
            Set<Id> setOfWardrobeKeys = new Set<Id>();
            Set<Id> setOfRoomKeys = new Set<Id>();
            Set<Id> setOfMainDoorkeys = new Set<Id>();
           	Set<Id> setNoofNonFunctioningRoomKeys = new Set<Id>();
            Set<Id> SetOfNumberofNonFunctioningWardrobeKeys= new Set<Id>();
             Set<Id> SetOfNumberofFans= new Set<Id>();
            
            for(Case c: caseList){
              
                    if(c.Category__c==MissingItemsConstants.CASE_CATEGORY_AGREEMENT){
                        if(c.Sub_Category__c == Constants.CASE_SUB_CATEGORY_AGREEMENT ){
                            SetOfAgreement.add(c.MimoHic__c);
                        }
                    }
                    if(c.Category__c==MissingItemsConstants.CASE_CATEGORY_CLEANING ){
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_LIVING_ROOM ){
                            
                            setOfLivingRoom.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_DINING_ROOM){
                            setOfdDiningRoom.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_TENANT_ROOM_CLEANED){
                            setOfTenantRoomcleaned.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_TENANT_BATHROOM_CLEANED){
                            setOfTenantBathroomCleaned.add(c.MimoBic__c);
                        }
                    }
                    
                    if(c.Category__c==MissingItemsConstants.CASE_CATEGORY_CONNECTIVITY ){
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_DTH  ){
                            setOfDth.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_INTERNET ){
                            setOfInternet.add(c.MimoHic__c);
                        }
                    }
                    if(c.Category__c==MissingItemsConstants.CASE_CATEGORY_ELECTRICAL   ){
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_TUBELIGHT ){
                            setOfTubelight.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_CEILING_FAN){
                            setOfCeilingFan.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_WATER_HEATER){
                            setOfWaterHeater.add(c.MimoBic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_Fans ){
                            SetOfNumberofFans.add(c.MimoRic__c);
                        }
                    }
                    if(c.Category__c==MissingItemsConstants.CASE_CATEGORY_ELECTRONICS ){
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_TV  ){
                            setOftv.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_FRIDGE ){
                            setOfFridge.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_WASHING_MACHINE ){
                            setOfWashingMachine.add(c.MimoHic__c);
                        }
                    }
                    if(c.Category__c==MissingItemsConstants.CASE_CATEGORY_FURNISHING ){
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_SOFA){
                            setOfSofa.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_CENTER_TABLE  ){
                            setOfCenterTable.add(c.MimoHic__c);
                        }
                       
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_DINING_TABLE ){
                            setOfDiningTable.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_DINING_CHAIRS   ){
                            setOfDiningChairs.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_GAS_PIPES   ){
                            setOfGasPipes.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_STOVE ){
                            setOfStove.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_CROCKERY_SET   ){
                            setOfCrockerySet.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_PILLOW){
                            setOfPillow.add(c.MimoRic__c);
                        }
                         if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_BED_SHEET){
                            setOfBedSheet.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_WINDOW_CURTAINS && c.TypeOfInspection__c =='hic'){
                            setOfWindowCurtainHouse.add(c.MimoHic__c);
                        }
						if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_WINDOW_CURTAINS  &&  c.TypeOfInspection__c =='Ric'  ){
                            setOfWindowCurtainRoom.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_COT    ){
                            setOfCot.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_MATTRESS    ){
                            setOfmattress.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_AC    ){
                            setOfac.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_CHAIR     ){
                            setOfchair.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_SIDE_TABLE    ){
                            setOfSideStable.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_MIRROR      ){
                            setOfMirror.add(c.MimoBic__c);
                        }
                    }
                    if(c.Category__c==MissingItemsConstants.CASE_CATEGORY_KEYS){
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_WARDROBE_KEYS ){
                            setOfWardrobeKeys .add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_ROOM_KEYS   ){
                            setOfRoomKeys.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_MAIN_DOOR_KEYS ){
                            SetOfMainDoorkeys.add(c.MimoHic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_ROOM_KEYS){
                            setNoofNonFunctioningRoomKeys.add(c.MimoRic__c);
                        }
                        if(c.Sub_Category__c == MissingItemsConstants.CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_WARDROBE_KEYS ){
                            SetOfNumberofNonFunctioningWardrobeKeys.add(c.MimoRic__c);
                        }
                    }
            }
            List<House_Inspection_Checklist__c> updateHicList = new List<House_Inspection_Checklist__c>();
            List<Room_Inspection__c > updateRicList = new List<Room_Inspection__c >();
            List<Bathroom__c> updateBicList = new List<Bathroom__c>();
            if(!setOfTenantBathroomCleaned.isEmpty()){
                List<Bathroom__c> listTenantBathroomCleaned=[Select Tenant_Bathroom_Cleaned__c from Bathroom__c where id in:setOfTenantBathroomCleaned];
                if(!listTenantBathroomCleaned.isEmpty()){
                    for(Bathroom__c each:listTenantBathroomCleaned){
                        each.Tenant_Bathroom_Cleaned__c='Yes';
                        updateBicList.add(each);
                    }
                }   
            }
            if(!setOfWaterHeater.isEmpty()){
                List<Bathroom__c> listWaterHeater=[Select Water_Heater__c from Bathroom__c where id in:setOfWaterHeater];
                if(!listWaterHeater.isEmpty()){
                    for(Bathroom__c each:listWaterHeater){
                        each.Water_Heater__c='Present & Working';
                        updateBicList.add(each);
                    }
                }
            }
            if(!setOfMirror.isEmpty()){
                List<Bathroom__c> listMirror=[Select Mirror__c from Bathroom__c where id in:setOfMirror];
                if(!listMirror.isEmpty()){
                    for(Bathroom__c each:listMirror){
                        each.Mirror__c='Present & In Good Condition';
                        updateBicList.add(each);
                    }
                }
            }
            if(!setOfTenantRoomcleaned.isEmpty()){
                List<Room_Inspection__c> listTenantRoomCleaned =[Select Tenant_Room_Cleaned__c from Room_Inspection__c where id in:setOfTenantRoomcleaned ];
                if(!listTenantRoomCleaned.isEmpty()){
                    for(Room_Inspection__c each:listTenantRoomCleaned){
                        each.Tenant_Room_Cleaned__c='Yes';
                        updateRicList.add(each);
                    }
                }
            }
            if(!setOfTubelight.isEmpty()){
                List<Room_Inspection__c> listTubeLight =[Select Tube_Light__c from Room_Inspection__c where id in:setOfTubelight  ];
                if(!listTubeLight.isEmpty()){
                    for(Room_Inspection__c each:listTubeLight){
                        each.Tube_Light__c='Provided ';
                        updateRicList.add(each);
                    }
                }
            }
            if(!setOfCeilingFan.isEmpty()){
                List<Room_Inspection__c> listCeilingFan =[Select Ceiling_Fan__c from Room_Inspection__c where id in:setOfCeilingFan];
                if(!listCeilingFan.isEmpty()){
                    for(Room_Inspection__c each:listCeilingFan){
                        each.Ceiling_Fan__c='Provided ';
                        updateRicList.add(each);
                    }
                }
            }
            
            if(!setOfWindowCurtainRoom.isEmpty()){
                List<Room_Inspection__c> listWindowCurtain =[Select Window_Curtain__c from Room_Inspection__c where id in:setOfWindowCurtainRoom];
                if(!listWindowCurtain.isEmpty()){
                    for(Room_Inspection__c each:listWindowCurtain){
                        each.Window_Curtain__c='Provided ';
                        updateRicList.add(each);
                    }
                }
            }
            if(!setOfPillow.isEmpty()){
                List<Room_Inspection__c> listPillow =[Select Pillow__c from Room_Inspection__c where id in:setOfPillow];
                if(!listPillow.isEmpty()){
                    for(Room_Inspection__c each:listPillow){
                        each.Pillow__c='Provided';
                        updateRicList.add(each);
                    }
                }
            }
             if(!setOfBedSheet.isEmpty()){
                List<Room_Inspection__c> listBedSheet =[Select Bed_Sheet__c from Room_Inspection__c where id in:setOfBedSheet];
                if(!listBedSheet.isEmpty()){
                    for(Room_Inspection__c each:listBedSheet){
                        each.Bed_Sheet__c='Provided';
                        updateRicList.add(each);
                    }
                }
             }
                if(!setOfCot.isEmpty()){
                    List<Room_Inspection__c> listCot =[Select Cot__c from Room_Inspection__c where id in:setOfCot];
                    if(!listCot.isEmpty()){
                        for(Room_Inspection__c each:listCot){
                            each.Cot__c='Yes';
                            updateRicList.add(each);
                        }
                    }
                }
              if(!setOfmattress.isEmpty()){
                    List<Room_Inspection__c> listMattress =[Select Mattress__c from Room_Inspection__c where id in:setOfmattress];
                    if(!listMattress.isEmpty()){
                        for(Room_Inspection__c each:listMattress){
                            each.Mattress__c='Yes';
                            updateRicList.add(each);
                        }
                    }
                }
                if(!setOfac.isEmpty()){
                    List<Room_Inspection__c> listAc =[Select AC__c from Room_Inspection__c where id in:setOfac];
                    if(!listAc.isEmpty()){
                        for(Room_Inspection__c each:listAc){
                            each.AC__c='Yes and Working';
                            updateRicList.add(each);
                        }
                    }
                }
                if(!setOfchair.isEmpty()){
                    List<Room_Inspection__c> listChair =[Select Chair__c from Room_Inspection__c where id in:setOfchair ];
                    if(!listChair.isEmpty()){
                        for(Room_Inspection__c each:listChair){
                            each.Chair__c='Yes';
                            updateRicList.add(each);
                        }
                    }
                }
                if(!setOfSideStable.isEmpty()){
                    List<Room_Inspection__c> listSideTable =[Select Side_Table__c from Room_Inspection__c where id in:setOfSideStable];
                    if(!listSideTable.isEmpty()){
                        for(Room_Inspection__c each:listSideTable){
                            each.Side_Table__c='Present & In Good Condition';
                            updateRicList.add(each);
                        }
                    }
                }
             if(!SetOfNumberofFans.isEmpty()){
                    List<Room_Inspection__c> listNoofFans =[Select No_of_Fans__c from Room_Inspection__c where id in:SetOfNumberofFans];
                    if(!listNoofFans.isEmpty()){
                        for(Room_Inspection__c each:listNoofFans){
                            each.No_of_Fans__c=0;
                            updateRicList.add(each);
                        }
                    }
                }
             if(!setNoofNonFunctioningRoomKeys.isEmpty()){
                    List<Room_Inspection__c> listNoofNonFunctioningRoomKeys =[Select No_of_Non_Functioning_Room_Keys__c from Room_Inspection__c where id in:setNoofNonFunctioningRoomKeys];
                    if(!listNoofNonFunctioningRoomKeys.isEmpty()){
                        for(Room_Inspection__c each:listNoofNonFunctioningRoomKeys){
                            each.No_of_Non_Functioning_Room_Keys__c=0;
                            updateRicList.add(each);
                        }
                    }
                }
             if(!SetOfNumberofNonFunctioningWardrobeKeys.isEmpty()){
                    List<Room_Inspection__c> listNumberofNonFunctioningWardrobeKeys =[Select Number_of_Non_Functioning_Wardrobe_Keys__c from Room_Inspection__c where id in:SetOfNumberofNonFunctioningWardrobeKeys];
                    if(!listNumberofNonFunctioningWardrobeKeys.isEmpty()){
                        for(Room_Inspection__c each:listNumberofNonFunctioningWardrobeKeys){
                            each.Number_of_Non_Functioning_Wardrobe_Keys__c=0;
                            updateRicList.add(each);
                        }
                    }
                }
                if(!SetOfAgreement.isEmpty()){
                    List<House_Inspection_Checklist__c> hicListAgreement=[Select Agreement__c from House_Inspection_Checklist__c where id in:SetOfAgreement];
                    if(!hicListAgreement.isEmpty()){
                        for(House_Inspection_Checklist__c hic:hicListAgreement){
                            hic.Agreement__c='Provided';
                            updateHicList.add(hic);
                        }
                    }
                }
                if(!setOfLivingRoom.isEmpty()){
                    List<House_Inspection_Checklist__c> listLivingRoom=[Select Living_Room__c from House_Inspection_Checklist__c where id in:setOfLivingRoom ];
                    if(!listLivingRoom.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listLivingRoom){
                            hic.Living_Room__c='Provided';
                            updateHicList.add(hic);
                        }
                    } 
                }
                 if(!setOfdDiningRoom.isEmpty()){
                    List<House_Inspection_Checklist__c> listDiningRoom=[Select Dining_Room__c from House_Inspection_Checklist__c where id in:setOfdDiningRoom];
                    if(!listDiningRoom.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listDiningRoom){
                            hic.Dining_Room__c='Provided';
                            updateHicList.add(hic);
                        }
                    } 
                }
                 if(!setOfDth.isEmpty()){
                    List<House_Inspection_Checklist__c> listDth=[Select DTH__c from House_Inspection_Checklist__c where id in:setOfDth];
                    if(!listDth.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listDth){
                            hic.DTH__c='Yes';
                            updateHicList.add(hic);
                        }
                    } 
                }
                if(!setOfInternet.isEmpty()){
                    List<House_Inspection_Checklist__c> listInternet=[Select Internet__c from House_Inspection_Checklist__c where id in:setOfInternet];
                    if(!listInternet.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listInternet){
                            hic.Internet__c='Yes';
                            updateHicList.add(hic);
                        }
                    } 
                }
                   if(!setOftv.isEmpty()){
                    List<House_Inspection_Checklist__c> listTV=[Select TV__c from House_Inspection_Checklist__c where id in:setOftv];
                    if(!listTV.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listTV){
                            hic.TV__c='Yes and Working';
                            updateHicList.add(hic);
                        }
                    } 
                }
                    if(!setOfFridge.isEmpty()){
                    List<House_Inspection_Checklist__c> listFridge=[Select Fridge__c from House_Inspection_Checklist__c where id in:setOfFridge];
                    if(!listFridge.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listFridge){
                            hic.Fridge__c='Yes and Working';
                            updateHicList.add(hic);
                        }
                    } 
                }
                 if(!setOfWashingMachine.isEmpty()){
                    List<House_Inspection_Checklist__c> listWashingMachine=[Select Washing_Machine__c from House_Inspection_Checklist__c where id in:setOfWashingMachine];
                    if(!listWashingMachine.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listWashingMachine){
                            hic.Washing_Machine__c='Yes and Working';
                            updateHicList.add(hic);
                        }
                    } 
                }
                 if(!setOfSofa.isEmpty()){
                    List<House_Inspection_Checklist__c> listSofa=[Select Sofa__c from House_Inspection_Checklist__c where id in:setOfSofa];
                    if(!listSofa.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listSofa){
                            hic.Sofa__c='Yes';
                            updateHicList.add(hic);
                        }
                    } 
                }
                 if(!setOfCenterTable .isEmpty()){
                    List<House_Inspection_Checklist__c> listCenterTable=[Select Center_Table__c from House_Inspection_Checklist__c where id in:setOfCenterTable ];
                    if(!listCenterTable.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listCenterTable){
                            hic.Center_Table__c='Present & in Good Condition';
                            updateHicList.add(hic);
                        }
                    } 
                }

                if(!setOfWindowCurtainHouse.isEmpty()){
                    List<House_Inspection_Checklist__c> listWindowCurtain=[Select Window_Curtain__c from House_Inspection_Checklist__c where id in:setOfWindowCurtainHouse];
                    if(!listWindowCurtain.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listWindowCurtain){
                            hic.Window_Curtain__c='Provided';
                            updateHicList.add(hic);
                        }
                    } 
                }
                if(!setOfDiningTable.isEmpty()){
                    List<House_Inspection_Checklist__c> listDiningTable=[Select Dining_Table__c from House_Inspection_Checklist__c where id in:setOfDiningTable];
                    if(!listDiningTable.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listDiningTable){
                            hic.Dining_Table__c='Yes';
                            updateHicList.add(hic);
                        }
                    } 
                }
                 if(!setOfGasPipes.isEmpty()){
                    List<House_Inspection_Checklist__c> listGasRegulator=[Select Gas_Regulator__c from House_Inspection_Checklist__c where id in:setOfGasPipes];
                    if(!listGasRegulator.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listGasRegulator){
                            hic.Gas_Regulator__c='Yes and Working';
                            updateHicList.add(hic);
                        }
                    } 
                }
                if(!setOfDiningChairs .isEmpty()){
                    List<House_Inspection_Checklist__c> listDiningChairs=[Select Dining_Chairs__c from House_Inspection_Checklist__c where id in:setOfDiningChairs];
                    if(!listDiningChairs.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listDiningChairs){
                            hic.Dining_Chairs__c='All Present & In Good Condition';
                            updateHicList.add(hic);
                        }
                    } 
                }
                 if(!setOfStove.isEmpty()){
                    List<House_Inspection_Checklist__c> listStove=[Select Stove__c from House_Inspection_Checklist__c where id in:setOfStove];
                    if(!listStove.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listStove){
                            hic.Stove__c='Yes and Working';
                            updateHicList.add(hic);
                        }
                    } 
                }
                if(!setOfCrockerySet.isEmpty()){
                    List<House_Inspection_Checklist__c> listCrockerySet=[Select Crockery_Set__c from House_Inspection_Checklist__c where id in:setOfCrockerySet ];
                    if(!listCrockerySet.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listCrockerySet){
                            hic.Crockery_Set__c='Yes and Complete';
                            updateHicList.add(hic);
                        }
                    } 
                }
                  if(!setOfWardrobeKeys.isEmpty()){
                    List<House_Inspection_Checklist__c> listWardrobeKeys=[Select Wardrobe_Keys__c from House_Inspection_Checklist__c where id in:setOfWardrobeKeys];
                    if(!listWardrobeKeys.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listWardrobeKeys){
                            hic.Wardrobe_Keys__c='Provided';
                            updateHicList.add(hic);
                        }
                    } 
                }
                
                if(!setOfRoomKeys.isEmpty()){
                    List<House_Inspection_Checklist__c> listRoomKeys=[Select Room_Keys__c from House_Inspection_Checklist__c where id in:setOfRoomKeys];
                    if(!listRoomKeys.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listRoomKeys){
                            hic.Room_Keys__c='Provided';
                            updateHicList.add(hic);
                        }
                    } 
                }
                     if(!SetOfMainDoorkeys.isEmpty()){
                    List<House_Inspection_Checklist__c> listMainDoorKeys=[Select Main_Door_Keys__c from House_Inspection_Checklist__c where id in:SetOfMainDoorkeys];
                    if(!listMainDoorKeys.isEmpty()){
                        for(House_Inspection_Checklist__c hic:listMainDoorKeys){
                            hic.Main_Door_Keys__c='Provided';
                            updateHicList.add(hic);
                        }
                    } 
                }
            	system.debug('updateHicList'+updateHicList);
            system.debug('updateRicList'+updateRicList);
            system.debug('updateBicList'+updateBicList);
                if(!updateHicList.isEmpty()){
                    update updateHicList;
                }
                if(!updateRicList.isEmpty()){
                    update updateRicList;
                }
                if(!updateBicList.isEmpty()){
                    update updateBicList;
                }
            } 
            catch (Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
                UtilityClass.insertGenericErrorLog(e);      
            }
        }
    }