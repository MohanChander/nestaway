@isTest
public Class  House2JsonTest {
  
      public Static TestMethod void testMethod1(){
         Set<id> setOfHouse = new Set<Id>();
         Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
        House__c hos= new House__c();
       // hos.Opportunity__c=opp.Id;
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        setOfHouse.add(hos.Id);
        House2Json.createJson(hos.Id);
    }
      public Static TestMethod void testMethod2(){
         Set<id> setOfHouse = new Set<Id>();
           Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567891';
            insert accObj;
            
         Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
            opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
                    Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            cont.status = 'Draft';
            
            insert cont;
        House__c hos= new House__c();
        hos.Opportunity__c=opp.Id;
        hos.name='house1';
        hos.Assets_Created__c=false;
          hos.Contract__c=cont.id;
        insert hos;
        setOfHouse.add(hos.Id);
           
     NestAway_End_Point__c cs = new NestAway_End_Point__c();
        cs.Name='test';
       	cs.Webentity_House_Url__c='www.google.com';
     insert cs;
          List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();
        House2Json.createJson(hos.Id);
    }
}