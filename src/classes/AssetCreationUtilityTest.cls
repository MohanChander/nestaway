@isTest
public Class  AssetCreationUtilityTest {
    public Static TestMethod void testMethod1(){
        Set<id> setOfHouse = new Set<Id>();
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        setOfHouse.add(hos.Id);
        House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;
        List<House_Inspection_Checklist__c> listOfHic= new List<House_Inspection_Checklist__c>();
        listOfHic.add(hic);
        AssetCreationUtility.assetCreationAfterOnboardingVerification(setOfHouse) ;        
    }
}