@isTest
public Class RoomInspectionHandlerTest{

    public Static TestMethod void roomInspectionTest(){
        Test.StartTest();
            Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.CloseDate = System.Today();
            opp.StageName = 'HouseInsection';
            
            insert opp;
            
            House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
            houseIC.RecordTypeId = recordTypeId;
            houseIC.Opportunity__c = opp.Id;
            houseIC.Status__c = 'House Inspection Completed';
            houseIC.House_layout__c = '1 BHK';
            
            insert houseIC;
            
            Room_Inspection__c rmIn = new Room_Inspection__c();
            rmIn.Name = 'Room Inspection 1';
            rmIn.House_Inspection_Checklist__c = houseIC.Id;
            insert rmIn;
            
            Room_Terms__c roomTerm = new Room_Terms__c();
            roomTerm.Name = 'Room Term 1';
            roomTerm.Room_Inspection__c = rmIn.Id;
            insert roomTerm;
            
            rmIn.Name = 'Room Inspection2';
            update rmIn;
            system.debug('----rmIn---'+rmIn);
            system.debug('----roomTerm---'+roomTerm);
            delete rmIn;
            
        Test.StopTest();
    }
}