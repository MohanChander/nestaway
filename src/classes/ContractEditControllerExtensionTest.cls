@isTest
public Class ContractEditControllerExtensionTest{
    public Static TestMethod void ContractEditTest(){
        Test.StartTest();
        
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567890';
            insert accObj;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            cont.status = 'Draft';
            
            insert cont;
            cont.status = 'Sample Contract';
            cont.Booking_Type__c ='Full House';
            cont.MG_Valid_Until__c = 'Date';
            update cont;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(cont );
            ContractEditControllerExtension testcont = new ContractEditControllerExtension (sc);
              
            PageReference pageRef = Page.ContractEditPage;
            Test.setCurrentPage(pageRef);
            testcont.save();
            
            cont.Furnishing_Type__c='Fully-furnished';
            cont.Furnishing_Plan__c = 'NestAway Rental Package';
            cont.MG_Start_Date__c =  system.today();
            cont.MG_End_Date__c = system.today();
            update cont;
            
             ApexPages.StandardController sc1 = new ApexPages.StandardController(cont );
            ContractEditControllerExtension testcont1 = new ContractEditControllerExtension (sc1);
              
            PageReference pageRef1 = Page.ContractEditPage;
            Test.setCurrentPage(pageRef1);
            testcont1.save();
        Test.StopTest();
        
    }
    
    public Static TestMethod void ContractEditTest1(){
        Test.StartTest();
        
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567891';
            insert accObj;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            cont.status = 'Draft';
            
            insert cont;
            
            cont.status = 'Sample Contract';
            cont.Booking_Type__c ='Full House';
            cont.MG_Valid_Until__c = 'Date';
            cont.Tenancy_type__C = 'Family';
            cont.Number_of_Bedrooms__c = '5';
            cont.Number_of_Bathrooms__c ='2';
            cont.Base_House_Rent__c = 50;
            cont.Maximum_number_of_tenants_allowed__c = '6';
            cont.Tenancy_Approval_time_period__c = '0 hrs';
            cont.Is_MG_Applicable__c = 'No';
            cont.MG_Start_Date__c =  system.today()-1;
            cont.MG_End_Date__c = system.today()-1;
            cont.Who_pays_Move_in_Charges__c = 'NestAway';
            cont.Who_pays_DTH__c = 'NestAway';
            cont.Who_pays_Society_Maintenance__c = 'Owner';
            cont.Furnishing_Type__c='Fully-furnished';
            cont.Who_pays_Electricity_Bill__c = 'Owner';
            cont.SD_Upfront_Amount__c = 50;
            cont.Restrict_SD_to_maximum_SD__c  = True;
            cont.Furnishing_Plan__c = 'NestAway furnished'; 
            cont.Rental_Plan__c = 'Fixed Rent';
            cont.Furnishing_Plan__c = 'NestAway furnished';
            cont.Who_pays_Deposit__c = 'NestAway';
            cont.Deposit_Payment_Date__c= System.Today().addDays(5);
            cont.Deposit_Amount_Equivalent_to__c ='0.5 Month';
            cont.Security_Deposit_Payment_Mode__c = 'Cheque';
            cont.Date_Of_Birth__c= System.today();
            cont.Maximum_SD_Amount__c = 100;
            cont.Booking_type__C = 'Full House';
            //cont.Furniture_Buyback_Option_Available__c = 'Yes';
            update cont;
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(cont );
            ContractEditControllerExtension testcont = new ContractEditControllerExtension (sc);
              
            PageReference pageRef = Page.ContractEditPage;
            Test.setCurrentPage(pageRef);
            testcont.save();
            testcont.validateNAContracts();
            testcont.calculateAge(cont.Deposit_Payment_Date__c);
            
        Test.StopTest();
        
    } public Static TestMethod void ContractEditTest2(){
        Test.StartTest();
        
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567893';
            insert accObj;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            //cont.status = 'Draft';
            
            insert cont;
            
            //cont.status = 'Sample Contract';
            cont.Booking_Type__c ='Full House';
            cont.MG_Valid_Until__c = 'Date';
            cont.Tenancy_type__C = 'Family';
            cont.Number_of_Bedrooms__c = '5';
            cont.Number_of_Bathrooms__c ='2';
            cont.Base_House_Rent__c = 50;
            cont.Maximum_number_of_tenants_allowed__c = '6';
            cont.Tenancy_Approval_time_period__c = '0 hrs';
            cont.Is_MG_Applicable__c = 'No';
            cont.MG_Start_Date__c =  system.today()+1;
            cont.MG_End_Date__c = system.today()+1;
            cont.Who_pays_Move_in_Charges__c = 'NestAway';
            cont.Who_pays_DTH__c = 'NestAway';
            cont.Who_pays_Society_Maintenance__c = 'Owner';
            cont.Furnishing_Type__c='Fully-furnished';
            cont.Who_pays_Electricity_Bill__c = 'Owner';
            cont.SD_Upfront_Amount__c = 50;
            cont.Restrict_SD_to_maximum_SD__c  = True;
            cont.Furnishing_Plan__c = 'NestAway furnished'; 
            cont.Rental_Plan__c = 'Fixed Rent';
            cont.Furnishing_Plan__c = 'NestAway furnished';
            cont.Who_pays_Deposit__c = 'NestAway';
            cont.Deposit_Payment_Date__c= System.Today().addDays(5);
            cont.Deposit_Amount_Equivalent_to__c ='0.5 Month';
           // cont.Security_Deposit_Payment_Mode__c = 'Cheque';
            cont.Date_Of_Birth__c= System.today();
            cont.Maximum_SD_Amount__c = 100;
            //cont.Booking_type__C = 'Full House';
            //cont.Furniture_Buyback_Option_Available__c = 'Yes';
            update cont;
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(cont );
            ContractEditControllerExtension testcont = new ContractEditControllerExtension (sc);
              
            PageReference pageRef = Page.ContractEditPage;
            Test.setCurrentPage(pageRef);
            testcont.save();
            testcont.validateNAContracts();
            testcont.calculateAge(cont.Deposit_Payment_Date__c);
            
        Test.StopTest();
        
    }
    
    
    public Static TestMethod void ContractEditTest3(){
        Test.StartTest();
        
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567894';
            insert accObj;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            //cont.status = 'Draft';
            
            insert cont;
            
            //cont.status = 'Sample Contract';
            cont.Booking_Type__c ='Full House';
            cont.MG_Valid_Until__c = 'Date';
            cont.Tenancy_type__C = 'Family';
            cont.Number_of_Bedrooms__c = '5';
            cont.Number_of_Bathrooms__c ='2';
            cont.Base_House_Rent__c = 50;
            cont.Maximum_number_of_tenants_allowed__c = '6';
            cont.Tenancy_Approval_time_period__c = '0 hrs';
            cont.MG_Start_Date__c =  system.today()+1;
            cont.MG_End_Date__c = system.today()+10;
            cont.Who_pays_Move_in_Charges__c = 'NestAway';
            cont.Who_pays_DTH__c = 'NestAway';
            cont.Who_pays_Society_Maintenance__c = 'Owner';
            cont.Furnishing_Type__c='Fully-furnished';
            cont.Who_pays_Electricity_Bill__c = 'Owner';
            cont.SD_Upfront_Amount__c = 50;
            cont.Restrict_SD_to_maximum_SD__c  = True;
            cont.Furnishing_Plan__c = 'NestAway furnished'; 
            cont.Rental_Plan__c = 'Revenue Share';
            cont.Furnishing_Plan__c = 'NestAway furnished';
            cont.Who_pays_Deposit__c = 'NestAway';
            cont.Deposit_Payment_Date__c= System.Today().addDays(5);
            cont.Deposit_Amount_Equivalent_to__c ='0.5 Month';
            cont.Is_MG_Applicable__c ='No';
            cont.MG_Amount__c =1000;
            cont.MG_Applicable__c ='Per bed';
            cont.MG_Payout_Frequency__c ='Monthly';


           // cont.Security_Deposit_Payment_Mode__c = 'Cheque';
            cont.Date_Of_Birth__c= System.today();
            cont.Maximum_SD_Amount__c = 100;
            //cont.Booking_type__C = 'Full House';
            //cont.Furniture_Buyback_Option_Available__c = 'Yes';
            update cont;
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(cont );
            ContractEditControllerExtension testcont = new ContractEditControllerExtension (sc);
              
            PageReference pageRef = Page.ContractEditPage;
            Test.setCurrentPage(pageRef);
            testcont.save();
            testcont.validateNAContracts();
            testcont.calculateAge(cont.Deposit_Payment_Date__c);
            
        Test.StopTest();
        
    }
    
    
    public Static TestMethod void ContractEditTest4(){
        Test.StartTest();
        
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567894';
            insert accObj;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            //cont.status = 'Draft';
            
            insert cont;
            
            //cont.status = 'Sample Contract';
            cont.Booking_Type__c ='Full House';
            cont.MG_Valid_Until__c = 'Date';
            cont.Tenancy_type__C = 'Family';
            cont.Number_of_Bedrooms__c = '5';
            cont.Number_of_Bathrooms__c ='2';
            cont.Base_House_Rent__c = 50;
            cont.Maximum_number_of_tenants_allowed__c = '6';
            cont.Tenancy_Approval_time_period__c = '0 hrs';
            cont.MG_Start_Date__c =  system.today()+1;
            cont.MG_End_Date__c = system.today()+10;
            cont.Who_pays_Move_in_Charges__c = 'NestAway';
            cont.Who_pays_DTH__c = 'NestAway';
            cont.Who_pays_Society_Maintenance__c = 'Owner';
            cont.Furnishing_Type__c='Fully-furnished';
            cont.Who_pays_Electricity_Bill__c = 'Owner';
            cont.SD_Upfront_Amount__c = 50;
            cont.Restrict_SD_to_maximum_SD__c  = True;
            cont.Furnishing_Plan__c = 'NestAway furnished'; 
            cont.Rental_Plan__c = 'Revenue Share';
            cont.Furnishing_Plan__c = 'NestAway furnished';
            cont.Who_pays_Deposit__c = 'Tenant';
            cont.Security_Deposit_Payment_Mode__c = 'Online';
            cont.Deposit_Payment_Date__c= System.Today().addDays(5);
            cont.Deposit_Amount_Equivalent_to__c ='0.5 Month';
            cont.Is_MG_Applicable__c ='No';
            cont.MG_Amount__c =1000;
            cont.MG_Applicable__c ='Per bed';
            cont.MG_Payout_Frequency__c ='Monthly';
            
            cont.Do_You_Want_To_Add_A_POA__c = false;


           // cont.Security_Deposit_Payment_Mode__c = 'Cheque';
            cont.Date_Of_Birth__c= System.today();
            cont.Maximum_SD_Amount__c = 100;
            //cont.Booking_type__C = 'Full House';
            //cont.Furniture_Buyback_Option_Available__c = 'Yes';
            update cont;
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(cont );
            ContractEditControllerExtension testcont = new ContractEditControllerExtension (sc);
              
            PageReference pageRef = Page.ContractEditPage;
            Test.setCurrentPage(pageRef);
            testcont.save();
            testcont.validateNAContracts();
            testcont.calculateAge(cont.Deposit_Payment_Date__c);
            
        Test.StopTest();
        
    }
    
    public Static TestMethod void ContractEditTest5(){
        Test.StartTest();
        
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567894';
            insert accObj;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            //cont.status = 'Draft';
            
            insert cont;
            
            //cont.status = 'Sample Contract';
            cont.Booking_Type__c ='Full House';
            cont.MG_Valid_Until__c = 'Date';
            cont.Tenancy_type__C = 'Family';
            cont.Number_of_Bedrooms__c = '5';
            cont.Number_of_Bathrooms__c ='2';
            cont.Base_House_Rent__c = 50;
            cont.Maximum_number_of_tenants_allowed__c = '6';
            cont.Tenancy_Approval_time_period__c = '0 hrs';
            cont.MG_Start_Date__c =  system.today()+1;
            cont.MG_End_Date__c = system.today()+10;
            cont.Who_pays_Move_in_Charges__c = 'NestAway';
            cont.Who_pays_DTH__c = 'NestAway';
            cont.Who_pays_Society_Maintenance__c = 'Owner';
            cont.Furnishing_Type__c='Fully-furnished';
            cont.Who_pays_Electricity_Bill__c = 'Owner';
            cont.SD_Upfront_Amount__c = 50;
            cont.Restrict_SD_to_maximum_SD__c  = True;
            cont.Furnishing_Plan__c = 'NestAway Rental Package'; 
            cont.Rental_Plan__c = 'Revenue Share';
            cont.Who_pays_Deposit__c = 'Tenant';
            cont.Security_Deposit_Payment_Mode__c = 'Online';
            cont.Deposit_Payment_Date__c= System.Today().addDays(5);
            cont.Deposit_Amount_Equivalent_to__c ='0.5 Month';
            cont.Is_MG_Applicable__c ='No';
            cont.MG_Amount__c =1000;
            cont.MG_Applicable__c ='Per bed';
            cont.MG_Payout_Frequency__c ='Monthly';
            
            cont.Do_You_Want_To_Add_A_POA__c = true;
            cont.POA_Age__c = 30;
            cont.POA_Date__c = System.today();
            cont.POA_Father_Husband_Name__c = 'test';
            cont.POA_Relation_with_Guardian__c = 'Son';
            cont.POA_Permanent_Address__c= 'test';
            cont.POA_Witness_Name__c = 'test';
            cont.POA_Marital_Status__c = 'Married';
            cont.POA_Name__c = 'test';



           // cont.Security_Deposit_Payment_Mode__c = 'Cheque';
            cont.Date_Of_Birth__c= System.today();
            cont.Maximum_SD_Amount__c = 100;
            //cont.Booking_type__C = 'Full House';
            //cont.Furniture_Buyback_Option_Available__c = 'Yes';
            update cont;
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(cont );
            ContractEditControllerExtension testcont = new ContractEditControllerExtension (sc);
              
            PageReference pageRef = Page.ContractEditPage;
            Test.setCurrentPage(pageRef);
            testcont.save();
            testcont.validateNAContracts();
            testcont.calculateAge(cont.Deposit_Payment_Date__c);
            
        Test.StopTest();
        
    }
    
    
    
}