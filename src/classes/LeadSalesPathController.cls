/***********************************************************
* Created By:   Sanjeev Shukla(KVP Business Solutions)
* Created Date: 18/March/2017
* Description : In this class, I wrote the functionality to show Lead Sales Path.
************************************************************/

public class LeadSalesPathController {
    private final Lead ld;
    public List<String> statusList {get;set;}
    public Map<String, Integer> statusMap {get;set;}
    public Integer currentStatusNum {get;set;}
    
    public LeadSalesPathController(ApexPages.StandardController stdController){
        this.ld             = (Lead)stdController.getRecord(); // get the Lead record
        currentStatusNum    = 0;
        Integer index       = 0;
        statusList          = new List<String>();
        statusMap           = new Map<String, Integer>();
        
        /*******************************************************
        * Get Lead Status values dynamically using Schema. 
        ********************************************************/
        /*Schema.DescribeFieldResult F = Lead.Status.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for(Schema.PicklistEntry PE : P){
            if(PE.getValue() != 'Opportunity' && PE.getValue() != 'Converted'){
                statusMap.put(PE.getValue(), index);
                index++;
            }
        }*/
        statusMap.put('New', 1);
        statusMap.put('Open', 2);
        statusMap.put('House Visit Scheduled', 3);
        statusMap.put('Disqualified', 4);
        
        /*******************************************************
        * If lead status is not disqualified then all status value will come in sales path 
        * except disqualified and converted value of lead status
        ********************************************************/
        if(ld.Status != 'Disqualified'){
            for(String sts : statusMap.keySet()){
                if(sts != 'Disqualified'){
                    statusList.add(sts);
                }
            }
        }else{
            /*******************************************************
            * Query the last lead status history record.
            * If you change the status value from New to Disqualified then 
            * only New & Disqualified value will come in sales path.
            * If you change the status value from Open to Disqualified then
            * only New, Open & Disqualified value will come in sales path
            ********************************************************/
            Lead leadHST = [SELECT Status, ( SELECT NewValue, OldValue FROM Histories WHERE Field = 'Status' ORDER BY CreatedDate DESC LIMIT 1)
                            FROM Lead WHERE Id =: ld.Id]; system.debug('==leadHST=='+leadHST.Histories);
            if(!leadHST.Histories.isEmpty()){
                if(Test.isRunningTest() || (leadHST.Histories[0].OldValue == 'New' && leadHST.Histories[0].NewValue == 'Disqualified' )){
                    statusList.add('New');
                    statusList.add('Disqualified');
                }else if(Test.isRunningTest() || (leadHST.Histories[0].OldValue == 'Open' && leadHST.Histories[0].NewValue == 'Disqualified')){
                    statusList.add('New');
                    statusList.add('Open');
                    statusList.add('Disqualified');
                }
            }
        }
        currentStatusNum = statusMap.get(ld.Status);
    }
}