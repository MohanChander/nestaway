@Istest
public class CaseHouseOffboardingTriggerHelperTest {
    Public Static TestMethod Void doTest(){
        //Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        //Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
         id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        //Booking__c b= new Booking__c();
        //b.Tenant__c=accObj.Id;
        //insert b;
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.house__c=hos.id;
        insert b;
         Test.startTest();
       Case c=  new Case();
        c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=3;
        c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
        insert c;
        List<Case>  cList =  new List<Case>();
        cList.add(c);
     
         Map<id,Case> oldmap =  new Map<Id,Case>();
        oldmap.put(c.id,c);
        c.status='Closed';
        c.Settlement_By__c=Constants.CASE_SETTLEMENT_BY_OWNER;
        update c;
         Map<id,Case> newmap =  new Map<Id,Case>();
        newmap.put(c.id,c);
       
        CaseHouseOffboardingTriggerHelper.houseOffboardingCaseInsertValidation(clist);
        CaseHouseOffboardingTriggerHelper.houseOffboardingWorkOrderCreationOnCaseInsert(Clist);
        CaseHouseOffboardingTriggerHelper.OffboardingFinanceCaseCreation(Clist,oldmap);
        CaseHouseOffboardingTriggerHelper.nestSattlementCaseAssignment(cList);
        CaseHouseOffboardingTriggerHelper.updateHouseInitiateOffboarding(CList);
        CaseHouseOffboardingTriggerHelper.ownerSattlementCaseAssignment(cList);
        Test.stopTest();
    }
    
}