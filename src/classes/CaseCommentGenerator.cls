/********************************************
 * Created By : Mohan
 * Purpose    : To Generate Case Comments for different events.
 * ******************************************/
public class CaseCommentGenerator {
	
    public static void generateVisitVerifiedComment(String technicianName, Date visitDate, Time visitTime, Id caseId){
		Case_Comment__c cc = new Case_Comment__c();
		cc.Comment__c = 'Hello, \n' + 
 						'Our technician, ' + technicianName + ', will visit your house on ' + visitDate + ' at ' + visitTime + 'to fix this.\n' + 
 						' \n' +
 						'Thank you for your patience.\n' +
 						'Note: This is an auto-generated message.';
        cc.System_Generated_Comment__c = true;
        insert cc;
    }
    
    public static void generateVisitRescheduledComment(String technicianName, Date visitDate, Time visitTime, Id caseId){
		Case_Comment__c cc = new Case_Comment__c();
		cc.Comment__c = 'Hello, \n' +
 						'There has been a change in the visit to sort this. We\'re sorry as all our technicians have been caught up.\n \n' + 
 						'We have rescheduled the visit to ' + visitDate + ' at ' + visitTime + '. Our technician, ' + technicianName + ', will attend to this.\n' + 
 						'We appreciate your patience.\n \n' +
                        'Note: This is an auto-generated message.';
        cc.System_Generated_Comment__c = true;
        insert cc;
    }
}