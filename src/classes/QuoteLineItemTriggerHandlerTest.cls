@isTest
public Class QuoteLineItemTriggerHandlerTest{

    public Static TestMethod void quoteTest(){
        Test.StartTest();
            Pricebook2 pb = new Pricebook2 ();
    pb.name='NestAway Standard Price Book';
    insert pb;
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            insert accObj;
            
            Product2 prod = new Product2();
            prod.Name = 'Laptop X200'; 
            prod.Family = 'Package A';
            prod.Product_Type__c = 'Furnishing';
            insert prod;
        
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
            insert customPrice;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            insert opp;
            
            Tax__c taxObj = new Tax__c();
           // taxObj.Name = 'TestTax';
            taxObj.Product__c = prod.Id;
            taxObj.State__c = 'Karnataka';
            taxObj.Service_Tax__c = 10;
            taxObj.Vat__c = 5;
            insert taxObj;
              PriceBook2 priceBk = new PriceBook2();
            priceBk.Name = 'TestPriceBook';
           // priceBk.isStandard = FALSE;
            insert priceBk;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            cont.PriceBook2Id = customPB.Id;
            cont.status = 'Draft';
            cont.Tenancy_Type__c = 'Family';
            cont.Furnishing_Package__c = 'Package A';
            insert cont;
            
            Quote quoObj = new Quote();
            quoObj.Name = 'Quote1';
            quoObj.Status = 'Draft';
            quoObj.OpportunityId = opp.Id;
            quoObj.approval_status__c = 'Awaiting Items Manager Approval';
            quoObj.Pricebook2Id = customPB.Id;
            insert quoObj;
            
            QuoteLineItem quoLI = new QuoteLineItem();
            quoLi.QuoteId = quoObj.Id;
            quoLI.Product2Id = prod.Id;
            quoLI.PricebookEntryId = customPrice.Id;
            
            quoLI.UnitPrice = 40000;
            quoLi.Quantity = 4;
            insert quoLI;
               
        Test.StopTest();
    }
}