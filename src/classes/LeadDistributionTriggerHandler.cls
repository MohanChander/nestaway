/*
* Description: It's trigger handler class of Lead Distribution object's trigger, Here we perform round robin algorithem.
*/

public class LeadDistributionTriggerHandler { 
     public static void afterInsertHandler(List<Lead_Distribution__c> newList, Map<Id, Lead_Distribution__c> newMap){
        Set<id> parentId=new Set<id>();
        for(Lead_Distribution__c led : newList){
            if(led.Round_Robin__c != null){
               parentId.add(led.Round_Robin__c);
            }
        }
        if(!parentId.isEmpty()){
           updateRoundRobin(parentId);
        }
        
    }
    
    public static void afterupdateHandler(List<Lead_Distribution__c> newList, Map<Id, Lead_Distribution__c> newMap,Map<Id, Lead_Distribution__c> oldMap){
        Set<id> parentId=new Set<id>();
        list<Lead_Distribution__c> updateOwner=new list<Lead_Distribution__c>(); 
        for(Lead_Distribution__c led : newList)
        {
            if(led.Round_Robin__c != null)
            {
               parentId.add(led.Round_Robin__c);
            }
           if(led.User__c!=null && led.User__c!=oldMap.get(Led.id).User__c)
            {
                updateOwner.add(led);
             }
        }
        if(!updateOwner.isEmpty())
        { System.debug('bony');
          UpdateleadAccOppCont(updateOwner,oldmap);
        }
        if(!parentId.isEmpty()){
           updateRoundRobin(parentId);
        }
    }

/*    ***************************************************************************************************************************************************
    Added by Baibhav
    ****************************************************************************************************************************************************/
        public static void UpdateleadAccOppCont(List<Lead_Distribution__c> ldLIst, Map<Id,Lead_Distribution__c> oldMap)
        {
          Map<id,id> lduserIDMap = new Map<id,id>();
          System.debug('bony2');   
          // Created a amp of Old userid and new
          for(Lead_Distribution__c ld:ldLIst)
          {
           lduserIDMap.put(oldMap.get(ld.id).User__c,ld.User__c);
          }
           List<lead> updateLeadList =new List<lead>();
           List<Account> updateAccList = new List<Account>(); 
           List<Opportunity> updateOppList =new List<Opportunity>();
           List<Contract> updateContrList = new List<Contract>();

           List<lead> leadList = [Select id,ownerId,isConverted from Lead where ownerid=:lduserIDMap.keySet()];
           List<Account> accList = [Select id,ownerId from Account where ownerid=:lduserIDMap.keySet()]; 
           List<Opportunity> oppList = [Select id,ownerId from Opportunity where ownerid=:lduserIDMap.keySet()]; 
           List<Contract> contrList = [Select id,ownerId from Contract where ownerid=:lduserIDMap.keySet()]; 

           for(id oldId:lduserIDMap.keySet())
           { 
            if(!leadList.isEmpty())
            { System.debug('bony3');
              for(Lead le:leadList)
              {
                if(le.isConverted==false && le.ownerid==oldId)
                { 
                   le.ownerid=lduserIDMap.get(oldId);
                   updateLeadList.add(le);
                }
             }  
           }
           if(!accList.isEmpty())
            { System.debug('bony4');
              for(Account ac:accList)
              { 
                if(ac.ownerid==oldId)
                {
                   ac.ownerid=lduserIDMap.get(oldId);
                   updateAccList.add(ac);
                }
             }  
           }
            if(!oppList.isEmpty())
            { System.debug('bony5');
              for(Opportunity ac:oppList)
              {
                if(ac.ownerid==oldId)
                {
                   ac.ownerid=lduserIDMap.get(oldId);
                   updateOppList.add(ac);
                }
             }  
           }
            if(!contrList.isEmpty())
            { System.debug('bony6');
              for(Contract ac:contrList)
              {
                if(ac.ownerid==oldId)
                {
                   ac.ownerid=lduserIDMap.get(oldId);
                   updateContrList.add(ac);
                }
             }  
           }
        }
        if(!updateLeadList.isEmpty())
        Update updateLeadList;

        if(!updateAccList.isEmpty())
         Update updateAccList;

         if(!updateOppList.isEmpty())
          Update updateOppList;

          if(!updateContrList.isEmpty())
           Update updateContrList;
    }
    /*************
      @Method Name:afterdeleteHandler
      @Param : List of Lead Assignment  
      @return type : void 
      @ Description: .
    **************/
    public static void afterdeleteHandler(List<Lead_Distribution__c> oldList){
        Set<id> parentId=new Set<id>();
        for(Lead_Distribution__c ldAsgn : oldList){
            if(ldAsgn.Round_Robin__c != null){
               parentId.add(ldAsgn.Round_Robin__c);
            }
        }
        if(!parentId.isEmpty()){
           updateRoundRobin(parentId);
        }
    }
    
    /*************
      @Method Name:updateRoundRobin
      @Param : Set of round robin id 
      @return type : void 
      @ Description:This method to update Total leads and total assignment percent in related round robin .
    **************/
    public static void updateRoundRobin(Set<Id> parentIdSet){
        List<Round_Robin__c> rRbnLst=new List<Round_Robin__c>();
        for(AggregateResult sr:[SELECT Round_Robin__c t,sum(Number_of_Leads__c) e,sum(Assigned__c) f FROM Lead_Distribution__c  WHERE  Round_Robin__c =: parentIdSet Group By Round_Robin__c]){
                Round_Robin__c rr =new Round_Robin__c();
                rr.id=(id)sr.get('t');
                rr.Total_Leads__c=(decimal)sr.get('e');
                rr.Total_Assigned_Percentage__c = (decimal)sr.get('f');
                rRbnLst.add(rr);
        }
        if(rRbnLst.size()>0){   
            try{
                update rRbnLst;
            }catch(DmlException e){
                System.debug('Error Caught '+e);
            }
        }
    }
}