public class BankDetailTriggerHelper 
{
    //Added by baibhav
    // Purpose: to populate Bank name
    public static void bankName(List<Bank_Detail__c> newlist)
    {
        for(Bank_Detail__c bk:newlist)
        {   
            if(bk.Account_Firstname__c!=null)
            {
             bk.name=bk.Account_Firstname__c;
            }
                      
            if(bk.Bank_Name__c!=null)
            {
             bk.name+='-'+bk.Bank_Name__c;
            }
            if(bk.Account_Number__c!=null)
            {
               bk.name+='-'+bk.Account_Number__c.left(4);
            }
        } 
       
     
    }
}