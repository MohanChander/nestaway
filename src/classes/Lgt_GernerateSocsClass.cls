global without sharing class Lgt_GernerateSocsClass {
    public Lgt_GernerateSocsClass(ApexPages.StandardController controller) {
        
    }
    public  pageReference generateSdoc(){
        String sourceId= ApexPages.currentPage().getParameters().get('sourceId');
        Contract cont=[Select id,Booking_Type__c, Agreement_Generation_Date__c  from contract where id=:sourceId];
        cont.Agreement_Generation_Date__c = System.now();
        update cont;
        String currentUserId=userinfo.getuserid();
        user u= [select name,username from user where id =:currentUserId];
        id IdOfcont =cont.id;
        SDOC__SDTemplate__c  SDOCNestawayFamilyAgreement=[Select id from SDOC__SDTemplate__c  where name='Nestaway Family Agreement'];
        SDOC__SDTemplate__c  SDOCNestawayTenantAgreement=[Select id from SDOC__SDTemplate__c  where name='Nestaway Tenant Agreement'];
        if(cont.Booking_Type__c =='Full House'){
            SDOC.SDBatch.CreateSDocSync ('',u.username,'id='+IdOfcont+'&Object=Contract&doclist='+SDOCNestawayFamilyAgreement.id+'&sendEmail=0');      
        }
        else{
            SDOC.SDBatch.CreateSDocSync ('',u.username,'id='+IdOfcont+'&Object=Contract&doclist='+SDOCNestawayTenantAgreement.id+'&sendEmail=0');         
        }
        pageReference ref = new PageReference('/one/one.app#/sObject/'+cont.id); 
        ref.setRedirect(true); 
        return ref; 
    }
}