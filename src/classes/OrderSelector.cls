/* Created By: Mohan - WarpDrive Tech Works
   Created Date: 18/05/2017
   Purpose   : All the queries performed on the Order Object are here in one place */

public with sharing class OrderSelector {

	//fetch all the Orders with OrderItems from Order Id Set
	public static List<Order> getOrderWithOLIFromIdSet(Set<Id> orderIdSet){

		return [select Id, AccountId, Pricebook2Id, House__c, (select Id, Vendor__c, Quantity, UnitPrice,
		        PricebookEntryId from OrderItems) from Order where Id =: orderIdSet];
	}
}