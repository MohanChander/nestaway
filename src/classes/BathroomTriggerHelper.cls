/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for Bathroom Trigger
**********************************************/

public class BathroomTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : When the MIMO BIC is updated - Update all the open Move in and Move Out BIC's
**********************************************/
    public static void syncMoveInMoveOutChecklistsWithMIMO(List<Bathroom__c> mimoBicList){

        System.debug('*******************syncMoveInMoveOutChecklistsWithMIMO');

        try{
                Set<Id> bicIdSet = new Set<Id>();
                Map<Id, Bathroom__c> mimoBicMap = new Map<Id, Bathroom__c>();
                List<Bathroom__c> UpdatedBicList = new List<Bathroom__c>();

                for(Bathroom__c mimoBic: mimoBicList){
                    bicIdSet.add(mimoBic.CheckFor__c); 
                    mimoBicMap.put(mimoBic.CheckFor__c, mimoBic);
                }    

                //query for the unverified Move In and Move Out Bic checklists
                List<Bathroom__c> queriedBicList = BathroomSelector.getUnverifiedChecklists(bicIdSet);

                for(Bathroom__c bic: queriedBicList){
                    Bathroom__c updatedBic = new Bathroom__c();
                    updatedBic = mimoBicMap.get(bic.CheckFor__c).clone(false, true, false, false);
                    updatedBic.Id = bic.Id;
                    updatedBic.Name = bic.Name;
                    updatedBic.CheckList_Verified__c = 'No';
                    updatedBic.Work_Order__c = bic.Work_Order__c;
                    updatedBic.RecordTypeId = bic.RecordTypeId;
                    updatedBic.Type_Of_BIC__c = bic.Type_Of_BIC__c;
                    updatedBic.MIMO_Comment__c = bic.MIMO_Comment__c;
                    UpdatedBicList.add(updatedBic);
                }

                if(!UpdatedBicList.isEmpty()){
                    update UpdatedBicList;
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Sync Move In Move Out HIC with MIMO HIC');      
            }                    
    }

}