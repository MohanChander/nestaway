/***********************************************************
* Created By:   Sanjeev Shukla(KVP Business Solutions)
* Created Date: 16/March/2017
* Description : It's handler of QuoteLineItem trigger.
************************************************************/

public class QuoteLineItemTriggerHandler {

    /*******************************************************
    * Map the fields(Service Tax and Vat) of QuoteLineItem based on state and product from Tax object records. 
    ********************************************************/
    public static void updateServiceAndVatTax(List<QuoteLineItem> qLIList){

        Set<String> stateSet            = new Set<String>(); // declare the set variable to keep the state of all QuoteLineItem
        Set<String> productIdSet        = new Set<String>(); // declare the set variable to keep the productId of all QuoteLineItem

        /*******************************************************
        * Query the opportunity state for each QuoteLineItem
        ********************************************************/
        Set<Id> quoteIdSet = new Set<Id>();
        for(QuoteLineItem qLIObj : qLIList){
            quoteIdSet.add(qLIObj.QuoteId);
        }
        Map<Id, Quote> quoteMap     = new Map<Id, Quote>([SELECT Id, Opportunity.State__c FROM Quote WHERE Id IN : quoteIdSet]);
        for(QuoteLineItem qLIObj : qLIList){
            stateSet.add(quoteMap.get(qLIObj.QuoteId).Opportunity.State__c);
            productIdSet.add(qLIObj.Product2Id);
        }
        try {
            Map<String, Tax__c> taxMap = new Map<String, Tax__c>();

            /*******************************************************
            * query tax object record based on set of state and productId of the QuoteLineItem.
            ********************************************************/
            for(Tax__c tax : [SELECT Id, Service_Tax__c, Vat__c, Krishi_Kalyan_Cess__c, Swachch_Bharat_Cess__c, Product__c, State__c FROM Tax__c WHERE State__c IN : stateSet AND Product__c IN : productIdSet]){
                if (tax.State__c != NULL && tax.State__c != '' && tax.Product__c != NULL) {

                    /*******************************************************
                    * put the tax records in a map of tax object keys are the combination of state and productid
                    ********************************************************/
                    taxMap.put(tax.State__c.toUpperCase()+'_'+String.valueOf(tax.Product__c), tax);
                }
            }
            if (!taxMap.isEmpty()) {
                for(QuoteLineItem qLI : qLIList){
                    if (quoteMap.get(qLI.QuoteId).Opportunity.State__c != NULL && qLI.Product2Id != NULL && taxMap.containsKey(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id)) {

                        /*******************************************************
                        * assign the service tax and vat value from tax object records
                        ********************************************************/
                        if (taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Service_Tax__c != NULL) {
                            qLI.Service_Tax__c = taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Service_Tax__c;
                            qLI.Service_Amount__c = (qli.TotalPrice * qli.Service_Tax__c)/100;
                        }
                        if (taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Vat__c != NULL) {
                            qLI.Vat__c = taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Vat__c;
                            qLI.Vat_Amount__c = (qli.TotalPrice * qli.Vat__c)/100;
                        }
                        if (taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Krishi_Kalyan_Cess__c != NULL) {
                            qLI.Krishi_Kalyan_Cess__c = taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Krishi_Kalyan_Cess__c;
                            qLI.Krishi_Kalyan_Cess_Amount__c = (qli.TotalPrice * qLI.Krishi_Kalyan_Cess__c)/100;
                        }
                        if (taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Swachch_Bharat_Cess__c != NULL) {
                            qLI.Swachch_Bharat_Cess__c = taxMap.get(quoteMap.get(qLI.QuoteId).Opportunity.State__c.toUpperCase()+'_'+qLI.Product2Id).Swachch_Bharat_Cess__c;
                            qLI.Swachch_Bharat_Cess_Amount__c = (qli.TotalPrice * qli.Swachch_Bharat_Cess__c)/100;
                        }

                    }
                }
            }
        }catch (Exception e) {}
    }

    /*******************************************************
    * Validate the product family's value of related product with contract's furnishing package's value of related oppportunity.
    ********************************************************/

    public static void validate(List<QuoteLineItem> qLIList){
        Set<String> quoteIdSet          = new Set<String>(); // declare the set variable to keep the quoteId of all QuoteLineItem
        Set<String> productIdSet        = new Set<String>(); // declare the set variable to keep the productId of all QuoteLineItem
        for(QuoteLineItem qLIObj : qLIList){
            quoteIdSet.add(qLIObj.QuoteId);
            productIdSet.add(qLIObj.Product2Id);
        }
        try {

            /*******************************************************
            * Query quote for opportunity id
            ********************************************************/
            List<Quote> qouteList = [SELECT Id, OpportunityId FROM Quote WHERE Id IN : quoteIdSet];

            /*******************************************************
            * declare set variable to keep id of all oppportunities.
            ********************************************************/
            Set<Id> oppIdSet = new Set<Id>();
            for(Quote qt : qouteList){
                oppIdSet.add(qt.OpportunityId);
            }

            /*******************************************************
            * query oppportunity record as well as only one contract record because for our business for each opportunity we have only one contract.
            * we query the contacts info because we have to comapare contracts's furnishing packake value should be same as related product family value of QuoteLineItem
            ********************************************************/
            Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, (SELECT Id, Furnishing_Package__c FROM Contracts__r WHERE Furnishing_Package__c != NULL LIMIT 1) FROM Opportunity WHERE Id IN : oppIdSet]);

            /*******************************************************
            * query product data with family field.
            ********************************************************/
            Map<Id, Product2> prodMap = new Map<Id, Product2>([SELECT Id, Family FROM Product2 WHERE Id IN : productIdSet AND Family != NULL]);

            /*******************************************************
            * declare and define a map to keep key as quote id and value as contracts's furnishing packake value .
            ********************************************************/
            Map<Id, String> qtWithContractMap = new Map<Id, String>();
            for(Quote qt : qouteList){
                if (!oppMap.get(qt.OpportunityId).Contracts__r.isEmpty() ) {
                    qtWithContractMap.put(qt.Id, oppMap.get(qt.OpportunityId).Contracts__r[0].Furnishing_Package__c);
                }
            }
            for(QuoteLineItem qLIObj : qLIList){

                /*******************************************************
                * compare contracts's furnishing packake with product family value.
                * If both are equal then QuoteLineItem will be create,
                * otherwise QuoteLineItem record wont create.
                * Even contracts's furnishing packake value is null or none then QuoteLineItem record wont create.
                ********************************************************/
                if(qtWithContractMap.get(qLIObj.QuoteId) == NULL || qtWithContractMap.get(qLIObj.QuoteId) != prodMap.get(qLIObj.Product2Id).Family){
                    qLIObj.addError('Please choose same package which is selected at related contract or contract\'s package should not be none.');
                }
            }
        }
        catch (Exception e) {

        }
    }

    /*
    *Created By: Poornapriya(KVP Business Solutions)
    */
    /*******************************************************
    Update Quote Object based on Product type of QuoteLineItem Object
    ***********************************************************/
    public static void updateQuoteType(List<QuoteLineItem >quoteLIList, Boolean isInsert){
        /*
        * here we use "isInsert" to execute part of code only when record will be create not update.
        */
        Set<Id> setOfQuoteId = new Set<Id>();
        Set<String> productIdSet        = new Set<String>(); 
        Map<Id,List<QuoteLineItem >> MapOfProduct= new Map<Id,List<QuoteLineItem>>();
        List<Quote>quoteToUpdate = new List<Quote>();
        for(QuoteLineItem qLI:quoteLIList ){
            setOfQuoteId.add(qLI.QuoteId);
            productIdSet.add(qLI.Product2Id);
        }
        Map<Id, Product2> prodMap;
        if(isInsert){
            prodMap = new Map<Id, Product2>([SELECT Id,Product_Type__c FROM Product2 WHERE Id IN : productIdSet]);
        }
        Map<Id, Quote> quoteMap     = new Map<Id, Quote>([SELECT Id,Furnishing__c,Service_Maintenance__c,Property_Management__c FROM Quote WHERE Id IN : setOfQuoteId]);
        for(QuoteLineItem  qtLi : quoteLIList){
            if(isInsert && prodMap.containsKey(qtLi.Product2Id) && prodMap.get(qtLi.Product2Id)!= Null ) {
                if(prodMap.get(qtLi.Product2Id).Product_Type__c == 'Furnishing'){
                    quoteMap.get(qtLi.QuoteId).Furnishing__c = TRUE;
                }
                else if(prodMap.get(qtLi.Product2Id).Product_Type__c== 'Property Management'){
                    quoteMap.get(qtLi.QuoteId).Property_Management__c  = TRUE;
                }
                else if(prodMap.get(qtLi.Product2Id).Product_Type__c == 'Service & Maintenance'){
                    quoteMap.get(qtLi.QuoteId).Service_Maintenance__c = TRUE;
                }
            }

            // Edited By: Sanjeev Shukla (KVP Business Solutions)

            // From here *****************************
            if(qtLi.Service_Tax__c != NULL){
                quoteMap.get(qtLi.QuoteId).Service_Tax__c = qtLi.Service_Tax__c;
            }
            if(qtLi.Vat__c != NULL){
                quoteMap.get(qtLi.QuoteId).Vat__c = qtLi.Vat__c;
            }
            if(qtLi.Krishi_Kalyan_Cess__c != NULL){
                System.debug('*************qtLi.Krishi_Kalyan_Cess__c***************** '+qtLi.Krishi_Kalyan_Cess__c);
                quoteMap.get(qtLi.QuoteId).Krishi_Kalyan_Cess__c = qtLi.Krishi_Kalyan_Cess__c;
            }
            if(qtLi.Swachch_Bharat_Cess__c != NULL){
                quoteMap.get(qtLi.QuoteId).Swachch_Bharat_Cess__c = qtLi.Swachch_Bharat_Cess__c;
            }
            //To here ********************************
        } 
        try{
            if(quoteMap.values().size()>0){
                update quoteMap.values();
            }
        }
        catch(System.DmlException e){
            for(QuoteLineItem  qtLi : quoteLIList){
                qtLi.addError(e.getDmlMessage(0));
            }
            system.debug('===Exception==='+e);
        }

    }

}