/*  Created By   : Mohan - WarpDrive Tech Works
    Created Date : 22/05/2017
    Purpose      : All the Trigger functionality will be handled in the Handler Class */

public class InvoiceTriggerHandler {

    // method to update the House Economics - Executed After Inset, After Delete and After Undelete
    public static void updateHouseEconomics(List<Invoice__c> newInvoices){

        System.debug('updateHouseEconomics is executed: ' + ' Invoices triggered: + ' + newInvoices);
        Set<Id> houseIdSet = new Set<Id>();

        for(Invoice__c inv: newInvoices){
            if(inv.Category__c != null && inv.House__c != null)
                houseIdSet.add(inv.House__c);
        }

        // send the House Id's to the Summation class
        if(!houseIdSet.isEmpty())
            HouseEconomicsSummation.houseEconomicsSummation(houseIdSet);
    } // end of the method - updateHouseEconomics

    // method will be executed after update - Update the House Economics on change of Invoice Amount
    public static void updateHouseEconomicsOnUpdate(Map<Id, Invoice__c> newMap, Map<Id, Invoice__c> oldMap){

        Set<Id> houseIdSet = new Set<Id>();

        for(Invoice__c inv: newMap.values()){
            if(inv.Amount__c != oldMap.get(inv.Id).Amount__c && inv.House__c != null){
                houseIdSet.add(inv.House__c);
            }
        }

        // send the House Id's to the Summation class
        HouseEconomicsSummation.houseEconomicsSummation(houseIdSet);
    }  // end of the method - updateHouseEconomicsOnUpdate
}