/*******************************************
 * Created By: Mohan
 * Purpose   : Trigger Helper for Received Product
 * *****************************************/
public class ReceivedProductTriggerHelper {

/*******************************************
 * Created By: Mohan
 * Purpose   : update the Quantity on the Order Product when Quantity on received Product is updated
 * *****************************************/    
    public static void updateQuantityOnOrderProduct(List<Received_Product__c> recProdList){
        try{
                Set<Id> orderItemIdSet = new Set<Id>();
                List<OrderItem> orderItemList = new List<OrderItem>();
                
                for(Received_Product__c recProd: recProdList){
                    if(recProd.Order_Product__c != null){
                        orderItemIdSet.add(recProd.Order_Product__c);
                    }
                }
                
                List<AggregateResult> groupedResult;
                if(!orderItemIdSet.isEmpty()){
                    groupedResult = [select Order_Product__c, Sum(Received_Quantity__c) recQty from Received_Product__c
                                                           where Order_Product__c =: orderItemIdSet
                                                           group by Order_Product__c];
                }
                
                for(AggregateResult ar: groupedResult){
                    OrderItem oi = new OrderItem();
                    oi.Received_Quantity__c = (Integer)ar.get('recQty');
                    orderItemList.add(oi);
                }
                
                if(!orderItemList.isEmpty()){
                    update orderItemList;
                }
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'dummy');      

            }   
    }
    
/*******************************************
 * Created By: Mohan
 * Purpose   : update the Invoice Amount
 * *****************************************/    
    public static void updateInvoiceAmount(List<Received_Product__c> recProdList){
        try{
                Set<Id> InvoiceIdSet = new Set<Id>();
                List<Invoice__c> InvoiceList = new List<Invoice__c>();
                
                for(Received_Product__c recProd: recProdList){
                    if(recProd.Invoice__c != null){
                        InvoiceIdSet.add(recProd.Invoice__c);
                    }
                }
                
                List<AggregateResult> groupedResult;
                if(!InvoiceIdSet.isEmpty()){
                    groupedResult = [select Invoice__c, Sum(Amount__c) amount from Received_Product__c
                                                           where Invoice__c =: InvoiceIdSet
                                                           group by Invoice__c];
                }
                
                for(AggregateResult ar: groupedResult){
                    Invoice__c inv = new Invoice__c();
                    inv.Amount__c = (Integer)ar.get('amount');
                    InvoiceList.add(inv);
                }
                
                if(!InvoiceList.isEmpty()){
                    update InvoiceList;
                }
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'dummy');      

            }   
    } 
}