/*  Created By : Mohan
    Purpose    : Utility Class to create Test Data to run the test Classes */
@isTest
public class TestClassUtility {

    //Create Test Entitlement Data
    @isTest
    public static void createTestEntitilement(){

        Account acc = new Account();
        acc.Name = 'NestAway';
        insert acc;

        Account acc1 = new Account();
        acc1.FirstName = 'NestAway';
        acc1.LastName = 'Account';
        acc1.Phone = '9738221961';
        acc1.RecordTypeId = [select Id from RecordType where Name = 'Person Account'].Id;
        acc1.PersonEmail = 'api_user@nestaway.com';
        insert acc1;        


       Contact con = new Contact(FirstName='john', LastName='doe', Email='john@doe.com', AccountId=acc.Id);
        insert con;

        Asset ass = new Asset(AccountId=acc.Id,ContactId=con.Id, Name='testing');
        insert ass;

        Entitlement ent = new Entitlement(Name='Testing',  
                                          AccountId=acc.Id, 
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)),
                                          AssetId=ass.Id);
        insert ent;     
    }
}