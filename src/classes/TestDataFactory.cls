/**************************************************
Created By : Mohan
Purpose    : DataFactory Class to create Test Data that can be accessed in the Test Classes
**************************************************/

@isTest
public class TestDataFactory  {

/**************************************************
Created By : Mohan
Purpose    : Creates Zone and Zone User Mapping Records for the given RecordTypes
**************************************************/

    public static void createZoneAndUserMappingRecords(Id zoneRecordTypeId, Id UserMappingRecordTypeId) {

        User Zom = Test_library.createStandardUser(1);

        Zone__c zone = new Zone__c();
        zone.Name = 'All Mumbai';
        zone.Zone_code__c = '123';
        zone.ZOM__c = Zom.Id;
        Insert Zone;

        Zone_and_OM_Mapping__c zu = new Zone_and_OM_Mapping__c();
        zu.Zone__c = zone.Id;
        zu.User__c = zom.Id;
        Insert zu;

    }
    
    public static House_Inspection_Checklist__c createHouseWithChecklists(House__c hos) {

                List<Room_Inspection__c> ricList = new List<Room_Inspection__c>();
                List<Bathroom__c> bicList = new List<Bathroom__c>();

        /*
                //create Zone
                zone__c zc= new zone__c();
                zc.Zone_code__c ='123';
                zc.Name='Test';
                insert zc;

                //create House
                House__c hos= new House__c();
                hos.name='house1';
                hos.Onboarding_Zone__c=zc.id;
                hos.Onboarding_Zone_Code__c = '123';
                hos.Assets_Created__c=false;
                hos.Furnishing_Type__c='Fully Furnished';
                insert hos;
                */

                //create HIC
                House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
                hic.name='test';
                hic.TV__c='Yes and Working';
                hic.House__c=hos.Id;
                hic.Fridge__c='Yes and Working';
                hic.Washing_Machine__c='Yes and Working';
                hic.Type_Of_HIC__c='House Inspection Checklist';
                hic.Kitchen_Package__c='Completely Present';
                insert hic;

                //Create room Inspection
                Room_Inspection__c ric = new Room_Inspection__c();
                ric.Name = 'Room Inspection 1';
                ric.House__c = hos.Id;
                ric.House_Inspection_Checklist__c = hic.Id;
                ricList.add(ric);

                Room_Inspection__c ric2 = new Room_Inspection__c();
                ric2.Name = 'Room Inspection 2';
                ric2.House__c = hos.Id;
                ric2.House_Inspection_Checklist__c = hic.Id;
                ricList.add(ric2);   

                insert ricList; 

                //Create Bathroom
                Bathroom__c b1 = new Bathroom__c();
                b1.Checklist__c = hic.Id;
                b1.Type__c = 'Common';
                bicList.add(b1);

                Bathroom__c b2 = new Bathroom__c();
                b2.Room_Inspection__c = ric.Id;  
                b2.Type__c = 'Attached';
                bicList.add(b2);

                insert bicList;

                return hic;

    }
    
}