/*  Created By   : Deepak - WarpDrive Tech Works
    Created Date : 18/05/2017
    */

public class CaseServiceRequestHandler {
    
    /*  Created By : Mohan 
        Purpose    : When a Service Request Case is Created/ Updated with Problem - Problem related info to be populated 
        on the Case */
    public static void UpdateProblemRelatedInfo(List<Case> caseList){
        try{
            Map<Id, Problem__c> problemMap = new Map<Id, Problem__c>(ProblemSelector.getProblemList());
            
            for(Case c: caseList){
                c.Queue__c = problemMap.get(c.Problem__c).Queue__c;
                c.Require_visit__c = problemMap.get(c.Problem__c).Scheduled_Visit_Required__c;
                c.Priority = problemMap.get(c.Problem__c).Priority__c;
                c.Subject = problemMap.get(c.Problem__c).Name;
                c.Subject_for_Escalation_Email__c = problemMap.get(c.Problem__c).Name.replace('➢','-->');
            }
        } catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e);      
        }
    }
    
    
    /*  Created By : Mohan
        Pupose     : On change of Escalation Level update the Next Escalation to field on the Case */  
    @future 
    public static void onCaseEsclation(Set<Id> caseIdSet){
        
        try{
            set<Id> userIdSet = new Set<Id>();
            Map<Id, User> managerMap = new Map<Id, User>();
            List<Case> caseList = CaseSelector.getCasesFromCaseIdSet(caseIdSet);
            
            for(Case c: caseList){
                if(c.Next_Escalation_to__c != null)
                    userIdSet.add(c.Next_Escalation_to__c);
                else if(c.OwnerId.getSObjectType() == User.SObjectType){
                    c.Next_Escalation_to__c = c.OwnerId;
                    userIdSet.add(c.OwnerId);
                }
            }
            
            for(User u: UserSelector.getUserDetails(userIdSet)){
                managerMap.put(u.Id, u);
            }
            
            for(Case c: caseList){
                if(c.Next_Escalation_to__c != null){
                    c.Next_Escalation_Email__c = !managerMap.isEmpty() && managerMap.containsKey(c.Next_Escalation_to__c) && managerMap.get(c.Next_Escalation_to__c) != null ? managerMap.get(c.Next_Escalation_to__c).Manager.Email : c.Next_Escalation_Email__c;                    
                    c.Next_Escalation_to__c  = !managerMap.isEmpty() && managerMap.containsKey(c.Next_Escalation_to__c) && managerMap.get(c.Next_Escalation_to__c) != null ? managerMap.get(c.Next_Escalation_to__c).ManagerId : c.Next_Escalation_to__c;
                } 
            }
            
            //auto completion of the Milestones
            completeMilestones(caseIdSet);
            
            //switch off the Case Trigger before updation
            if(!caseList.isEmpty()){
                StopRecursion.CASE_SWITCH = false;
                update caseList;
                StopRecursion.CASE_SWITCH = true;
            }
        } catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e);
        }
    } 
    
    /*  Created by : Mohan
        Purpose    : Complete the Milestones for the given CaseIdSet */
    public static void completeMilestones(Set<Id> caseIdSet){
        
        List<CaseMilestone> cmList = [select Id, CompletionDate, CaseId from CaseMilestone where CaseId =: caseIdSet and CompletionDate = null];
        
        for(CaseMilestone cm: cmList){
            cm.CompletionDate = System.now();
        }
        
        update cmList;
    }

    /*  Created by : Mohan
        Purpose    : Populate the Next Email to field when Owner is changed
                   : Manager to Whome SLA voilation mail has to go */
    public static void populateNextEmailTofield(List<Case> caseList){

        try{
                Set<Id> userIdSet = new Set<Id>();

                for(Case c: caseList){
                    if(c.OwnerId.getSObjectType() == User.SObjectType)
                        userIdSet.add(c.OwnerId);
                }

                Map<Id, User> userMap = new Map<Id, User>(UserSelector.getUserDetails(userIdSet));

                for(Case c: caseList){
                    if(c.OwnerId.getSObjectType() == User.SObjectType){
                        c.Next_Escalation_Email__c = userMap.get(c.OwnerId).Manager.Email;
                        c.Next_Escalation_to__c = userMap.get(c.OwnerId).ManagerId;
                    }
                }
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }
    }
}