@isTest
public Class  BathroomSelectorTest {
    public Static TestMethod void testMethod1(){
        Set<Id> setOfOppid = new Set<Id>();
        Set<Id> setOfHic = new Set<Id>();
          Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            insert opp;
        setOfOppid.add(opp.Id);
         House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;
        setOfHic.add(hic.Id);
        BathroomSelector.getBathroomsForOpportunities(setOfOppid);
        BathroomSelector.getBathroomCountForHIC(setOfHic);
    }
}