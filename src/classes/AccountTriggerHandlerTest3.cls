@istest
public class AccountTriggerHandlerTest3 {
    public Static TestMethod void contractTest(){
        Test.StartTest();
        Account accObj = new Account();
        // accObj.FirstName='test';
        // accObj.LastName='test';
        //accObj.PersonEmail='test@gmail.com';
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        insert cont;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        
        Photo__c ph= new Photo__c ();   
          ph.Tenant__c=accObj.id;
        ph.Is_Current_Document__c=true;
        ph.Approval_Status__c='Approved';
        insert ph;
       
        
        Bank_Detail__c	bdetail = new Bank_Detail__c();
        bdetail.Related_Account__c=accObj.Id;
        bdetail.Name='test';
        bdetail.Account_Number__c='837368283662896';
        bdetail.Bank_Name__c ='Axis';
        insert bdetail;
        
        Map<Id,Account> mapAcount = new Map<Id,Account>();
        mapAcount.put(accObj.id,accObj);
        AccountTriggerHandler.checkTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyIfTenantDocumentsarePresent(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantDocuments(mapAcount,mapAcount);
        AccountTriggerHandler.beforeUpdate(mapAcount,mapAcount);
        
    }
     public Static TestMethod void contractTest2(){
        Test.StartTest();
        Account accObj = new Account();
        // accObj.FirstName='test';
        // accObj.LastName='test';
        //accObj.PersonEmail='test@gmail.com';
        accObj.Name = 'TestAcc';
         accObj.Employment_Type__c='I am employed';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        insert cont;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        
        Photo__c ph= new Photo__c ();   
          ph.Tenant__c=accObj.id;
        ph.Is_Current_Document__c=true;
        ph.Approval_Status__c='Approved';
        insert ph;
       
        
        Bank_Detail__c	bdetail = new Bank_Detail__c();
        bdetail.Related_Account__c=accObj.Id;
        bdetail.Name='test';
        bdetail.Account_Number__c='837368283662896';
        bdetail.Bank_Name__c ='Axis';
        insert bdetail;
        
        Map<Id,Account> mapAcount = new Map<Id,Account>();
        mapAcount.put(accObj.id,accObj);
        AccountTriggerHandler.checkTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyIfTenantDocumentsarePresent(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantDocuments(mapAcount,mapAcount);
        AccountTriggerHandler.beforeUpdate(mapAcount,mapAcount);
        
    }
     public Static TestMethod void contractTest3(){
        Test.StartTest();
        Account accObj = new Account();
        // accObj.FirstName='test';
        // accObj.LastName='test';
        //accObj.PersonEmail='test@gmail.com';
        accObj.Name = 'TestAcc';
         accObj.Employment_Type__c='I am self employed';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        insert cont;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        
        Photo__c ph= new Photo__c ();   
          ph.Tenant__c=accObj.id;
        ph.Is_Current_Document__c=true;
        ph.Approval_Status__c='Approved';
        insert ph;
       
        
        Bank_Detail__c	bdetail = new Bank_Detail__c();
        bdetail.Related_Account__c=accObj.Id;
        bdetail.Name='test';
        bdetail.Account_Number__c='837368283662896';
        bdetail.Bank_Name__c ='Axis';
        insert bdetail;
        
        Map<Id,Account> mapAcount = new Map<Id,Account>();
        mapAcount.put(accObj.id,accObj);
        AccountTriggerHandler.checkTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyIfTenantDocumentsarePresent(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantDocuments(mapAcount,mapAcount);
        AccountTriggerHandler.beforeUpdate(mapAcount,mapAcount);
        
    }
    public Static TestMethod void contractTest4(){
        Test.StartTest();
        Account accObj = new Account();
        // accObj.FirstName='test';
        // accObj.LastName='test';
        //accObj.PersonEmail='test@gmail.com';
        accObj.Name = 'TestAcc';
         accObj.Employment_Type__c='I am Student';
         accObj.ShippingStreet='lakeview';
              accObj.ShippingCity='Bangalore';
              accObj.ShippingState='Karnataka';
              accObj.ShippingCountry='India';
              accObj.ShippingPostalCode='276737';
              accObj.Guardian_s_Name__c='david';
        accObj.Profile_Verified__c=true;
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        insert cont;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        
        Photo__c ph= new Photo__c ();   
          ph.Tenant__c=accObj.id;
        ph.Is_Current_Document__c=true;
        insert ph;
       
        
        Bank_Detail__c	bdetail = new Bank_Detail__c();
        bdetail.Related_Account__c=accObj.Id;
        bdetail.Name='test';
        bdetail.Account_Number__c='837368283662896';
        bdetail.Bank_Name__c ='Axis';
        insert bdetail;
        
        Map<Id,Account> mapAcount = new Map<Id,Account>();
        mapAcount.put(accObj.id,accObj);
        AccountTriggerHandler.checkTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyIfTenantDocumentsarePresent(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantDocuments(mapAcount,mapAcount);
        AccountTriggerHandler.beforeUpdate(mapAcount,mapAcount);
        
    }
    
      public Static TestMethod void contractTest5(){
        Test.StartTest();
       Id Owner_RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.ACCOUNT_RECORD_TYPE_PERSON_ACCOUNT).getRecordTypeId();

      RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account']; 
Account newPersonAccount = new Account();
 // for person accounts we can not update the Name field instead we have to update the FirstName and LastName individually
          newPersonAccount.FirstName = 'Fred'; 
          newPersonAccount.LastName = 'Smith';
          newPersonAccount.Phone='8900000000';
          newPersonAccount.Relation_with_Guardian__c='son';
          newPersonAccount.Birth_Date__c=system.today();
          newPersonAccount.ShippingStreet='lakeview';
              newPersonAccount.ShippingCity='Bangalore';
              newPersonAccount.ShippingState='Karnataka';
              newPersonAccount.ShippingCountry='India';
              newPersonAccount.ShippingPostalCode='276737';
       
              newPersonAccount.Guardian_s_Name__c='david';
          newPersonAccount.RecordType = personAccountRecordType;
          insert newPersonAccount;
          
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = newPersonAccount.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        insert cont;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        
        Photo__c ph= new Photo__c ();   
          ph.Tenant__c=newPersonAccount.id;
        ph.Is_Current_Document__c=true;
        ph.Approval_Status__c='Approved';
        insert ph;
       
        
        Bank_Detail__c	bdetail = new Bank_Detail__c();
        bdetail.Related_Account__c=newPersonAccount.Id;
        bdetail.Name='test';
        bdetail.Account_Number__c='837368283662896';
        bdetail.Bank_Name__c ='Axis';
        insert bdetail;
        List<Account> accList= new List<Account>();
          accList.add(newPersonAccount);
        Map<Id,Account> mapAcount = new Map<Id,Account>();
        mapAcount.put(newPersonAccount.id,newPersonAccount);
          AccountTriggerHandler.assignToRecordType(accList);
        AccountTriggerHandler.checkTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantProfile(mapAcount,mapAcount);
        AccountTriggerHandler.verifyIfTenantDocumentsarePresent(mapAcount,mapAcount);
        AccountTriggerHandler.verifyTenantDocuments(mapAcount,mapAcount);
        AccountTriggerHandler.beforeUpdate(mapAcount,mapAcount);
        
    }
    
    
    
}