/*  Created By   : Mohan - WarpDrive Tech Works
	Created Date : 21/05/2017
	Purpose      : All the queries related to the Product2 Object are here */

public with sharing class Product2Selector {

	//query for all the Products with Package Selection and Quantity for 1BHK, 2BHK so on ..
	public static List<Product2> getAllProductsWithPackageSelectionInfo(){
		return [select Id, Name, Package_Selection__c, X1R__c, X1BK__c, X1BH__c, X1BHK__c, X2BHK__c, X3BHK__c, X4BHK__c, X5BHK__c,
		 		(select Id, UnitPrice from PriceBookEntries Where Pricebook2.Name = 'NestAway Standard Price Book') 
		 		from Product2 where Package_Selection__c != null];        
	}  

}