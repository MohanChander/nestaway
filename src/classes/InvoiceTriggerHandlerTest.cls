@isTest
public class InvoiceTriggerHandlerTest {

    @testSetup static void createdata(){
        Id AccRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test Acc';
        acc.Active__c = true;
        acc.recordtypeid = accrecId;
        insert acc;
        
        
        
        House__c hs = new House__c();
        hs.name = 'Test House';
        insert hs;
        
        Invoice__c inv = new Invoice__c();
        inv.Label__c = '';
        inv.Amount__c=124;
        inv.Invoice_Generation_Date__c = Date.today();
        inv.Vendor__c = acc.Id;
        inv.Recoverable__c = 'Yes';
        inv.Billed_To__c = 'NestAway';
        inv.Category__c = 'On-boarding';
        inv.House__c = hs.Id;
        insert inv;
    }
    
    @isTest
    static void testInvoice(){
        Invoice__c Inv2 = [select Id,Description__c from Invoice__c limit 1];
        inv2.Description__c = 'test';
        update Inv2;
    }
}