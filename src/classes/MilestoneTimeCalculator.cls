/*  Created By: Mohan
    Purpose   : Calculate the TargetDate of the Milestone */

global class MilestoneTimeCalculator implements Support.MilestoneTriggerTimeCalculator { 

     global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId){
        Case c = [SELECT Priority, Escalation_Level__c, Problem__r.SLA_1__c, Problem__r.SLA_2__c,
                  Problem__r.SLA_3__c, Problem__r.SLA_4__c, SLA_1__c FROM Case WHERE Id=:caseId];
        MilestoneType mt = [SELECT Name FROM MilestoneType WHERE Id=:milestoneTypeId];

        if(c.Escalation_Level__c == null){
            return (Integer)c.SLA_1__c;
        } else if(c.Escalation_Level__c == 'Level 1'){
            return c.Problem__r.SLA_2__c != null ? (Integer)c.Problem__r.SLA_2__c * 1440 : Integer.valueOf(Label.Default_Level_2_SLA);
        } else if(c.Escalation_Level__c == 'Level 2'){
            return c.Problem__r.SLA_3__c != null ? (Integer)c.Problem__r.SLA_3__c * 1440 : Integer.valueOf(Label.Default_Level_3_SLA);
        } else {
            return c.Problem__r.SLA_4__c != null ? (Integer)c.Problem__r.SLA_4__c * 1440 : Integer.valueOf(Label.Default_Level_4_SLA);
        }           
    }
}