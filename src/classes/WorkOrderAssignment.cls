/**********************************
 Created By : Mohan
 Purpose    : Assign the WO to a member inside a Queue 
**********************************/
public class WorkOrderAssignment {

    public static void WorkOrderAssignment(List<WorkOrder> woList) {
        try{
                List<WorkOrder> updatedWorkOrderList = new List<WorkOrder>();
                Map<Id, Map<Id, Integer>> queueLoadMap = getWOLoadForAllQueues();

                System.debug('***************queueLoadMap: ' + queueLoadMap + '\n Size of queueLoadMap: ' + queueLoadMap.size());

                for(WorkOrder wo: woList){
                    //if there is no member in the Queue - Allow the Case to remain in the Queue
                    if(!queueLoadMap.isEmpty() && queueLoadMap.containsKey(wo.OwnerId) && !queueLoadMap.get(wo.OwnerId).isEmpty()){
                        List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                        for(Id userId: queueLoadMap.get(wo.OwnerId).keySet()){
                            awList.add(new AssignmentWrapper(userId, queueLoadMap.get(wo.OwnerId).get(userId)));
                        }
                        awList.sort();
                        wo.OwnerId = awList[0].UserId;              
                    }
                }
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'WorkOrder Assignment');      
            }
        
    }

    public static Map<Id, Map<Id, Integer>> getWOLoadForAllQueues(){

            Map<Id, Integer> openCaseMap = new Map<Id, Integer>();
            Map<Id, Map<Id, Integer>> queueLoadMap = new Map<Id, Map<Id, Integer>>();

            for(AggregateResult ar: [select OwnerId, count(Id) from WorkOrder where IsClosed = false
                                     group by OwnerId]){
                openCaseMap.put((Id)ar.get('OwnerId'), (Integer)ar.get('expr0'));            
            } 

            for(Group grp: [select Id, Name from Group]){
                queueLoadMap.put(grp.Id, new Map<Id, Integer>{});
            }

            for(GroupMember member: [Select UserOrGroupId, Group.Name, GroupId From GroupMember where 
                                     UserOrGroupId in (select Id from User where isActive = true and isAvailableForAssignment__c = true)]){
                if(queueLoadMap.containsKey(member.GroupId)){
                    queueLoadMap.get(member.GroupId).put(member.UserOrGroupId, openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId));
                } else{
                    queueLoadMap.put(member.GroupId, new Map<Id, Integer>{member.UserOrGroupId => openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId)});
                }  
            }   

            return queueLoadMap;               
    } 

    /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfCases {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfCases){
            this.userId = userId;
            this.noOfCases = noOfCases;
        }

        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfCases == compareToObj.noOfCases) return 0;
            if (noOfCases > compareToObj.noOfCases) return 1;
            return -1;        
        }       
    }       
}