/*Added By Baibhav
purpose to change status of tanent WorkOrder
*/

public class BedTriggerHelper 
{
    public static void BedTriggerTanentOffboardingUpdate(List<Bed__c> bedList)
    {
        Set<Id> houseSet = new Set<Id>();
        for(Bed__c bd:bedList)
        {
            houseSet.add(bd.house__c);
        } 
         System.debug('bony'+ houseSet);
        List<Bed__c> allBedList =BedSelector.getBedsWithRent(houseSet);
        System.debug('bony'+ allBedList);
        Map<Id,list<Bed__c>> bedMapHouseid = new Map<Id,list<Bed__c>>();
        for(Bed__c bd:allBedList)
        {
            List<Bed__c> beList = new List<Bed__c>(); 
            if(bedMapHouseid.containsKey(bd.house__c))
            {
                beList=bedMapHouseid.get(bd.house__c);
            }
            beList.add(bd);
            bedMapHouseid.put(bd.house__c,beList);
        }

        List<WorkOrder> allTanentWrkoList = WorkOrderSelector.getTanentOffBoardingWorkOrders(houseSet);
        System.debug('bony'+ allTanentWrkoList);
        Map<Id,List<WorkOrder>> tanentwrkoMapHouseid = new Map<Id,List<WorkOrder>>();
        for(WorkOrder wrko:allTanentWrkoList)
        {
            List<WorkOrder> wrkoList = new List<WorkOrder>(); 
            if(tanentwrkoMapHouseid.containsKey(wrko.house__c))
            {
                wrkoList=tanentwrkoMapHouseid.get(wrko.house__c);
            }
            wrkoList.add(wrko);
            tanentwrkoMapHouseid.put(wrko.house__c,wrkoList);
        }
        System.debug('Map'+ tanentwrkoMapHouseid);

         List<WorkOrder> tanentWrkoListUpdate = new List<WorkOrder>(); 
         map<id,WorkOrder> tanentWrkoListUpdateMap = new map <id,WorkOrder> ();

         for(Bed__c bd:bedList)
         {
            Boolean bedsoldout = false;
            List<Bed__c> beList= bedMapHouseid.get(bd.house__c);
            if(beList!=null)
            {
                for(Bed__c b:beList)
                {
                    if(b.Status__c == Constants.BED_STATUS_SOLD_OUT)
                    {
                     bedsoldout=true;
                     break;
                    }
                }
            }
            if(bedsoldout==false)
            {
                List<WorkOrder> tanentwrkolist = tanentwrkoMapHouseid.get(bd.house__c);
                System.debug('Tan'+ tanentwrkolist);
                if(tanentwrkolist!=null)
                {
                    for(WorkOrder wrk:tanentwrkolist)
                    {
                        wrk.Status=Constants.WORKORDER_STATUS_CLOSED;
                        //tanentWrkoListUpdate.add(wrk);
                        tanentWrkoListUpdateMap.put(wrk.id,wrk);
                    }
                }

            }
         }
         System.debug(tanentWrkoListUpdate);
         if(tanentWrkoListUpdateMap !=null && tanentWrkoListUpdateMap.size() > 0)
          update tanentWrkoListUpdateMap.values();
    }
}