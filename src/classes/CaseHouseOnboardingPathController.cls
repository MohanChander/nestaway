/**    
  *   Class Name     :  CaseHouseOnboardingPathController
  *
  *   Description    :  CaseHouseOnboardingPathController class for showing Onboarding process.   
  *
  *    Created By    :  Chandraprakash Jangid   
  *
  *    Created Date  :  21/05/2017
  *
  *    Version       :  V1.0 Created
  
**/

public class CaseHouseOnboardingPathController {
    private final case   cs;    
    public List<String>         stageList {get;set;}
    public Map<String, Integer> stageMap {get;set;}
    public Integer              currentStatusNum {get;set;}
    public Integer              currentStageNum {get;set;}
    public CaseHouseOnboardingPathController(ApexPages.StandardController stdController){
        this.cs            = (case)stdController.getRecord(); // get the case record
        currentStatusNum    = 0;
        currentStageNum     = 0;
        Integer index       = 0;
        stageMap            = new Map<String, Integer>(); 
        stageList           = new List<String>();
        
        /*******************************************************
        * Get case Status values dynamically from process configuration. 
        ********************************************************/
        String caseRecTypeName='';
        List<Case> currCaseLst=[select id,recordtype.name from case where id=:cs.id];
        if(currCaseLst.size()>0){
            caseRecTypeName=currCaseLst.get(0).recordtype.name;
            
        }
       Id onBoardingRecordTypeId = Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(Constants.PROCESS_CONFIG_RT_HOUSE_ONB).getRecordTypeId();
        List<Operational_Process__c> processConfigurationLst=[select id,Case_Status__c,Order__c,For_RecordType__c from Operational_Process__c where recordtypeid=:onBoardingRecordTypeId order by Order__c];
        /* if(cs.status=='Closed'){
            
            stageList.add('Closed');
        }
        else{
        
           /* for(Operational_Process__c prc: processConfigurationLst){
                            
                if(prc.For_RecordType__c!=null && prc.Case_Status__c!=null){
                    
                    if(prc.For_RecordType__c.contains(caseRecTypeName)){
                        stageList.add(prc.Case_Status__c);
                        stageMap.put(prc.Case_Status__c, index);
                        index++;
                    }
                    
                }
                
            }
            */
            if(caseRecTypeName==Constants.CASE_RT_Furnished_House_Onboarding){
                
                    stageList.add('Key Handling');
                    stageMap.put('Key Handling', index);
                    index++;
                    stageList.add('Maintenance');
                    stageMap.put('Maintenance', index);
                    index++;
                    stageList.add('Furnishing and DTH / Wifi Connection');
                    stageMap.put('Furnishing and DTH / Wifi Connection', index);
                    index++; 
                    stageList.add('Verification');
                    stageMap.put('Verification', index);
                    index++;
                    stageList.add('Photography');
                    stageMap.put('Photography', index);
                    index++;
                    stageList.add('Make House Live');
                    stageMap.put('Make House Live', index);
                    index++;
                    stageList.add('Closed');
                    stageMap.put('Closed', index);
                    index++;   
                    stageList.add('Dropped');
                    stageMap.put('Dropped', index);
                    index++;                                      
            }
            else if(caseRecTypeName==Constants.CASE_RT_Unfurnished_House_Onboarding){

                    stageList.add('Key Handling');
                    stageMap.put('Key Handling', index);
                    index++;
                    stageList.add('Maintenance');
                    stageMap.put('Maintenance', index);
                    index++;
                    stageList.add('Verification');
                    stageMap.put('Verification', index);
                    index++;
                    stageList.add('Photography');
                    stageMap.put('Photography', index);
                    index++;
                    stageList.add('Make House Live');
                    stageMap.put('Make House Live', index);
                    index++;
                    stageList.add('Closed');
                    stageMap.put('Closed', index);
                    index++; 
                    stageList.add('Dropped');
                    stageMap.put('Dropped', index);
                    index++;                                         
            }
            
            
           // currentStatusNum = stageMap.get(opp.StageName);
            if(stageMap.containskey(cs.status)){
               currentStageNum  = stageMap.get(cs.status);
            }       
        //}
    }
}