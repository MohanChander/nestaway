public with sharing class GetInvoiceDataController {

private final Case cs;
    public String idofcase{get;set;}
public WBInvoicesJsonWrapperClass.TenantInvoices allInvoices {get;set;}

public GetInvoiceDataController(ApexPages.StandardController controller) {
    
   try{

         cs=(Case)controller.getRecord(); 
         System.debug('***cs '+cs);  
         case c = new case();
         if(cs!=null) {
             c  = [Select id,parentId  from case where id = : cs.id];
         }
               
         if(c != null && c.parentId != null ){
             idofcase=c.id;
             WBInvoicesJsonWrapperClass.SendCaseId sendDataToWebapp = new WBInvoicesJsonWrapperClass.SendCaseId();
             sendDataToWebapp.case_id = c.parentId;
             
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
            String authToken=nestURL[0].Webapp_Auth__c;
            String strEndPoint;         
            strEndPoint= nestURL[0].Move_Out_Settlement_API__c+'&auth='+authToken;
            System.debug('****strEndPoint - > '+strEndPoint);
            string jsonBody='';
            jsonBody=JSON.serialize(sendDataToWebapp);
            system.debug('****jsonBody'+jsonBody);
            HttpResponse res;
            res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
            String respBody = res.getBody();
            system.debug('****respBody'+respBody);
            // tem code
            
            /* 
            
            respBody='{'+
            '   "sf_id": "jajajaj",'+
            '   "invoices": [{'+
            '       "invoice_id": "jaja77a7a77a",'+
            '       "old_transaction_id": "",'+
            '       "transaction_status": "success",'+
            '       "paid_on": "09 / 02 / 2017",'+
            '       "amount": "10000.00",'+
            '       "mode_of_payment": "",'+
            '       "message": "",'+
            '       "rps": [{'+
            '           "status": "paid",'+
            '           "reason": "owere",'+
            '           "amount": "101010",'+
            '           "type": "RECEIVABLE",'+
            '           "house_id": "9191",'+
            '           "due_date": "09 / 02 / 2017",'+
            '           "notes": ""'+
            '       }]'+
            '   }]'+
            '}';
            */
         //   WBInvoicesJsonWrapperClass.TenantInvoices allInvoices;
            
            allInvoices = (WBInvoicesJsonWrapperClass.TenantInvoices) JSON.deserialize(respBody, WBInvoicesJsonWrapperClass.TenantInvoices.class);
            system.debug('****allInvoices'+allInvoices);
            if(allInvoices!=null){
                     
              // system.debug('****allInvoices'+allInvoices);
            }           
             
         }
         else{
             
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Case details not found.'));
         }
    }
    catch(exception e){
        system.debug('****Exception in page:-'+e.getMessage()+' at line number:- '+e.getLineNumber());
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'An internal exception has occurred. please contact administrator.'));
    }

}
    public pageReference backTocase(){
         pageReference ref = new PageReference('/'+idofcase); 
        ref.setRedirect(true); 
        return ref;
    }

}