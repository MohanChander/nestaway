public class ProblemSelector {
     //Query for all the Problem
    public static List<Problem__c > getProblemList(){
    return [select Id,Queue__c, Scheduled_Visit_Required__c, Priority__c, Name
            from Problem__c];
  }
}