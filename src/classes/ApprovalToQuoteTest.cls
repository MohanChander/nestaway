@isTest
public Class ApprovalToQuoteTest{

    public Static TestMethod void quoteTest(){
        Test.StartTest();
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            insert opp;
            
            Quote quoObj = new Quote();
            quoObj.Name = 'Quote1';
            quoObj.Status = 'Draft';
            quoObj.OpportunityId = opp.Id;
            quoObj.approval_status__c = 'Awaiting Items Manager Approval';
            insert quoObj;
            system.debug('----quoObj---'+quoObj);
           String quoteNumber = [SELECT iD,QuoteNumber from Quote Where id =: quoObj.Id].QuoteNumber;
            system.debug('----quoteNumber ---'+quoteNumber );
            ApprovalToQuote objconfirm = new ApprovalToQuote();
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
           // objconfirm.handleInboundEmail(email, envelope);
            
            email.subject = 'Quote '+quoteNumber ;
            email.plainTextBody = 'Yes, \n Approved';
            envelope.fromAddress = 'poornapriya.yn@kvpcorp.com';
            email.fromName = 'Poornapriya YN';
            objconfirm.handleInboundEmail(email, envelope);
            
            Messaging.InboundEmailResult result = objconfirm.handleInboundEmail(email, envelope);
            System.assertEquals( result.success  ,true);
            
        Test.StopTest();
    }
    public Static TestMethod void contractTest(){
        Test.StartTest();
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            insert accObj;
        
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            insert opp;
            
            PriceBook2 priceBk = new PriceBook2();
            priceBk.Name = 'TestPriceBook';
           // priceBk.isStandard = FALSE;
            insert priceBk;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            cont.PriceBook2Id = priceBk.Id;
            cont.status = 'Draft';
            insert cont;
            
            
           String contractNumber = [SELECT iD,contractNumber from Contract Where id =: cont.Id].contractNumber;
           
            ApprovalToQuote objconfirm = new ApprovalToQuote();
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
           // objconfirm.handleInboundEmail(email, envelope);
            
            email.subject = 'Contract '+contractNumber;
            email.plainTextBody = 'Yes, \n Approved';
            envelope.fromAddress = 'poornapriya.yn@kvpcorp.com';
            email.fromName = 'Poornapriya YN';
            objconfirm.handleInboundEmail(email, envelope);
            
            Messaging.InboundEmailResult result = objconfirm.handleInboundEmail(email, envelope);
            System.assertEquals( result.success  ,true);
            
        Test.StopTest();
    }
}