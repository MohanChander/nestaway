@isTest
public Class HouseTriggerHandlerTest{
  public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
    public static id acquisitionRecTypeId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RT_ACQUISITION).getRecordTypeId();
    public static Id dccRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('Document Collection Checklist').getRecordTypeId();
    public static Id furnishedOnboardingHouseRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();
    public static Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HO_ZONE ).getRecordTypeId();
    public static Id opRecordTypeId = Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get('House Onboarding').getRecordTypeId();
    public static Id zuRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();

    public Static TestMethod House__c houseTest(){
          
        List<House_Inspection_Checklist__c> hicList = new List<House_Inspection_Checklist__c>();
        List<Zone_and_OM_Mapping__c> zuList = new List<Zone_and_OM_Mapping__c>();
     
            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
          User newUser = Test_library.createStandardUser(1);
        insert newuser;

        zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.ZOM__c = newuser.Id;
        zc.ZOM_as_Onboarding_Manager__c = true;
        zc.Name='Test';
        zc.RecordTypeId = ZoneRecordTypeId;
        insert zc;

        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.FE__c = true;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zuRecordTypeId;
        zuList.add(zu1);

        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Zone__c = zc.Id;
        zu2.FE__c = true;
        zu2.RecordTypeId = zuRecordTypeId;
        zuList.add(zu2); 

        insert zuList;     

           Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
       
        
         Opportunity opp = new Opportunity();
         opp.Name ='Test Opp 2'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            opp.House_Layout__c = '2 BHK';
            insert opp;

        House_Inspection_Checklist__c hic1 = [select Id, Number_of_common_bathrooms__c, Number_of_attached_bathrooms__c
                                             from House_Inspection_Checklist__c where Opportunity__c =: opp.Id];
        hic1.Number_of_common_bathrooms__c = 1;
        hic1.Number_of_attached_bathrooms__c = 1;
        hicList.add(hic1);

        List<Room_Inspection__c> ricList = [select Id, Has_an_attached_bathroom__c, Area_of_the_room_in_sqft__c
                                            from Room_Inspection__c where House_Inspection_Checklist__c =: hic1.Id];
        Room_Inspection__c newRic = ricList[0];
        newRic.Has_an_attached_bathroom__c = 'Yes';
        update newRic;            

        //insert HIC
        House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.RecordTypeId = dccRecordTypeId;
        hic.Opportunity__c = opp.id;
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c = 'Document Collection Checklist';
        hic.House_layout__c = '2 BHK';
        hic.Doc_Reviewer_Approved__c = true;
        hic.Address_Proof_Document__c = 'Aadhar Card';
        hic.Address_Proof_ID__c = '123';
        hic.Verified_Electric_Bill_For_No_Dues__c = 'Yes';
        hic.PAN_Card_Available__c = 'Yes';
        hic.PAN_Card_Number__c = 'BDFPR0591E';
        hic.Kitchen_Package__c='Completely Present';
        hic.Approval_Status__c = 'Approved by Docs Reviewer';
        hic.Verified_Electric_Bill_For_No_Dues__c = 'Yes';
        hic.Electric_Bill_Account_Number__c = '123';
        hic.Status__c = 'Completed';
        hicList.add(hic);

        upsert hicList;           

        //insert Contract
        contract c= new Contract();
        c.accountid=accobj.id;
        c.Opportunity__c=opp.id;
        c.Furnishing_Type__c = Constants.CONTRACT_FURNISHING_CONDITION_FURNISHED;
        insert c;

        List<Room_Terms__c> roomList = [select Id, Number_of_beds__c from Room_Terms__c where Contract__c =: c.Id];

        for(Room_Terms__c room: roomList){
            room.Number_of_beds__c = String.valueOf(2);
        }

        update roomList;

        Set<String> strSet= new Set<String>();
        strSet.add(zc.Zone_code__c);

        City__c ci = new City__c();
        ci.name='bangalore';
        insert ci;
         Test.startTest();
        House__c hos= new House__c();
        hos.name='house1';
        hos.Contract__c=c.id;
        hos.City__c='bangalore';
        hos.House_Owner__c=accObj.id;
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '123';
        hos.Acquisition_Zone_Code__c='364';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c =	Constants.HOUSE_STAGE_HOUSE_DRAFT;
       	hos.Opportunity__c = opp.id;
        hos.Initiate_Offboarding__c='Yes';
        hos.OwnerId=newuser.id;
        insert hos;

        hic.House__c = hos.Id;      
        update hic;  

         Case c1=  new Case();
         c1.House__c = hos.Id;
        c1.RecordTypeId = furnishedOnboardingHouseRtId;
        c1.Active_Tenants__c=3;   
        c1.Status='Open';
        insert c1;

        List<House__c> houseList= new   List<House__c>();
        houseList.add(hos);
        Set<Id> sethouse= new Set<id>();
        setHouse.add(hos.id);
        Map<id,House__c> housemap= new Map<id,House__c>();
        houseMap.put(hos.id,hos);
        test.stopTest();
        return hos;
        
    }


    @isTest
    public static void sendWebEntityHouseJsonTest(){
    
        House__c hos = houseTest();

        List<Case> onboardingCaseList = [select Id, Status from Case where House__c =: hos.Id];
        for(Case c: onboardingCaseList){
            c.Status = 'Closed';
        }
        update onboardingCaseList;
   
        HouseJsonOptimizer.HOUSE_WEB_ENTITY_HOUSE_FLAG = true;
        hos.Stage__c = 'House Live';
        update hos;
     
    }
    
}