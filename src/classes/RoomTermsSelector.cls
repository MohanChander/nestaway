/*  Created By   : Mohan - WarpDrive Tech Works
    Created Date : 06/02/2017
    Purpose      : All the queries related to the Room Terms Object are here */
public class RoomTermsSelector {

	//get all the related to the given Opportunities 
	public static List<Room_Terms__c> getRoomTermsForOpportunities(Set<Id> oppIdSet){

		return [select Id, Contract__r.Opportunity__c, House__c, Room_Inspection__c 
				from Room_Terms__c where Contract__r.Opportunity__c =: oppIdSet];
	}

	//get all Room Terms related to the House
	public static List<Room_Terms__c> getRoomTermsFromHouseId(Id houseId){

		return [Select id,name, Actual_Room_Rent__c,Monthly_Base_Rent_Per_Bed__c,
                            Monthly_Base_Rent_Per_Room__c,City_Master__c,
                            Number_of_beds__c,Status__c,City_Master__r.Rent_Min_Limit__c,
                            (Select id,name,Actual_Rent__c,Initial_Rent__c,Status__c from Beds1__r ) 
                            from room_terms__c where house__c =:  houseId];
	}


	//get Room Term List with House Status
	public static List<Room_Terms__c> getRoomTermsWithHouseStatus(Set<Id> roomTermIdSet){

		return [select Id, House__c, House__r.Stage__c from Room_Terms__c where Id =: roomTermIdSet];
	}

	//get rent for the Room from Houses
	public static List<Room_Terms__c> getRoomTermWithRentDetails(Set<Id> houseIdSet){
		return [select Id, Name, House__c, Actual_Room_Rent__c from Room_Terms__c where House__c =: houseIdSet];
	}

}