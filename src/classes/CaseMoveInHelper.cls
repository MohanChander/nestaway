/*  Created By: Mohan - WarpDrive Tech Works
    Purpose   : Helper for Move in functionality - Case Object */

public class CaseMoveInHelper {

    //  Created By: Mohan 
     //   Purpose   : Create WorkOrder for the Move in Case Whose Status is Move in Scheduled
    public static void createMoveInOrMoveOutCheckWorkOrder(List<Case> caseList){

        System.debug('*********************createMoveInOrMoveOutCheckWorkOrder');

        try{
            	Set<id> setCase= new Set<Id>();
                Id moveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK).getRecordTypeId();
                Id moveOutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_OUT_CHECK).getRecordTypeId();
            for(Case c:caseList){
                setCase.add(c.id);
            }
                List<WorkOrder> woList = new List<WorkOrder>();
                List<case> caslist=[select id, RecordTypeId, Tenant__c, AccountId, House__c, Tenancy__c, Booked_Object_Type__c,
                                    OwnerId, Owner.isActive, 
                                    tenant__r.PersonEmail,tenant__r.PersonMobilePhone, MoveIn_Slot_Start_Time__c,
                                    MoveOut_Executive__c, House_Id__c, Tenant__r.Name, MoveOut_Executive__r.isActive,
                                    MoveIn_Slot_End_Time__c, MoveIn_Executive__c, MoveIn_Executive__r.isActive 
                                    from case where id in:setCase];
                for(Case c: caslist){
                    WorkOrder wo = new WorkOrder();
                    wo.CaseId = c.Id;
                    if(c.RecordTypeId == CaseMIMOTriggerHandler.moveInRecordTypeId){
                        wo.RecordTypeId = moveInWorkOrderRTId;
                        wo.Subject = 'Move in ' + c.House_Id__c + ' ' + c.Tenant__r.Name;
                        if(c.MoveIn_Executive__c != null && c.MoveIn_Executive__r.isActive){
                            wo.OwnerId = c.MoveIn_Executive__c;
                        } else if(c.Owner.isActive){
                            wo.OwnerId = c.OwnerId;
                        } else{
                            wo.OwnerId = UserInfo.getUserId();
                        }
                        wo.StartDate = c.MoveIn_Slot_Start_Time__c;
                        wo.EndDate = c.MoveIn_Slot_End_Time__c;
                    } else if(c.RecordTypeId == CaseMIMOTriggerHandler.moveOutRecordTypeId){
                        wo.RecordTypeId = moveOutWorkOrderRTId;
                        wo.Subject = 'Move Out ' + c.House_Id__c + ' ' + c.Tenant__r.Name;  
                        wo.Description='Move out Check task is created to Check House Room Bathroom and verify all things';
                        if(c.MoveOut_Executive__c != null && c.MoveOut_Executive__r.isActive){
                            wo.OwnerId = c.MoveOut_Executive__c;
                        } else if(c.Owner.isActive){
                            wo.OwnerId = c.OwnerId;
                        } else {
                            wo.OwnerId = UserInfo.getUserId();
                        }
                    }
                    wo.Accountid=c.AccountId;
                    wo.Tenant_Email__c=c.tenant__r.PersonEmail;
                    wo.Tenant_PhoneNo__c=c.tenant__r.PersonMobilePhone;
                    wo.Tenant__c=c.Tenant__c;                    
                    wo.Tenancy__c = c.Tenancy__c;
                    if(c.House__c != null)
                        wo.House__c = c.House__c;
                    else
                        c.addError('The Move in Case is associated with House. Please ensure House is Present on Case to schedule Move in');
                    woList.add(wo);
                }

                if(!woList.isEmpty())
                    insert woList;
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'MIMO Functionality');                
            }
    }
}