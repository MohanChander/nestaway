/*******************************************
 * Created By: Mohan
 * Purpose   : TriggerHandler for Received Product
 * *****************************************/
public class ReceivedProductTriggerHandler {
	
    public static void afterInsert(Map<Id, Received_Product__c> newMap){
        try{
                List<Received_Product__c> recProdList = new List<Received_Product__c>();
                for(Received_Product__c recProd: newMap.values()){
                    recProdList.add(recProd);
                }
                
                if(!recProdList.isEmpty()){
                    ReceivedProductTriggerHelper.updateQuantityOnOrderProduct(recProdList);
                }
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Received product afterInsert');      

            }   
    }
    
    public static void afterUpdate(Map<Id, Received_Product__c> newMap, Map<Id, Received_Product__c> oldMap){
        
        try{
                List<Received_Product__c> recProdList = new List<Received_Product__c>();
                for(Received_Product__c recProd: newMap.values()){
                    if(recProd.Received_Quantity__c != oldMap.get(recProd.Id).Received_Quantity__c){
                        recProdList.add(recProd);
                    }         
                }
                
                if(!recProdList.isEmpty()){
                    ReceivedProductTriggerHelper.updateQuantityOnOrderProduct(recProdList);
                }   
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Received Product afterUpdate');      

            }        
    }
}