/*
* Description: update opportunity records based on few conditions.
*/


Public Class QuoteTriggerHandler{
    
    Public Static Void BeforeUpdate(Map<Id,Quote> newMap, Map<Id,Quote> oldMap){
        for(Quote each : newMap.Values()){
            if(each.Rounded_Total__c!=null){
                 Long n = each.Rounded_Total__c.longValue(); 
                 each.In_Words__c = NumberToWord.english_number(n) + ' Only';
            }
        }
    }
   
   
    Public Static Void AfterInsert(list<Quote> lstNewQuote){
        
        map<Id,Id> mapQuote_OppId = new map<Id,Id>();
        
        for(Quote each : lstNewQuote){
           mapQuote_OppId.put(each.Id,each.OpportunityId);
        }
      
        
        if(!mapQuote_OppId.isEmpty()){
             // calling sync call method 
             OpportunityTriggerHandler.SyncQuote(mapQuote_OppId);      
        }
        
      
    } 
    
    public static void AfterUpdate(Map<id,Quote> NewMap,Map<id,Quote> OldMap){
        Set<id> oppoIdSet = new Set<id>();
        set<Id> setOppId  = new set<Id>();
        list<Opportunity> lstOppUpdate = new list<Opportunity>();
        for(Quote each : NewMap.values()){
            system.debug('--each.Status--'+each.Status);
            system.debug('--oldMap.get(each.Id).Status--'+oldMap.get(each.Id).Status);
            if(each.approval_status__c != oldMap.get(each.Id).approval_status__c &&  (each.approval_status__c == 'Approved by Owner' || each.approval_status__c == 'Manually Approved by ZM') ){
                oppoIdSet.add(each.OpportunityId);    
            }
            if(each.Status != oldMap.get(each.Id).Status && each.Status == 'Cancelled'){
                Opportunity opp = new Opportunity(Id=each.OpportunityId,stageName='Lost',Lost_Reason__c='Quote Cancelled');
                lstOppUpdate.add(opp);
            }else if(each.Status != oldMap.get(each.Id).Status && each.Status == 'To be Revised'){
                setOppId.add(each.OpportunityId);    
            }     
        }
        
        if(setOppId.size()>0){
            RevisedContract_SalesOrder(setOppId);
        }
       
        List<Opportunity> oppList = [Select id,StageName from Opportunity where Id IN : oppoIdSet];
        for(Opportunity opp : oppList){
            opp.StageName = 'Sales Order';
        }
        system.debug('--oppList---'+oppList);
        update oppList;   
        
        if(lstOppUpdate.size()>0){
            try{
                update lstOppUpdate;
            }catch(Exception ex){
                system.debug('==exception while updating opp in Quote TrigggerHandler=='+ex);
            }
        }
    }
    
     Public Static Void RevisedContract_SalesOrder(set<Id> setOppId){
          
          list<Contract> lstContractToUpdate    = new list<Contract>();
          list<Order> lstOrderToUpdate          = new list<Order>();  
          
          lstContractToUpdate=[SELECT id,Status,Opportunity__c,Revised__c FROM Contract WHERE Opportunity__c IN : setOppId];
          lstOrderToUpdate=[SELECT id,Status,OpportunityId FROM Order WHERE OpportunityId IN : setOppId];
          
           if(lstContractToUpdate.size()>0){
              for(Contract each : lstContractToUpdate){
                  each.Status='To be Revised';
                  each.Revised__c= TRUE;
              }
              try{
                  update lstContractToUpdate;     
              }catch(exception ex){
                  System.debug('==exception while Updating Contract in QuoteTriggerHandler=='+ex);
              }
          }
          
          if(lstOrderToUpdate.size()>0){
              for(order each : lstOrderToUpdate){
                  each.Status='To be Revised';
              }
              try{
                  update lstOrderToUpdate;     
              }catch(exception ex){
                  System.debug('==exception while Updating Order in QuoteTriggerHandler=='+ex);
              }
          }
    }

    /*  Created By   : Mohan - WarpDrive Tech Works
        Created Date : 18/05/2017
        Purpose      : When a Package is selected for a Quote the QuoteLineItems related to that package are 
                     : populated automatically 
        Execution    : After Update */
    public static void createQuoteLineItems(Map<Id, Quote> newMap, Map<Id, Quote> oldMap){

      if(!Test.isRunningTest()){
        Set<Id> quoteIdSet = new Set<Id>();
        Map<Id, Quote> quoteMap = new Map<Id, Quote>();
        Map<Id, String> quoteWithHouseLayout = new Map<Id, String>();
        Map<String, List<Product2>> packageMap = new Map<String, List<Product2>>();
        Set<Id> opptyIdSet = new Set<Id>();
        
        for(Quote q: newMap.values()){
          //q.addError('Test Exception');
          if(q.Package_Selection__c != null && oldMap.get(q.Id).Package_Selection__c != q.Package_Selection__c){
            quoteMap.put(q.Id, q);
            opptyIdSet.add(q.OpportunityId);
          }
        }

        //query for Opportunity and Contract Details (fetch House Layout Details)
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, PriceBook2Id, (SELECT Id, House_layout__c
                                                                FROM House_Inspection_Checklist__r where House_Layout__c != null
                                                                LIMIT 1) FROM Opportunity WHERE Id IN : opptyIdSet]);

        //Query for the Pricbook id
        PriceBook2 pbe = Pricebook2Selector.getNestAwayStandardPricebook();

        //associate Quote to the House Layout
        for(Quote qt : quoteMap.values()){
            if (!oppMap.get(qt.OpportunityId).House_Inspection_Checklist__r.isEmpty() ) {
                quoteWithHouseLayout.put(qt.Id, oppMap.get(qt.OpportunityId).House_Inspection_Checklist__r[0].House_layout__c);
            }
        } 

        //query for the Products which are part of Package
        List<Product2> prodsList = Product2Selector.getAllProductsWithPackageSelectionInfo();

        for(Product2 prd: prodsList){
          if(packageMap.containsKey(prd.Package_Selection__c)){
            packageMap.get(prd.Package_Selection__c).add(prd);
          } else{
            packageMap.put(prd.Package_Selection__c, new List<Product2>{prd});
          }
        }

        List<QuoteLineItem> qliList = new List<QuoteLineItem>();

        for(Quote q: quoteMap.values()){

          //formating the field to get the field to fetch the Quantity from the Product2 Object
          String[] houseLayoutSplit = new List<String>();

          houseLayoutSplit = quoteWithHouseLayout.get(q.Id).split(' ');
          String fieldToFetchFrom = 'X';          //field on the Product2 from which we will fetch the Quantity for the QLI
          for(String s: houseLayoutSplit){
              fieldToFetchFrom += s;
          }
          fieldToFetchFrom += '__c';          

          String[] selectedPackages = q.Package_Selection__c.split(';');
          Set<String> selectedPackagesSet = new Set<String>(selectedPackages);

          if(!selectedPackagesSet.isEmpty() && selectedPackagesSet.contains('Complete Package') && selectedPackagesSet.size()>1){
            newMap.get(q.Id).addError('Please select complete Package or Individual Packages but not both');
          }      

          if(!selectedPackagesSet.isEmpty() && selectedPackagesSet.contains('Complete Package')){
            for(List<Product2> pList: packageMap.values()){
              for(Product2 p: pList){

                //check if the LineItem quantity to be added is not 0 & null
                if(p.get(fieldToFetchFrom) != null && p.get(fieldToFetchFrom) != 0){
                  QuoteLineItem qli = new QuoteLineItem(QuoteId = q.id, 
                                                        PriceBookentryId = p.PricebookEntries[0].Id, 
                                                        Quantity = Integer.valueOf(p.get(fieldToFetchFrom)), 
                                                        UnitPrice = p.PricebookEntries[0].UnitPrice);
                  qliList.add(qli);
                }
              }
            }
          } else {
            if(!selectedPackagesSet.isEmpty()){
              for(String pack: selectedPackagesSet){
                if(!packageMap.isEmpty() && packageMap.containsKey(pack) && !packageMap.get(pack).isEmpty()){
                  for(Product2 p: packageMap.get(pack)){

                    //check if the LineItem quantity to be added is not 0 & null
                    if(p.get(fieldToFetchFrom) != null && p.get(fieldToFetchFrom) != 0){                
                      QuoteLineItem qli = new QuoteLineItem(QuoteId = q.id, 
                                                            PriceBookentryId = p.PricebookEntries[0].Id, 
                                                            Quantity = Integer.valueOf(p.get(fieldToFetchFrom)), 
                                                            UnitPrice = p.PricebookEntries[0].UnitPrice);

                      qliList.add(qli);
                    }  
                  }                  
                }
              }
            }
          }     // end of If else block   
 
        }  // end of outer for Loop

        insert qliList;         
      }


    }  // end of the method - createQuoteLineItems 

    /*  Created By   : Mohan - WarpDrive Tech Works
        Created Date : 18/05/2017
        Purpose      : Update the Pricebook on the Quote for generating QuoteLineItems automatically
        Execution    : before Update */    
    public static void updatePricebookOnQuote(Map<Id, Quote> newMap, Map<Id, Quote> oldMap){

      if(!Test.isRunningTest()){

        try {

                //Query for the Pricbook id
                PriceBook2 pbe = Pricebook2Selector.getNestAwayStandardPricebook();      

                for(Quote q: newMap.values()){
                  if(q.Package_Selection__c != null && oldMap.get(q.Id).Package_Selection__c == null && q.Pricebook2Id == null){
                    q.Pricebook2Id = pbe.Id;
                  } else if(q.Pricebook2Id == null && q.Package_Selection__c != oldMap.get(q.Id).Package_Selection__c){
                    q.Pricebook2Id = pbe.Id;
                  }  
                }         

            } catch (Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);          
            }  

      } 

    } // end of the method - updatePricebookOnQuote    
}