/*  Created By : Mohan - WarpDrive Tech Works */
public class RoomTermsTriggerHelper {

    public static void checkRoomTermValidationBeforeUpdate(Map<Id, Room_Terms__c> newMap, Map<Id,Room_Terms__c> oldMap){

        try{
                Set<Id> roomIdSet = new Set<Id>();

                for(Room_Terms__c room: newMap.values()){
                    if(room.Actual_Room_Rent__c != oldMap.get(room.Id).Actual_Room_Rent__c && 
                        room.Booking_Type__c != null && room.Booking_Type__c == Constants.HOUSE_BOOKING_TYPE_SHARED_HOUSE){
                        if(room.Actual_Room_Rent__c == null)
                            room.addError('Actual Room rent cannot be Empty - Room: ' + room.Name);
                        else
                            roomIdSet.add(room.Id);
                    }
                }

                if(!roomIdSet.isEmpty()){

                    Map<Id, City__c> cityMap = new Map<Id, City__c>(CitySelector.getCityDetails());
                    Map<Id, Decimal> roomBedTotalMap = new Map<Id, Decimal>();    //Room and the sum of the Bed Rents
                    Map<Id, Decimal> roomRentMinMap = new Map<Id, Decimal>();     // Room and the min rent limit for the room
                    List<AggregateResult> bedAggregateList = BedSelector.getBedAggregationFromRoomTerms(roomIdSet);

                    for(AggregateResult ar: bedAggregateList){
                        roomBedTotalMap.put((Id)ar.get('Room_Terms__c'), (Decimal)ar.get('expr0'));
                    }

                    //calulate the min rent for each room
                    for(Id roomId: roomBedTotalMap.keySet()){
                        Room_Terms__c room = newMap.get(roomId);
                        Decimal minRentAmount = 0;
                        if(room.City__c != null && cityMap.get(room.City__c).Rent_Min_Limit__c != null)
                            minRentAmount = (cityMap.get(room.City__c).Rent_Min_Limit__c/100) * roomBedTotalMap.get(roomId);
                        else{
                            System.debug('min limit from custom label '+Decimal.valueOf(Label.Rent_Min_Limit) * roomBedTotalMap.get(roomId)/100);
                            minRentAmount = Decimal.valueOf(Label.Rent_Min_Limit) * roomBedTotalMap.get(roomId)/100;
                        }
                            
                         system.debug('minRentAmount mohan cods '+minRentAmount);
                        roomRentMinMap.put(roomId, minRentAmount);
                    }

                    //max rent validation
                    for(Id roomId: roomIdSet){
                        if(!roomBedTotalMap.isEmpty() && roomBedTotalMap.containsKey(roomId) && 
                            newMap.get(roomId).Actual_Room_Rent__c > roomBedTotalMap.get(roomId) && !newMap.get(roomId).Data_Migration__c)
                         {
                          //  newMap.get(roomId).addError('Room Rent cannot be greater than Sum of Bed Rents. Room - ' + newMap.get(roomId).Name + ': Sum of Bed Rent  - ' + roomBedTotalMap.get(roomId));
                        }
                    }

                    //min rent validation
                    for(Id roomId: roomIdSet){
                        if(!roomRentMinMap.isEmpty() && roomRentMinMap.containsKey(roomId) && 
                            newMap.get(roomId).Actual_Room_Rent__c < roomRentMinMap.get(roomId)) {
                         //   newMap.get(roomId).addError('Room Rent cannot be less than the Minimum Amount. Room - ' + newMap.get(roomId).Name + ': Min Room Rent - ' + roomRentMinMap.get(roomId));
                    }
                }

                }           

            } catch(Exception e) {
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }
    }
}