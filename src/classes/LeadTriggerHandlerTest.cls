@isTest
Public Class LeadTriggerHandlerTest{
    
    Public Static TestMethod void doTest(){
      Test.StartTest();
        
            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Phone_URL__c = 'www.test.com';
            insert cusSet;
        
            City__c city = new City__c();
           city.name='Bangalore';
           insert city;
        
            Zing_API_URL__c zing = new Zing_API_URL__c();
            zing.Name          = 'Zing URL';
            zing.Auth_Token__c = 'ftLKMp_z_e3xX9Yiax-q';
            zing.Email__c      = 'unknownadmin@nestaway.com';
            zing.Tag__c        = 'OwnerAcquisition';
            zing.URL__c        = 'http://40.122.207.71/admin/zinc/lat_long?';
            
            insert zing;
            
            
            Lead objLead = new Lead();
            objLead.FirstName                  = 'Test';
            objLead.LastName                   = 'Test';
            objLead.Phone                      = '9066955369';
            objLead.MobilePhone                = '9406695578';      
            objLead.Email                      = 'Test@test.com';
            objLead.Source_Type__c             = 'Assisted';
            objLead.Area_Code__c               = '7';         
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.Company                    = 'NestAway';
            objLead.IsValidPrimaryContact__c           = True;        // Added by Poornapriya
            insert objLead;
            
            objLead.Status                     = 'House Visit Scheduled';
            objLead.House_Visit_Scheduled_Date__c = System.Now().AddDays(2);
            Update objLead;
            
            PhoneValidationCheck.numberValidation(objLead.Id, objLead.MobilePhone, objLead.Email, '+91', false);
            
        Test.StopTest();   
    } 
}