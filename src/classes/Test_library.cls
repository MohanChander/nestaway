@IsTest
public with sharing class Test_library {    
    
    // user constants
    //static String defaultUserProfile = 'US Aimia Administrator';
    static String defaultUserProfile = 'COO';
    
    // create standard users
    public static User createStandardUser ( Integer userNumber) 
    {
        User newUser = createStandardUser( userNumber, defaultUserProfile);
        return newUser;
    }
   
     public static User createStandardUser ( Integer userNumber, String userProfileName) 
    {   
        Profile prof = profileNameToObjectMap.get( userProfileName );
        System.assertNotEquals(null, prof, 'createStandardUser: profile not found: ' + userProfileName);
        List<User> eUsers = [Select Id,email,ProfileId,UserName,Alias,CommunityNickName,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,FirstName,LastName From User where UserName = 'tuser@aimia.com'];
        if(eUsers.size() > 0)
            return eUsers[0];
        
        User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' + String.valueOf(userNumber)
                    , CommunityNickName='cspu'+ String.valueOf(userNumber) 
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'+ userProfileName
                    , LastName = 'Test' + String.valueOf(userNumber) 
                   );
        return newUser;
    }

    public static User createStandardUserWithRole ( Integer userNumber) 
    {
        User newUser = createStandardUserWithRole( userNumber, defaultUserProfile);
        return newUser;
    }
   
     public static User createStandardUserWithRole ( Integer userNumber, String userProfileName) 
    {   
        Profile prof = profileNameToObjectMap.get( userProfileName );
        System.assertNotEquals(null, prof, 'createStandardUser: profile not found: ' + userProfileName);
        List<User> eUsers = [Select Id,email,ProfileId,UserName,Alias,CommunityNickName,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,FirstName,LastName From User where UserName = 'tuser@aimia.com'];
        if(eUsers.size() > 0)
            return eUsers[0];
        UserRole ur = new UserRole(Name = 'CEO');
        insert ur;
        
        User newUser = new User( email='firstlast@aimia.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@aimia.com'
                    , Alias='cspu' + String.valueOf(userNumber)
                    , CommunityNickName='cspu'+ String.valueOf(userNumber) 
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'+ userProfileName
                    , LastName = 'Test' + String.valueOf(userNumber) 
                    
                    ,UserRoleId = ur.Id);
        return newUser;
    }
    
    
    // Profiles
    public static Map<String,Profile> profileNameToObjectMap
    {
        get{
            if (profileNameToObjectMap == null) {
                initializeProfileMap();
            }
            return profileNameToObjectMap;
        }
        private set {
            profileNameToObjectMap = value;
        }
    }
    
    
    private static void initializeProfileMap()
    {
        
        Map<Id,Profile> profileIdToObjectMap = new Map<Id,Profile> ([select Id, Name
            from Profile 
            order by Name]);
        profileNameToObjectMap = new Map<String,Profile>();
        for (Profile pr : profileIdToObjectMap.values()) {
            profileNameToObjectMap.put( pr.Name, pr);
        }
    }
   
}