/***************************************
 * Created By : Mohan
 * Purpose    : WorkOrder Trigger Helper for SnM related functionality
 * ************************************/
public class WorkOrderSnMTriggerHelper {
    
/****************************************
 * Created By : Mohan
 * Purpose    : Validations that will fire before the Vendor can resolve the WO
 *              1) if Material Cost is > 0 then there should be a invoice attached
 * **************************************/ 
    public static void vendorWorkOrderResolutionValidator(List<WorkOrder> woList){
        
        try{
                Set<Id> woIdSet = new Set<Id>();
                Map<Id, Attachment> attMap = new Map<Id, Attachment>();
                for(WorkOrder wo: woList){
                    if(wo.Material_Cost__c != null && wo.Material_Cost__c > 0){
                        woIdSet.add(wo.Id);
                    }
                }
                
                List<Attachment> attList = AttachmentSelector.getAttachementsForParentIdSet(woIdSet);
            
                for(Attachment att: attList){
                    attMap.put(att.ParentId, att);
                }
            
                for(WorkOrder wo: woList){
                    if(wo.Material_Cost__c != null && wo.Material_Cost__c > 0 && !attMap.containsKey(wo.Id)){
                        wo.addError('Please upload the Invoices before you resolve the Work Order');
                    }
                }               
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Vendor Work Order Resolution Validation');      

            }   
    }


/****************************************
 * Created By : Mohan
 * Purpose    : Once the WorkOrder is resolved by the Vendor check certain conditions and update the case
 * **************************************/   
    public static void updateCaseOnWorkOrderResolution(List<WorkOrder> woList){
        try{
                Set<Id> caseIdSet = new Set<Id>();
                Set<Id> resolvedCaseIdSet = new Set<Id>();
                List<Case> updatedCaseList = new List<Case>();
                for(WorkOrder wo: woList){
                    caseIdSet.add(wo.CaseId);
                }

                //Query for Cases in which all the work Orders are resolved
                List<Case> caseList = [select Id from Case where Id not in 
                                      (select CaseId from WorkOrder where CaseId =: caseIdSet and isClosed = false)
                                      and Id =: caseIdSet];

                for(Case c: caseList){
                    resolvedCaseIdSet.add(c.Id);
                }    

                if(!resolvedCaseIdSet.isEmpty()){
                    List<AggregateResult> groupedResult = [select Sum(Material_Cost__c) matCost, Sum(Labour_Cost__c) labCost, CaseId cId 
                                                           from WorkOrder
                                                           where CaseId =: resolvedCaseIdSet group by CaseId];
                    
                    for(AggregateResult ar: groupedResult){
                        Case c = new Case();
                        c.Id = (Id)ar.get('cId');
                        c.Material_Cost__c = (Decimal)ar.get('matCost');
                        c.Labour_Cost__c = (Decimal)ar.get('labCost');
                        c.Status = Constants.CASE_STATUS_RESOLVED;
                        updatedCaseList.add(c);
                    }  

                    if(!updatedCaseList.isEmpty()){
                        update updatedCaseList;
                    }                                                         
                }                                  

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Work Order Resolution');      
            }   
    } 

/***************************************************
    Created By : Mohan
    Purpose    : Send an API call when Work Order is created for Vendor
***************************************************/
    public static void sendVendorDetailsToWebApp(WorkOrder wo, Boolean isInsert){

        System.debug('**********************sendVendorDetailsToWebApp');

        try{                 
                Case c = [select Id, CaseNumber, Preferred_Visit_Time__c from Case where Id =: wo.CaseId];

                /*
                WorkOrder queriedWo = [select Id, Onwer.Name, Owner.Contact.Phone, Case.CaseNumber 
                                       from WorkOrder where Id =: wo.Id];  */

                User technician = [select Id, Name, ContactId from User where 
                                   Id in (select OwnerId from WorkOrder where Id =: wo.Id)];

                Contact technicianContact = [select Phone from Contact where Id =: technician.ContactId];                                       

                List<NestAway_End_Point__c> nestURL = NestAway_End_Point__c.getall().values();       
                String authToken=nestURL[0].Webapp_Auth__c;
                String EndPoint = nestURL[0].Vendor_Details_Notification__c +'&auth='+authToken;
                
                System.debug('***EndPoint  '+EndPoint);
                
                string jsonBody='';
                WBMIMOJSONWrapperClasses.VendorDetailsNotificationJSON vendorDetailsObject = new WBMIMOJSONWrapperClasses.VendorDetailsNotificationJSON();

                /*if Preferred Visit Time and Time Scheduled on the WO send Vendor Notification and if its not the same 
                  rescheduled Notification */                
                if(wo.StartDate == c.Preferred_Visit_Time__c && isInsert){
                    vendorDetailsObject.method = 'send_vendor_details';
                } else {
                    vendorDetailsObject.method = 'reschedule_vendor_visit';                    
                }

                vendorDetailsObject.ticket_number = c.CaseNumber;
                vendorDetailsObject.technician_name = technician.Name;
                vendorDetailsObject.technician_phone = technicianContact.Phone;
                vendorDetailsObject.schedule_date_time = wo.StartDate;

                jsonBody = JSON.serialize(vendorDetailsObject);
                System.debug('***jsonBody  '+jsonBody);
                
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
                
                String respBody = res.getBody();
                system.debug('****respBody'+ respBody);
        }
        catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Vendor Details API');  
        }                        
    } 
        /*****************************************************************************************************************************************
Added by baibahv
Purpose: to create validation Task
****************************************************************************************************************************************
*/

    public static void CreateTaskToServiceCase(List<workOrder> wrkList)
    {
        try
        {  
            Id genTaskRT = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_VERIFICATION_TASK).getRecordTypeId();

            System.debug('******Owner*********');
            Set<Id> casidSet= new Set<id>(); 
           for(workOrder wrk : wrkList)
             {
                casidSet.add(wrk.caseID);
             } 
             Map<id,Case> casMap = CaseSelector.getMapCasefromCaseID(casidSet);
            List<task> tskList =new List<task>(); 
            for(workOrder wrk : wrkList)
            {     System.debug('******Owner2*********');
                Task tsk = new Task();
                tsk.whatid=wrk.Caseid;
                tsk.RecordTypeId=genTaskRT;
                tsk.subject='Varification task for WorkOrder :'+wrk.WorkOrderNumber;
                tsk.status='Open';
                tsk.OwnerID=casMap.get(wrk.caseId).OwnerId;
                tsk.ActivityDate=System.Today().addDays(1);
                tskList.add(tsk);
            }
            if(!tskList.isEmpty())
             { System.debug('******Owner2*********');
              insert tskList;
             }
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);             
        }
    } 

/***************************************************
    Created By : Mohan
    Purpose    : Send an API call when Work Order is created for Vendor
***************************************************/
    public static void sendVendorDetailsToWebApp(WorkOrder wo){

        System.debug('**********************sendVendorDetailsToWebApp');

        try{                 
                Case c = [select Id, CaseNumber from Case where Id =: wo.CaseId];

                /*
                WorkOrder queriedWo = [select Id, Onwer.Name, Owner.Contact.Phone, Case.CaseNumber 
                                       from WorkOrder where Id =: wo.Id];  */

                User technician = [select Id, Name, ContactId from User where 
                                   Id in (select OwnerId from WorkOrder where Id =: wo.Id)];

                Contact technicianContact = [select Phone from Contact where Id =: technician.ContactId];                                       

                List<NestAway_End_Point__c> nestURL = NestAway_End_Point__c.getall().values();       
                String authToken=nestURL[0].Webapp_Auth__c;
                String EndPoint = nestURL[0].Vendor_Details_Notification__c +'&auth='+authToken;
                
                System.debug('***EndPoint  '+EndPoint);
                
                string jsonBody='';
                WBMIMOJSONWrapperClasses.VendorDetailsNotificationJSON vendorDetailsObject = new WBMIMOJSONWrapperClasses.VendorDetailsNotificationJSON();
                vendorDetailsObject.ticket_number = c.CaseNumber;
                vendorDetailsObject.technician_name = technician.Name;
                vendorDetailsObject.technician_phone = technicianContact.Phone;
                vendorDetailsObject.schedule_date_time = wo.StartDate;

                jsonBody = JSON.serialize(vendorDetailsObject);
                System.debug('***jsonBody  '+jsonBody);
                
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
                
                String respBody = res.getBody();
                system.debug('****respBody'+ respBody);
        }
        catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Vendor Details API');  
        }                        
    }               
}