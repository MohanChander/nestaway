/*
* Description: It's class, here we wrote functionality to get lacation geo location based lead address.
*/

public class NA_TerittoryMappingSite {
    public static List<wrapper> wrpList;
    public NA_TerittoryMappingSite(){
        wrpList = new List<Wrapper>();
    }
        @future(CallOut=True)
        public Static Void handleMappingSite(set<Id> setLeadId){ 
                 
            Id LeadUnCLassifiedRecId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Unclassified Lead').getRecordTypeId();
            System.debug('==LeadUnCLassifiedRecId=='+LeadUnCLassifiedRecId);
            
            Set<Id> setLeadRecordId = new set<Id>();
            setLeadRecordId.add(LeadUnCLassifiedRecId);
            
            List<Lead> lstNewLead = new List<Lead>();
            //lstNewLead = [SELECT id,Name,Email,OwnerId,Latitude,longitude,Area_Code__c,Owner.UserRoleId,Owner.userRole.Name,NonServiceable_Locality__c,NonServiceable_City__c,Street,City,State,Country,PostalCode FROM Lead WHERE Id IN: setLeadId AND RecordtypeId !=: LeadUnCLassifiedRecId];
            lstNewLead = [SELECT id,Name,Email,OwnerId,Latitude,longitude,Area_Code__c,Owner.UserRoleId,Owner.userRole.Name,NonServiceable_Locality__c,NonServiceable_City__c,Street,City,State,Country,PostalCode FROM Lead WHERE Id IN: setLeadId AND RecordtypeId NOT IN : setLeadRecordId];
            system.debug('==lstNewLead=='+lstNewLead);
            Map<String, Zing_API_URL__c> allParam = Zing_API_URL__c.getAll();
            System.debug('==allParam=='+allParam);
            Zing_API_URL__c zingAPI = allParam.values();
            String strURL;
            String strAuth;
            String strTag;
            String strEmail;
            String strAreaId;
            String strTimeStamp;
            strTimeStamp=System.Now().year()+'-'+System.Now().month()+'-'+System.Now().day()+' '+System.Now().hour()+':'+System.Now().minute()+':'+System.Now().second();
            system.debug('==strTimeStamp=='+strTimeStamp);
            
            
            if(zingAPI!=Null){
                strURL   = zingAPI.URL__c;
                strAuth  = zingAPI.Auth_Token__c;
                strTag   = zingAPI.Tag__c;
                strEmail = zingAPI.Email__c;      
            }
            system.debug('==strURL=='+strURL);
            system.debug('==strAuth=='+strAuth);
            system.debug('==strTag=='+strTag);
            system.debug('==strEmail=='+strEmail);
                
            
            for(Lead each : lstNewLead){ 
                  
              string address='';
  
              if(each.Street!=null){
                address += each.Street+',';
              }
              if(each.City !=null){
                address += each.City+',';
              }
              if(each.State!=null){
                address +=each.State+',';
              }
              if(each.Country!=null){
                address += each.Country+',';
              }
              if(each.PostalCode!=null){
                address += each.PostalCode+',';
              }
              /* if address is not blank then get the latitude and longitude through google api */
              if(address !=''){
                HttpRequest req = new HttpRequest();
                /* Encode address */
                address = EncodingUtil.urlEncode(address,'UTF-8');
                req.setEndPoint('https://maps.googleapis.com/maps/api/geocode/xml?address='+address+'&sensor=true');
                req.setMethod('GET');
                Http http = new Http();
                HttpResponse res;
                if(!Test.isRunningTest()){
                  res = http.send(req);
                }else{
                  /* create sample data for test method */
                  String resString = '<GeocodeResponse><status>OK</status><result><geometry><location><lat>37.4217550</lat> <lng>-122.0846330</lng></location>';
                  resString +='</geometry> </result> </GeocodeResponse>';
                  res = new HttpResponse();
                  res.setBody(resString);
                  res.setStatusCode(200);
                  
                }
                
                Dom.Document doc = res.getBodyDocument();   
                /* Get the root of xml response */
                Dom.XMLNode geocodeResponse = doc.getRootElement();
                if(geocodeResponse!=null){
                  /* Get the result tag of xml response */
                  Dom.XMLNode result = geocodeResponse.getChildElement('result',null);
                  if(result!=null){
                    /* Get the geometry tag  of xml response */
                    Dom.XMLNode geometry = result.getChildElement('geometry',null);
                    if(geometry!=null){
                      /* Get the location tag  of xml response */
                      Dom.XMLNode location = geometry.getChildElement('location',null);
                      if(location!=null){
                        /* Get the lat and lng tag  of xml response */
                        String lat = location.getChildElement('lat', null).getText();
                        String lng = location.getChildElement('lng', null).getText();
                        try{
                          if(Decimal.valueof(lat) != null)  
                              each.Latitude =Decimal.valueof(lat);
                          if(Decimal.valueof(lng) != null)
                              each.Longitude =Decimal.valueof(lng);
                          
                        }catch(Exception ex){
                          system.debug('Exception '+ex.getMessage());
                        }
                      }
                    }
                  }
                }
              }
               if(each.Latitude!= null && each.Longitude!=null){
                    String strLat  = String.ValueOf(each.Latitude);
                    String strLong = String.ValueOf(each.Longitude); 
                    Http pro = new Http();
                    HttpRequest Req = new HttpRequest();
                    HttpResponse Res = new HttpResponse();
                    String endPoint = strURL+'auth_token='+strAuth +'&tag=' +strTag ;
                    strTimeStamp = EncodingUtil.urlEncode(strTimeStamp, 'UTF-8');
                    endPoint = endpoint+'&lat='+ strLat +'&long='+ strLong +'&timestamp='+ strTimeStamp +'&email='+strEmail+'&password='+'admin123'; 
                    
                    System.debug('==endPoint=='+endPoint);
                    Req.setEndpoint(endPoint);
                    Req.setMethod('GET');
                    if(!Test.isRunningTest()){
                        Res = pro.send(req);
                    }
                    else{
                        /* create sample data for test method */
                          String resString = strURL+'auth_token='+strAuth +'&tag=' +strTag;
                          resString += '&lat=37.4217550&long=-122.0846330&timestamp='+ strTimeStamp +'&email='+strEmail+'&password='+'admin123' ;
                          system.debug('---resString---'+resString);
                          res = new HttpResponse();
                          res.setBody(resString);
                       //   res.setStatusCode(200);
                    }
                    System.debug('STatus code='+Res.getStatusCode());
                    if(Res.getStatusCode()==200){
                        each.Area_Code__c='';
                        System.debug('==each.Area_Code=='+each.Area_Code__c);
                        wrpList = (List<Wrapper>)JSON.deserialize(Res.getBody(),List<wrapper>.class);
                        System.debug('wrapper list='+wrpList);
                        for(Wrapper eachWrap: wrpList){
                            if(String.isBLANK(each.Area_Code__c)){
                                each.Area_Code__c = eachWrap.area_id; System.debug('==each.Area_Code inside=='+each.Area_Code__c);
                            }
                            if(eachWrap.serviceable=='f'){
                                each.NonServiceable_Locality__c= TRUE;
                            }
                        }
                    }
                }
            }
            LeadAssignment(lstNewLead);
        }
        public class wrapper{
            public String area_type{get;set;}
            public String area_id{get;set;}
            public String area_name{get;set;}
            public String parent_id{get;set;}
            public String serviceable{get;set;}
        }
        Public Static void LeadAssignment(List<Lead> lstLead){
            
            set<String> setLeadTerritory = new set<String>();
            set<String> setLeadEmail     = new set<String>();
            
            list<Territory_Area_Code_Mapping__mdt> lstTerrritory = new list<Territory_Area_Code_Mapping__mdt>();
            lstTerrritory=[SELECT id,Area_Code__c,Queue_Name__c,Territory_Name__c,Role_Name__c FROM  Territory_Area_Code_Mapping__mdt];
            map<String,Territory_Area_Code_Mapping__mdt> mapArea_Queue = new map<String, Territory_Area_Code_Mapping__mdt>();    
            
            list<Group> lstGroup = new list<Group>();
            lstGroup = [SELECT Id,Name,Type FROM Group WHERE Type = 'Queue'];
            map<String,Id> mapQueueName_Id = new map<String,Id>();
            
            for(Group each : lstGroup){
                mapQueueName_Id.put(each.Name,each.Id);
            }
           
            for(Territory_Area_Code_Mapping__mdt each:lstTerrritory){
                mapArea_Queue.put(each.Area_Code__c,each);
            }    
            for(Lead eachLead : lstLead){
                System.debug('area code=='+eachLead.Area_Code__c);
                System.debug('aNonServiceable_City__c=='+eachLead.NonServiceable_City__c);
             if(String.IsBLANK(eachLead.Area_Code__c)){
                eachLead.NonServiceable_City__c= TRUE;
             }else{
                eachLead.NonServiceable_City__c= FALSE;
             }
             
             if(!String.IsBLANK(eachLead.Email)){
                setLeadEmail.add(eachLead.Email);
             }
             
              If(eachLead.Area_Code__c != Null ){
                   System.debug('area code=======================>'+eachLead.Area_Code__c);
                   If(!(mapArea_Queue.containsKey(eachLead.Area_Code__c)  && eachLead.Owner.UserRole.Name == mapArea_Queue.get(eachLead.Area_Code__c).Role_Name__c)){
                     System.debug('mapArea_Queue.get(eachLead.Area_Code__c)=======================>'+mapArea_Queue.get(eachLead.Area_Code__c));
                     System.debug('==eachLead.NonServiceable_City__c=='+eachLead.NonServiceable_City__c);
                     System.debug('==eachLead.NonServiceable_Locality__c=='+eachLead.NonServiceable_Locality__c);
                       If(eachLead.Area_Code__c != Null && eachLead.NonServiceable_City__c== FALSE && eachLead.NonServiceable_Locality__c== FALSE){
                           System.debug('eachLead.NonServiceable_City__c=======================>'+eachLead.NonServiceable_City__c);
                          // System.debug('==mapArea_Queue.get(eachLead.Area_Code__c).Queue_Name__c=='+mapArea_Queue.get(eachLead.Area_Code__c).Queue_Name__c);
                           System.debug('==mapQueueName_Id=='+mapQueueName_Id);
                           if(!mapQueueName_Id.isEmpty() && mapArea_Queue.get(eachLead.Area_Code__c)!=null && mapQueueName_Id.containsKey(mapArea_Queue.get(eachLead.Area_Code__c).Queue_Name__c) && mapQueueName_Id.get(mapArea_Queue.get(eachLead.Area_Code__c).Queue_Name__c)!=null){
                               eachLead.OwnerId = mapQueueName_Id.get(mapArea_Queue.get(eachLead.Area_Code__c).Queue_Name__c);    
                               eachLead.Territory__c = mapArea_Queue.get(eachLead.Area_Code__c).Territory_Name__c;
                           }
                           if(!String.IsBLANK(eachLead.Territory__c)){
                               setLeadTerritory.add(eachLead.Territory__c);
                           }
                           System.debug('emapArea_Queue.get(eachLead.Area_Code__c).Territory_Name__c======================>'+eachLead.Territory__c);
                       }else{
                           System.debug('else1===============>');
                           If(eachLead.Source_Type__c=='Direct'){
                            eachLead.OwnerId = mapQueueName_Id.get('Unassigned Lead Queue');
                            eachLead.Area_Code__c='';  
                            eachLead.Territory__c=''; 
                           }    
                       }
                   }
              }else{
                    If(eachLead.Source_Type__c=='Direct'){
                        eachLead.OwnerId = mapQueueName_Id.get('Unassigned Lead Queue');
                        eachLead.Area_Code__c='';  
                        eachLead.Territory__c='';
                    }
                             
              } 
              system.debug('==eachLead.OwnerId=='+eachLead.OwnerId);           
           }
           list<lead> lstExtLead = new list<Lead>();
           map<String,Lead> mapEmail_Lead = new map<String,Lead>(); system.debug('==setLeadTerritory=='+setLeadTerritory);system.debug('==setLeadEmail=='+setLeadEmail);
           lstExtLead=[SELECT Email,Id,IsConverted,OwnerId,Territory__c FROM Lead WHERE Territory__c IN:setLeadTerritory AND email IN: setLeadEmail];
           system.debug('==lstExtLead=='+lstExtLead);
           for(Lead each: lstExtLead){
               if(!String.IsBLANK(each.Email)){
                   mapEmail_Lead.put(each.Email,each);
               }
           } system.debug('==mapEmail_Lead=='+mapEmail_Lead);
           if(!mapEmail_Lead.isEmpty()){
               for(Lead eachLead : lstLead){
                   if(eachLead.Email!=null && mapEmail_Lead.containskey(eachLead.Email) && eachLead.Territory__c!=null && mapEmail_Lead.get(eachLead.email).Territory__c== eachLead.Territory__c){
                        if(mapEmail_Lead.get(eachLead.email).OwnerId.getSObjectType() == User.SObjectType){
                            eachLead.OwnerId=  mapEmail_Lead.get(eachLead.email).OwnerId;
                           if(eachLead.Status=='Disqualified' && eachLead.OwnerId.getSObjectType() == User.SObjectType){
                                eachLead.Status='New';
                                eachLead.Reason_for_closing__c='';
                            }               
                        }
                   }
               }
           } 
           handleRoundRobin(lstLead);
        }
        
        /*****************************************************************************************************************
   * This method is to perform custom round robin logic in which a lead will be assigned automatically to a user 
   * once it comes through lead assignement rule. To understand the flow we have two objects namely Round_Robin__c(Parent)
   * and Lead_Distribution(child) in which we are holding the informations like assigned%,Actual %,number of leads....
   * ***************************************************************************************************************/
    public static void handleRoundRobin(List<Lead> leadData){
        //LeadHandlerBefore.noOfRun ++;
        Set<id> holdQueueName=new Set<id>();
        Set<Id> leadAssSet = new Set<Id>();
        //Map<string,ActiveCount__c> mapCodes = ActiveCount__c.getAll();
        Map<Id, String> groupNameMap = new Map<Id, String>();
        Map<String,Id> rrNameMap = new Map<String,Id>();        
        //Integer InActiveCount=(integer)mapCodes.get('Counter').InActive_Days__c;
        //DateTime Dc=System.now().addDays(-(InActiveCount));
        String leadPrefix =  Lead.sObjectType.getDescribe().getKeyPrefix();
        List<Lead_Distribution__c> leadAssList = new List<Lead_Distribution__c>();
        for(Lead ldRec : leadData){
            //if(ldRec.checkAssignment__c==true){
                if(!(ldRec.OwnerId.getSObjectType() == User.SObjectType) ){
                    holdQueueName.add(ldRec.ownerId);
                    System.debug('in round robin==>'+holdQueueName);
                }
            //}
        }
                            System.debug('holdQueueName==>'+holdQueueName);

       if(!holdQueueName.isEmpty()){
            for(Group grp : [SELECT Id, Name, Email FROM Group  WHERE id IN : holdQueueName ]){
                groupNameMap.put(grp.id,grp.Name);
                System.debug('holdQue==>'+grp);
            }
            if(!groupNameMap.isEmpty()){
                Map<Id, Round_Robin__c> roundRobinMap= new Map<Id, Round_Robin__c>([SELECT Queue_Name__c,Total_Leads__c,
                (SELECT Number_of_Leads__c,USer__r.name,User__r.isActive,User__r.LastLoginDate,Assigned__c,Actual__c,Round_Robin__c,User__c from Lead_Distributions__r )
                FROM Round_Robin__c 
                WHERE Queue_Name__c IN : groupNameMap.values()]);
                if(!roundRobinMap.isEmpty()){
                   for(Round_Robin__c rb : roundRobinMap.values()){
                        rrNameMap.put(rb.Queue_Name__c, rb.id);
                    }
                    for(Lead le : leadData){
                       if(!(le.OwnerId.getSObjectType() == User.SObjectType) ){ 
                            Round_Robin__c roundRobin = roundRobinMap.get(rrNameMap.get(groupNameMap.get(le.OwnerId)));
                            if(roundRobin != null){ 
                                if(roundRobin.Lead_Distributions__r != null){
                                    Integer cnt = 0; // to get the count 
                                    for(Lead_Distribution__c leadAssign : roundRobin.Lead_Distributions__r){
                                        cnt++;
                                    }
                                    if(cnt > 0){
                                        Lead_Distribution__c curLAss = roundRobin.Lead_Distributions__r[0];
                                        System.debug('current lead of user==>'+curLAss);
                                        decimal i= curLAss.Assigned__c - curLAss.Actual__c;
                                        System.debug('For the current lead value of i==>'+i);
                                        for(Lead_Distribution__c lAss : roundRobin.Lead_Distributions__r){
                                            System.debug('I Value'+i+'User information'+lass.Actual__c+'==>'+lass.Assigned__c+'==>'+lass.Number_of_Leads__c);
                                           if(i < lAss.Assigned__c - lAss.Actual__c){
                                                System.debug('current value of i===>'+i);
                                                curLAss = lAss;
                                                i = lAss.Assigned__c - lAss.Actual__c;
                                                System.debug('value of i after assigning lead='+i);
                                            } 
                                            
                                        }
                                        if(curLAss != null && curLAss.Number_of_Leads__c != null && curLAss.User__c != null ){
                                            le.OwnerId = curLAss.User__c; system.debug('==le.OwnerId=='+le.OwnerId);
                                            //if(le.Status=='Disqualified' ){
                                                le.Status='New';
                                                le.Reason_for_closing__c='';
                                            //}
                                            //le.Lead_Assigned__c = true;
                                            //le.Field_for_Lead_Workflow__c = true;
                                            curLAss.Number_of_Leads__c += 1;
                                            if(!leadAssSet.contains(curLAss.id)){
                                                leadAssList.add(curLAss);
                                                leadAssSet.add(curLAss.id);
                                            }
                                        }
                                        
                                    } //c > 0
                                    else{
                                        // No user is assigned from the queue;
                                        System.debug('No user is assigned from the queue and hence the owner is empty and default owner will be assigned');
                                        //le.Field_for_Lead_Workflow__c = true;
                                    }
                                }
                            } //if roundrobin != null
                        }
                        
                    }// end of for loop
                    if(!leadAssList.isEmpty()){
                        update leadAssList;
                    }
                }
            }
        }
        System.debug('leadData------>'+leadData);
        if(leadData.size()>0){
            //added by moham
            StopRecursion.isLeadAfterUpdate=FALSE;
            update leadData;
        } 
        
    }
}