public class OrderService {
    
    public static House__c getHouseRecords(id houseid){
        return [Select id,Pincode__c,State__c,City__c,country__c from House__c where id =:houseid ];
    }
    public static account getOwnerBillingAddDetails(id ownerId){
        return [Select id,account.name,BillingStreet,BillingCity,BillingState,BillingPostalCode,billingcountry
                from account where id =: ownerId ];
    }
    public static order getOrderRecords(id orderId){
        return [Select id,OrderNumber,Expected_Arrival_Date__c,PoDate,Type,EffectiveDate,Category__c,
                accountid,account.name,account.BillingStreet,account.BillingCity,Account.BillingState,
                account.billingcountry,account.BillingPostalCode,
                Vendor__c,Vendor__r.name,Vendor__r.TIN_No__c,Vendor__r.Contact_Email__c,
                Vendor__r.BillingStreet,Vendor__r.BillingCity,Vendor__r.BillingState,Vendor__r.Phone,
                Vendor__r.BillingPostalCode,Vendor__r.BillingCountry,Vendor__r.Phone__c,
                Vendor__r.Reference_Number__c,Vendor__r.Mode_of_Payment__c,Vendor__r.Terms_of_Payment__c,    
                House_PO__c,House_PO__r.Pincode__c,House_PO__r.State__c,House_PO__r.City__c,
                House_PO__r.country__c,House_PO__r.Street__c,
                Show_Nestaway_Billing_Address__c,
                house__r.country__c, house__r.Street__c 
                from Order where id = : orderId];
    } 
    public static list <OrderItem> getOrderProducts(id orderId){
        return [Select id,Vat__c,ListPrice,UnitPrice,Quantity,
                Product2id,Product2.name,Product2.productCode,Product2.Product_Type__c
                from OrderItem where Orderid =: orderId];
    }
    
     public static account getvendorDetails(id vendorId){
        account acc =  [Select id,Name,TIN_No__c,Contact_Email__c,
                BillingStreet,BillingCity,BillingState,BillingPostalCode
                    from Account where id =: vendorId AND Contact_Email__c != null];
         System.debug('acc orders service'+acc);
         return acc;
    }
    public static list<contact> getvendorContacts(id vendorId){
        return [Select id,Name , City_Master__c, Email 
            from contact where accountId =: vendorId];
    }

}