/*  Created By : Mohan
    Purpose    : Trigger invocation for Case - RecordType - Service Request are handled in this Handler   */

public class CaseServiceRequestTriggerHandler {

    public static Id ServiceRequestRecordTypeId  = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();  
    public static Id unverifiedCaseQueueId {get; set;}
    public static Id unassignedCaseQueueId {get; set;}

    static {
        Set<String> caseQueues = new Set<String>{ Constants.QUEUE_UNASSIGNED_CASE_QUEUE, Constants.QUEUE_UNVERIFIED_SERVICE_REQUESTS};
        List<Group> groupList = [select Id, Name, DeveloperName from Group where Name =: caseQueues];
        for(Group g: groupList){
            if(g.Name == Constants.QUEUE_UNASSIGNED_CASE_QUEUE)
                unassignedCaseQueueId = g.Id;
            else if(g.Name == Constants.QUEUE_UNVERIFIED_SERVICE_REQUESTS)
                unverifiedCaseQueueId = g.Id;
        }
    }
    
    // Before Insert oprations for the Cases with Recordtype Service Request   
    public static void beforeInsert(List<Case> newCases){
        
        try{    
                List<Case> caseList = new List<Case>();                          //Case List for Which Problem Related info needs to be updated
                List<Case> assignmentCaseList = new List<Case>();                //Case List for which assignment needs to be performed
                Map<Id, Id> accountMap = new Map<Id, Id>();
                Set<Id> accountIdSet = new Set<Id>(); 

                for(Case c: newCases){
                    if(c.AccountId != null && c.RecordTypeId == ServiceRequestRecordTypeId)
                        accountIdSet.add(c.AccountId);
                }

                if(!accountIdSet.isEmpty()){
                    List<Contact> contactList = [select Id, AccountId from Contact where AccountId =: accountIdSet
                                                 and Account.RecordType.Name =: Constants.ACCOUNT_RECORD_TYPE_PERSON_ACCOUNT];

                    for(Contact con: contactList){
                        accountMap.put(con.AccountId, con.Id);
                    }
                }            

                //Id defaultEntitlementId =  [select Id from Entitlement where Account.Name = 'NestAway' limit 1].Id;
                
                for(Case c: newCases){

                    if(c.AccountId != null && c.ContactId == null && c.RecordTypeId == ServiceRequestRecordTypeId)
                        c.ContactId = !accountMap.isEmpty() && accountMap.containsKey(c.AccountId) && accountMap.get(c.AccountId) != null ? accountMap.get(c.AccountId) : null;

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && (c.Issue_level__c == Constants.CASE_ISSUE_LEVEL_HOUSE_LEVEL || c.Issue_level__c == Constants.CASE_ISSUE_LEVEL_ROOM_LEVEL) && c.Verified_by_all_Tenants__c == false && c.Status == Constants.CASE_STATUS_WATING_ON_TENANT_APPROVAL){
                        c.OwnerId = unverifiedCaseQueueId;
                        caseList.add(c);
                    } else if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Origin == Constants.CASE_ORIGIN_EMAIL){
                        assignmentCaseList.add(c);
                    } else if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Problem__c != null){
                        //c.EntitlementId = defaultEntitlementId;
                        caseList.add(c);
                        assignmentCaseList.add(c);
                    } else if(c.RecordTypeId == ServiceRequestRecordTypeId){
                        System.debug('*******************in the assignment Case List');
                        assignmentCaseList.add(c);
                    }

                }
                
                if(!caseList.isEmpty()){
                    CaseServiceRequestHandler.UpdateProblemRelatedInfo(caseList);
                }

                System.debug('******************assignmentCaseList ' + assignmentCaseList);
                if(!assignmentCaseList.isEmpty())
                    CaseServiceRequestAssignment.serviceRequestCaseAssignment(assignmentCaseList);
              
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
    

    // Before Update oprations for the Cases with Recordtype Service Request
    public static void beforeUpdate(Map<Id, Case> newMap, Map<Id, Case> oldMap){
        
        System.debug('*******************beforeUpdate');

        try{          
                List<Case> caseList = new List<Case>();       //cases for whome Problem is changed and Assignment needs to fire
                List<Case> changedQueueCaseList = new List<Case>();
                List<Case> tenantVerifiedCaseList = new List<Case>();
                List<Case> ownerChangedCaseList = new List<Case>();

                for(Case c: newMap.values()){
                    Case oldCase = oldMap.get(c.Id);


                    //update Market Price and Actual Price
                    if(c.RecordTypeId == ServiceRequestRecordTypeId){  

                        if(c.Owner_Inclusion__c == 'true' && c.Owner_Inclusion__c != oldMap.get(c.Id).Owner_Inclusion__c){
                            c.Owner_approval_status__c = 'Pending';
                            c.Deadline_For_Owner_Approval__c = System.now().addDays(2);
                        }     

                        if(c.Owner_Inclusion__c == 'false' && c.Owner_Inclusion__c != oldMap.get(c.Id).Owner_Inclusion__c){
                            c.Owner_approval_status__c = '';
                            c.Status = Constants.CASE_STATUS_WORK_IN_PROGRESS;
                        }                                          

                        if(c.Express_Service_Cost__c == null){
                            c.Express_Service_Cost__c = 0;
                        }

                        if(c.Service_Visit_Cost_Actual__c == null){
                            c.Service_Visit_Cost_Actual__c = 0;
                        }

                        if(c.Service_Cost_Actual__c  == null){
                            c.Service_Cost_Actual__c = 0;
                        }

                        if(c.Service_visit_cost__c   == null){
                            c.Service_visit_cost__c  = 0;
                        }

                        if(c.Service_Cost__c == null){
                            c.Service_Cost__c = 0;
                        }                                                

                        c.Total_Actual_Price__c = c.Express_Service_Cost__c + c.Service_Visit_Cost_Actual__c + c.Service_Cost_Actual__c;
                        c.Total_Market_Price__c = c.Express_Service_Cost__c + c.Service_visit_cost__c + c.Service_Cost__c;                      
                    }

                    //when Problem is changed on the Case run the Assignment Logic
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Problem__c != oldCase.Problem__c){
                        caseList.add(c);
                    }  

                    //when verified by all tenants is true - run the assignment logic
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Verified_by_all_Tenants__c == true && c.Verified_by_all_Tenants__c != oldCase.Verified_by_all_Tenants__c)  
                        tenantVerifiedCaseList.add(c);

                    //when Case Queue is Changed Assignment logic to start
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Problem__c == oldCase.Problem__c && c.Queue__c != oldCase.Queue__c){
                        changedQueueCaseList.add(c);
                    }

                    //when Case Owner is changed populate the Next Email to field
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.OwnerId != oldCase.OwnerId && c.Problem__c == oldCase.Problem__c && c.Queue__c == oldCase.Queue__c){
                        ownerChangedCaseList.add(c);
                    }

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Owner_Inclusion__c == 'Yes' && c.Owner_Inclusion__c != oldCase.Owner_Inclusion__c){
                        c.Owner_approval_status__c = 'Pending';
                    }
                }
                
                if(!caseList.isEmpty()){
                    CaseServiceRequestHandler.UpdateProblemRelatedInfo(caseList);
                    CaseServiceRequestAssignment.serviceRequestCaseAssignment(caseList);  
                }     

                if(!tenantVerifiedCaseList.isEmpty())
                    CaseServiceRequestAssignment.serviceRequestCaseAssignment(tenantVerifiedCaseList);    

                if(!changedQueueCaseList.isEmpty())
                    CaseServiceRequestAssignment.serviceRequestCaseAssignment(changedQueueCaseList);

                if(!ownerChangedCaseList.isEmpty())
                    CaseServiceRequestHandler.populateNextEmailTofield(ownerChangedCaseList);
            
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }
    }


    /*  Created By : Mohan
        Purpose    : After Update operations on Case with RecordType Service Request are handled from this method */
    public static void afterUpdate(Map<Id, Case> newMap, Map<id, Case> oldMap){

        try{    
                Set<Id> esclatedCaseIdSet = new Set<Id>();
                Set<Id> resolvedCaseIdSet = new Set<Id>();
				List<Case> caseList = new List<CAse>();
                for(Case c: newMap.values()){

                    
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Escalation_Level__c != oldMap.get(c.Id).Escalation_Level__c){                        
                        esclatedCaseIdSet.add(c.Id);
                    }

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Status == Constants.CASE_STATUS_RESOLVED && c.Status != oldMap.get(c.Id).Status)
                        resolvedCaseIdSet.add(c.Id);
                     if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Status == Constants.CASE_STATUS_RESOLVED && c.Status != oldMap.get(c.Id).Status)
                        caseList.add(c);

                     if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Owner_approval_status__c != oldMap.get(c.Id).Owner_approval_status__c){
                        Case_Comment__c cc = new Case_Comment__c();
                        cc.Comment__c = 'Owner Approval Status is ' + c.Owner_approval_status__c;
                        cc.Case__c = c.Id;
                        insert cc;
                     }

                    //Make an API call when Owner Inclusion is Yes to Web App
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Owner_Inclusion__c == 'true' && 
                        c.Owner_Inclusion__c != oldMap.get(c.Id).Owner_Inclusion__c){
                        CaseSnMTriggerHelper.sendOwnerInclusionDetails(c.Id);
                    }                                          
                }

                if(!esclatedCaseIdSet.isEmpty()){
                    CaseServiceRequestHandler.onCaseEsclation(esclatedCaseIdSet);
                }

                if(!resolvedCaseIdSet.isEmpty()){
                    CaseServiceRequestHandler.completeMilestones(resolvedCaseIdSet);
                    CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(caseList);
                }

            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }
    }



}