/*  Created By   : Mohan - WarpDrive Tech Works
    Created Date : 19/05/2017
    Purpose      : All the queries related to the Opportunity Object are here */


public class OpportunitySelector {

    //Query all the Opportunities with the Childs - Checklist, Contract, SalesOrder, Keys
    public static List<Opportunity> getOpportunitiesWithChilds(Set<Id> oppIdSet){

        return [select Id, (select Id,RecordtypeId,Onboarding_Zone_code__c, House__c,Type_Of_HIC__c, House_Lat_Long_details__c from House_Inspection_Checklist__r), (select Id, House__c from Contracts__r), 
                (select Id, House__c from Orders) from Opportunity where Id =: oppIdSet];
    }
     public static Map<id,Opportunity> getOpportunitiesOppidSet(Set<Id> oppIdSet){

        return new Map<id,Opportunity>([select Id,street__c,State__c,Pin_Code__c,City__c,Country__c from Opportunity where Id =: oppIdSet]);
    }
}