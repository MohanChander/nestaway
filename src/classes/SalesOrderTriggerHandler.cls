/*
* Description: It's trigger handler class of Order object's trigger, to update opportunity records based on few conditions.
*/



public class SalesOrderTriggerHandler {
    Public Static Void AfterUpdate(Map<Id,Order> newMap,Map<Id,Order> oldMap){
        set<id> oppIdSet = new set<id>();
        list<Opportunity>lstOppUpdate = new list<Opportunity>();
        
        for(Order each : newMap.values()){
            if(each.Status != oldMap.get(each.Id).Status && each.Status == 'Submitted'){
                oppIdSet.add(each.OpportunityId);    
            }else if(each.Status != oldMap.get(each.Id).Status && each.Status == 'Cancelled'){
                Opportunity Opp = new Opportunity(Id= each.OpportunityId,StageName='Lost',Lost_Reason__c='Sales Order Cancelled');
                lstOppUpdate.add(opp);
            }   
        }
        
        List<Opportunity> oppList = [Select id,StageName from Opportunity where Id IN : oppIdSet];
        for(Opportunity opp : oppList){
            opp.StageName = 'Final Contract';
        }
        update oppList;
        
        if(lstOppUpdate.size()>0){
            try{
                update lstOppUpdate;
            }catch(exception ex){
                system.debug('==exception while updating opportunity in SOTriggerHandler=='+ex);
            }
        }
    }


    /*Created By: Mohan : WarpDrive Tech Works
      Purpose           : Create Purchase Orders for the Sales Order when the Generate PO check box is checked
      Execution        : After Update */
    
   
    public static void generatePurchaseOrders(Map<Id,Order> newMap,Map<Id,Order> oldMap){

        Set<Id> orderIdSet = new Set<Id>();
        
        for(Order each : newMap.values()){
            if(each.Generate_PO__c != oldMap.get(each.Id).Generate_PO__c && each.Generate_PO__c == true && !(each.Total_Items__c > 0)){
                each.addError('There are no products associated with the Order');
            } else if(each.Generate_PO__c != oldMap.get(each.Id).Generate_PO__c && each.Generate_PO__c == true){
                orderIdSet.add(each.Id); 
            }
        }        

        //Query for OrderItems 
        List<Order> ordList = OrderSelector.getOrderWithOLIFromIdSet(orderIdSet);

        for(Order ord: ordList){

            Map<Id, List<OrderItem>> vendorOrderItemMap = new Map<Id, List<OrderItem>>();   //stores the vendor Id and the related OrderItems

            for(OrderItem oi: ord.OrderItems){
                if(vendorOrderItemMap.containsKey(oi.Vendor__c)){
                    vendorOrderItemMap.get(oi.Vendor__c).add(oi);
                } else {
                    vendorOrderItemMap.put(oi.Vendor__c, new List<OrderItem>{oi});
                }
            }

            List<Order> newOrderList = new List<Order>();
            List<OrderItem> newOrderItemList = new List<OrderItem>();

            //using unit of work pattern
            RelationshipUtiltiy relUtil = new RelationshipUtiltiy();

            if(!vendorOrderItemMap.isEmpty()){
                for(Id vendorId: vendorOrderItemMap.keySet()){
                    Order newOrder = new Order();
                    newOrder.Vendor__c = vendorId;
                    newOrder.EffectiveDate = System.today();
                    newOrder.Status = Constants.ORDER_STATUS_DRAFT;
                    newOrder.AccountId = ord.AccountId;
                    newOrder.Pricebook2Id = ord.Pricebook2Id;
                    newOrder.House_PO__c = ord.House__c;
                    newOrder.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(Constants.ORDER_RT_PURCHASE_ORDER).getRecordTypeId();
                    newOrderList.add(newOrder);

                    for(OrderItem oi: vendorOrderItemMap.get(vendorId)){
                        OrderItem ordItem = new OrderItem();
                        ordItem = oi.clone();

                        //register the relationship - UOW
                        relUtil.registerRelationship(ordItem, newOrder, OrderItem.OrderId);
                        newOrderItemList.add(ordItem);
                    }
                }
            }

            insert newOrderList;

            //resolve the relationship - UOW
            relUtil.resolve('OrderItem');
            insert newOrderItemList;
        }
    }  // end of generatePurchaseOrders method
     
}