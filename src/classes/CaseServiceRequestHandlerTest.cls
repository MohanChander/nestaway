@isTest
Public Class CaseServiceRequestHandlerTest {
    Public Static TestMethod Void doTest(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
         problem__c p= new problem__c();
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        Case c=  new Case();
        c.Status='Waiting On Tenant Approval';
        c.House__c=hos.Id;
        c.Problem__c=p.id;
        c.RecordTypeId=devRecordTypeId;
        c.ownerId=newuser.Id;
        c.Subject='test';
        c.Next_Escalation_to__c= newuser.id;
        insert c;
        cList.add(c);
        Map<Id,Case> mapCase= new Map<Id,Case>();
        for(Case cc:cList){
            mapCase.put(cc.Id,cc);
        }
        updatedcList.add(c.id);
        CaseServiceRequestHandler.onCaseEsclation(updatedcList);
        CaseServiceRequestHandler.UpdateProblemRelatedInfo(cList);
    }
    
}