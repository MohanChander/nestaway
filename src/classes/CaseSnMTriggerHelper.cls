/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for Case - SnM module
**********************************************/

public class CaseSnMTriggerHelper {
  
/**********************************************
    Created By : Mohan
    Purpose    : Make an API call to WebApp with Owner Inclusion Details
**********************************************/
  @Future(callout=true)
  public static void sendOwnerInclusionDetails(Id CaseId){

            System.debug('****************sendOwnerInclusionDetails');

        try{           
                Case c = [select Id, CaseNumber, Deadline_For_Owner_Approval__c, Owner_Share_approx__c, 
                          Owner_Inclusion_comment__c from Case where Id =: caseId];                                          
                List<NestAway_End_Point__c> nestURL = NestAway_End_Point__c.getall().values();       
                String authToken=nestURL[0].Webapp_Auth__c;
                String EndPoint = nestURL[0].SnM_Notification__c +'&auth='+authToken;
                
                System.debug('***EndPoint  '+EndPoint);
                
                string jsonBody='';
                WBMIMOJSONWrapperClasses.OwnerInclusionNotificationJSON ownerInclusionObject = new WBMIMOJSONWrapperClasses.OwnerInclusionNotificationJSON();
                ownerInclusionObject.method = 'notify_for_owner_approval';
                ownerInclusionObject.ticket_number = c.CaseNumber;
                ownerInclusionObject.deadline_for_owner_approval = c.Deadline_For_Owner_Approval__c;
                ownerInclusionObject.approx_cost = c.Owner_Share_approx__c;
                ownerInclusionObject.inclusion_comment = c.Owner_Inclusion_comment__c;

                jsonBody = JSON.serialize(ownerInclusionObject);
                System.debug('***jsonBody  '+jsonBody);
                
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
                
                String respBody = res.getBody();
                system.debug('****respBody'+ respBody);
        } catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Invoice Generation API');  
        }    
  }

     /*****************************************************************************************************************************************************************************************************************************************
 Added by baibhav
 purpose: to create Hrm task For SNM 
 ********************************************************************************************************************************************************************************************************************************************/  
     public static void HrmTaskForService(List<case> caseList)
    {
      try
      {      sYSTEM.DEBUG('******Generate****');
            Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_OWNER_FOLLOW_UP).getRecordTypeId();
            List<task> insertTask=new List<task>();
            Set<id> houIdSet = new Set<id>();
            set<id> casetenantIdSet =new Set<id>();
            for(Case c:caseList)
            {
               houIdSet.add(c.house1__c);
               casetenantIdSet.add(c.Accountid);
            } 
            Map<Id,house__c> houMap=new Map<id,house__c>();
            Map<id,Account> accMap=new Map<Id,Account>();
            if(!casetenantIdSet.isEmpty())
            { System.debug(casetenantIdSet);
              accMap=new Map<id,Account>([select id,name from Account where id=:casetenantIdSet]);
            }

            if(!houIdSet.isEmpty())
            {
              houMap=new Map<Id,house__c>([Select id,houseID__c,House_Owner__r.name,House_Owner__r.Phone,HRM__c,House_Owner__r.PersonEmail from house__c where id=:houIdSet]); 
            }
           for(Case c:caseList)
            { 
              Task ta= new task();
              ta.whatid=c.id;
              ta.recordtypeId=taskRecordTypeId;
              ta.Description=c.Owner_Inclusion_comment__c;
              ta.Case_number__c=c.CaseNumber;
              if(C.house1__c!=null)
              ta.House__c=c.house1__c;
              
              if(accMap.get(c.Accountid).name!=null)
               {
                  ta.Tenant_owner_name__c=accMap.get(c.Accountid).name;
               }
              sYSTEM.DEBUG('******Generate0****');
              if(houMap.containsKey(c.house1__c))
              {
                  if(houMap.get(c.house1__c).House_Owner__r.name!=null)
                   {
                      ta.Owner_Name__c=houMap.get(c.house1__c).House_Owner__r.name;
                   }
                  if(houMap.get(c.house1__c).House_Owner__r.Phone!=null)
                  {
                      ta.Owner_Phone__c=houMap.get(c.house1__c).House_Owner__r.Phone;
                  }
                  if(houMap.get(c.house1__c).hRM__c!=null)
                  {
                      ta.OwnerId=houMap.get(c.house1__c).hRM__c;
                  }
                   if(houMap.get(c.house1__c).House_Owner__r.PersonEmail!=null)
                  {
                      ta.Owner_Email__c=houMap.get(c.house1__c).House_Owner__r.PersonEmail;
                  }
                  sYSTEM.DEBUG('******Generate1****');
               }
               ta.status='Open';
               ta.subject='Owner follow up Task for house: '+houMap.get(c.house1__c).houseid__c;
               ta.ActivityDate=System.today().addDays(2);
      
              insertTask.add(ta);
            }
            if(!insertTask.isEmpty())
            {
            sYSTEM.DEBUG('******Generate2****');
            insert insertTask;
            }
      } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
 /*****************************************************************************************************************************************************************************************************************************************
 Added by baibhav
 purpose: to Owner task status in SNM
 ********************************************************************************************************************************************************************************************************************************************/  
  public static void updateOwnerTask(List<case> newList)
  {
        try
        {
            Set<id> casIdSet = new Set<id>();
            List<Task> tsklist=new List<task>();
            List<Task> updatetsklist=new List<Task>();
            for(case cs:newList)
            {
                casIdSet.add(cs.id);
            }

            if(!casIdSet.isEmpty())
            tsklist =TaskSelector.getOwnerTaskRelatedToseviceCase(casIdSet);
            
            for(Case cs:newList)
            {
                for(Task tsk:tsklist)
                {
                    if(tsk.whatid==cs.id)
                    {
                        if(cs.Owner_approval_status__c=='Auto Approved')
                        {
                            tsk.status='Auto Approved';
                        }
                        if(cs.Owner_approval_status__c=='Approved')
                        {
                            tsk.status='Owner Approved';
                        }
                        if(cs.Owner_approval_status__c=='Rejected')
                        {
                            tsk.status='Rejected';
                        }
                        updatetsklist.add(tsk);
                    }
                }
         
            }
           if(!updatetsklist.isEmpty())
           {
              update updatetsklist;
           }
        } Catch(Exception e)
          {
             System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e); 
          }
  } 
  /**********************************************************************************************************************************************
    Added by baibahv
    Purpose for creating SNM settlement case
**********************************************************************************************************************************************/
    public static void CreateSettlementCase(set<id> pcasIDSet)
    {  System.debug('Bony2');
        try
           {   Id verfictionSettlement =Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_VENDOR_SETTLEMENT).getRecordTypeId();  
                         List<Case> newCase =new List<Case>();
                         list<workorder> wrkList=new list<workorder>(); 
                         Map<id,Case> parCaseMap=new Map<id,Case>(); 
                         Map<id,user> usrMAp=new Map<id,user>();
                         if(!pcasIDSet.isEmpty())
                         {
                            parCaseMap = CaseSelector.getMapCasefromCaseID(pcasIDSet);
                             wrkList=WorkOrderSelector.getvenderWorkOrders(pcasIDSet);
                         }
                          Map<Id,workorder> wrkMapCasId=new Map<Id,workorder>();
                            Set<Id> ownerSet =new Set<id>();
                            for(Workorder wrk:wrkList)
                            {
                                ownerSet.add(wrk.ownerId);
                                wrkMapCasId.put(wrk.caseId,wrk);
                            }
                            if(!ownerSet.isEmpty()) 
                            {
                                usrMAp =UserSelector.getUserDetailsMap(ownerSet);
                            }
                            Set<id> accid =new Set<Id>();
                            for(User us:usrMAp.values())
                            {
                              accid.add(us.Accountid);
                            }
                            List<Bank_Detail__c> bnkList =BankDetailSelector.getBankDetailByAccount(accid);
                            System.debug('Bony2'+bnkList);
                           Map<id,Bank_Detail__c> bnkMapToAccID =new Map<id,Bank_Detail__c>();
                          for(Bank_Detail__c bk:bnkList)
                             {
                               bnkMapToAccID.put(bk.Related_Account__c,bk);
                             }
                         System.debug('Bony3');
                         for(Case cas:parCaseMap.values())
                         {
                           if(wrkMapCasId.containsKey(cas.id) && usrMAp.containsKey(wrkMapCasId.get(cas.id).ownerid) && usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Vendor_Type__c!='Inhouse')
                           {  System.debug('Bony3-1');
                              Case ca=new Case();
                              ca.Parentid=cas.id;
                              ca.RecordTypeId=verfictionSettlement;
                              ca.subject='Settlement Case';
                              ca.AccountId=cas.AccountId;
                              ca.ContactId=cas.ContactId;
                              ca.House1__c=cas.House1__c;
                              ca.Material_Cost__c=cas.Material_Cost__c;
                              ca.Labour_Cost__c=cas.Labour_Cost__c;
                              //ca.Express_Service_Cost__c=cas.Express_Service_Cost__c;
                              ca.Service_visit_cost__c=cas.Service_visit_cost__c;
                              System.debug('Bony3-1'+bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId));
                              if(bnkMapToAccID.containsKey(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId))
                              {
                                ca.A_c_Holder_Name_Tenant__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).name;
                                ca.Tenant_bank_A_c_No__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Account_Number__c;
                                ca.Bank_IFSC_code__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).IFSC_Code__c;
                                ca.Tenant_Bank_Name__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Bank_Name__c;
                                ca.Tenant_Bank_Branch_Name__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Branch_Name__c;
                              }
                              newCase.add(ca);
                            }
                         } 
                         if(!newCase.isEmpty())
                         {System.debug('Bony4');
                           insert newCase;
                         }

           }catch (Exception e)
           {
             System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
           }
    }

}